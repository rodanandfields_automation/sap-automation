
Sub ErrComponentCreation(sTestcaseId)

	ExecuteGlobal "Dim QTPReporter"
	Set QTPReporter = Reporter
	ExecuteGlobal "Dim Reporter"

	Set Reporter = New CustomReporter
	'Reporter.tExcelFilePath=EXCEL_RESULT_FILE
End Sub 

Sub ReportWriter(sEventStatus,sReportStepName,sDetails,bExitRun)
	Reporter.ReportEvent sEventStatus,sReportStepName,sDetails,bExitRun
End Sub

Class CustomReporter
	Public ReportSheet
	Public ReportSheetName
	Public iIndex
	Public sName
	Public sDes
	Public sStatus
	Public sScreen
	Public sScreenShotPath
	Public clsTestCaseID
	Public tExecutionStartTime
	Public tExecutionEndTime
	Public tExecutionTimeZone
	Public tExecutionTime
	Public CurrentFunctionName
	Public FunctionCallFlow
	Public eLogFilePath
	Public cErrLogFolderPath
	Public eImageFilePath
	Public ExecutionData
	Public ExecutionEnvironment
	Public tHtmlResultFilePath
	Public TrackStatus
	Public lGroupStatus
	Public TestCasePriority
	Public tExcelFilePath
	Public clsErrDescription
	Public cErrLogName
	Public BrowserCreationTimeIndex
	Public uExpInputSuggest
'########################################################################################
   	Private Sub Class_Initialize ' this will initialize the class

		ReportSheetName="eReport"
		Set ReportSheet=DataTable.AddSheet("eReport")
'		Set SNo=ReportSheet.AddParameter("S.No","")
		Set sName=ReportSheet.AddParameter("StepName","")
		Set sDes=ReportSheet.AddParameter("Description","")
		Set sStatus=ReportSheet.AddParameter("Status","")
		Set sScreen=ReportSheet.AddParameter("ScreenShot","")
		tExecutionStartTime=Now
		
		''clsTestCaseID = Environment.Value("TestCaseID")
		''MsgBox clsTestCaseID 
		clsTestCaseID=Environment("TestName")
		iIndex=1

		eLogFilePath=CreateLogFile
        ExecutionEnvironment=GetExecutionEnvironment()
	End Sub
'########################################################################################
 Sub ReportEvent(sEventStatus,sReportStepName,sDetails,bExitRun)

		If sEventStatus="0" Then
			sEventStatus="PASS"
		Elseif sEventStatus="1" Then
			sEventStatus="FAIL"
		Elseif sEventStatus="4" Then
			sEventStatus="INFO"
		End If
		
		If LCase(sEventStatus) = "fail" And BVALUE_DEBUG_MODE Then
			Set oWSHPop = CreateObject("WScript.Shell")

			iUserSelection = oWSHPop.Popup ("There was an error occured." & vbLf & "Error Description: " & sDetails & vbLf &vbLf & "Do you want to debug the script Click on 'Yes' Button " &vbLf &vbLf &"Do you want to ''Exit'' Click on 'Cancel' Button",22,"Debug",3+32)
            If iUserSelection = 6 Then
                Stop
            End If
            
            If iUserSelection = 2 Then
                ExitTest
            End If

			Set oWSHPop = Nothing
			
		End If
		If LCase(uExpInputSuggest) = "pass" Then
			sEventStatus="PASS"
		End If
		
		ReportSheet.SetCurrentRow(iIndex)
		sName.ValuebyRow(iIndex)=sReportStepName
		sDes.ValuebyRow(iIndex)=sDetails
		sStatus.ValuebyRow(iIndex)=sEventStatus

		If ucase(sEventStatus)="FAIL" Then
			eImageFilePath=CreateImageFilePath
'			Desktop.CaptureBitmap eImageFilePath,true
			If BrowserCreationTimeIndex="" Then
				BrowserCreationTimeIndex=0
			End If
			'Browser("creationtime:="&BrowserCreationTimeIndex).CaptureBitmap eImageFilePath,True
			If Browser("creationtime:="&BrowserCreationTimeIndex).Exist(2) Then
				Browser("creationtime:="&BrowserCreationTimeIndex).CaptureBitmap eImageFilePath,True
				Else
				Desktop.CaptureBitmap eImageFilePath,True
			End If
		End If
		sScreen.ValuebyRow(iIndex)=eImageFilePath

		If TrackStatus=true Then
			If lGroupStatus="" then
				lGroupStatus=UCase(sEventStatus) 
				If lGroupStatus="DONE" Then
					lGroupStatus="PASS"
				End If
			ElseIf	lGroupStatus= "PASS" and UCase(sEventStatus) = "PASS" then
				lGroupStatus= "PASS"
			ElseIf	lGroupStatus= "PASS" and UCase(sEventStatus) = "FAIL" then
				lGroupStatus= "FAIL"
			End If
		End If
		If UCase(sEventStatus) = "PASS" Then 
			QTPReporter.ReportEvent MicPass,sReportStepName,sDetails
		ElseIf UCase(sEventStatus) = "FAIL" Then
			QTPReporter.ReportEvent MicFail,sReportStepName,sDetails
			If bExitRun = True Or bExitRun = 1 Then
				clsErrDescription=Err.Description
				ExitRun
			End If 
		End If 
	iIndex=iIndex+1
	eImageFilePath=""
	End Sub
'########################################################################################
	Function QTPRunStatus()    
               dtRowCount=ReportSheet.GetRowCount
               dtColumnCount=ReportSheet.GetParameterCount
               sWarningStatus = ""
               sFailStatus = ""
               For dtIndex=1 to dtRowCount
                              dtStatus=UCase(sStatus.ValuebyRow(dtIndex))
                              If dtStatus = "PASS" Then
                                             QTPRunStatus = "PASS"
                              ElseIf dtStatus = "FAIL" Then
                                             sFailStatus  = "FAIL"
                              ElseIf dtStatus = "WARNING" Then
                                             sWarningStatus = "PASS WITH WARNING"            
                              End If            
               Next
               If sFailStatus  = "FAIL" Then
                              QTPRunStatus = "FAIL" 
               ElseIf sWarningStatus = "PASS WITH WARNING" and QTPRunStatus = "PASS" Then
                              QTPRunStatus = "PASS WITH WARNING"
               ElseIf sWarningStatus = "" and QTPRunStatus = "PASS" Then
                              QTPRunStatus = "PASS"                
               End If
End Function

'########################################################################################
	Function BuildFolderPath(byval fldPath)
		Dim bFso
		Dim PathArray
		Dim rfldPath
		Dim pIndex
		Dim fPath
		
		Set bFso=CreateObject("scripting.filesystemobject")
	
		If instr(1,fldPath,"\\")=1 Then
			rfldPath=mid(fldPath,3,len(fldPath)-2)
			PathArray=Split(rfldPath,"\")
			fPath="\\"&PathArray(0)
		Else
			PathArray=Split(fldPath,"\")
			fPath=PathArray(0)
		End If
	
			For pIndex=1 to ubound(PathArray)
		
			fPath=fPath&"\"&PathArray(pIndex)
		
				If not bFso.FolderExists(fPath) then
					bFso.CreateFolder(fPath)
				End If

			Next
		
		Set bFso=nothing
	
	End Function
'########################################################################################
	Function CreateLogFile()
		cErrLogName = clsTestCaseID&"-"&Replace(Replace(Replace(Now,"/",""),":","")," ","-")
		cErrLogFolderName=clsTestCaseID&"-"&replace(date,"/","_")
'		cErrLogFolderPath=TEMP_DIR&"\"&cErrLogName
	
		cErrLogFolderPath=TEMP_DIR&cErrLogFolderName
	
		BuildFolderPath cErrLogFolderPath
		CreateLogFile=cErrLogFolderPath&"\"&cErrLogName&".txt"
	End Function
'########################################################################################
	Function CreateImageFilePath()
				CreateImageFilePath=cErrLogFolderPath&"\"&cErrLogName&"_Step"&iIndex&" Error"&".png"
	End Function
'########################################################################################
	Function CreateHtmlResultFile()
				cResFolderPath=PROJECT_DIR&"\Html Results\"&GetExecutionEnvironment&"\"&replace(date,"/","_")
				BuildFolderPath cResFolderPath
				cHTMLFileName=Environment("TestName")&"_"&replace(date,"/","_")&"_"&replace(time,":","_")&".html"
				CreateHtmlResultFile=cResFolderPath&"\"&cHTMLFileName
	End Function
'########################################################################################
	Function GenerateHTMLFile()
		'Backup reporting files
		Set hFso=CreateObject("scripting.filesystemobject")
		If hFso.FileExists(PROJECT_DIR&"Html Results\"&sTestCaseID&".html") Then
			Set oFile=hFso.GetFile(PROJECT_DIR&"Html Results\"&sTestCaseID&".html")
			sFileCreatedTime=oFile.DateLastModified
			Existfiles=sTestCaseID&sFileCreatedTime
			Existfiles=Replace(Existfiles,"/","")
			Existfiles=Replace(Existfiles,"\","")
			Existfiles=Replace(Existfiles,"-","")
			Existfiles=Replace(Existfiles,":","")
			Existfiles=Replace(Existfiles," ","_")
		End If	
		
		If  hFso.FileExists(PROJECT_DIR&"Html Results\"&sTestCaseID&".html") then
		      hFso.MoveFile PROJECT_DIR&"Html Results\"&sTestCaseID&".html",PROJECT_DIR&"Html Results\"&Existfiles&".html"
		End If
		tHtmlResultFilePath=PROJECT_DIR&"Html Results\"&sTestCaseID&".html" 
		Set objHtmlFile=hFso.CreateTextFile(tHtmlResultFilePath)
	
		clsResultName=clsTestCaseID
		clsUser=Environment("UserName")
		'tExecutionTimeZone=GetTimeZone
		tExecutionEndTime=Now
		tExecutionTime=ScriptExecutionTimeInSeconds(tExecutionStartTime,tExecutionEndTime)

		'Write HTML Header
		objHtmlFile.WriteLine ("<html><body  bgcolor=	#E6F0B8>")
		objHtmlFile.WriteLine ("<table align=center width=1600 style=""font-family: Georgia, Arial;"" border=0><tr><td align=center bgcolor=#4A9586><font color=white><b>"&clsResultName&" Test Results</b></font></td></tr></table>")
		objHtmlFile.WriteLine ("<br>")
		objHtmlFile.WriteLine ("<table align=center width=1600 border=0>")
		'Row1 (Test ScriptId, Start Time)
		objHtmlFile.WriteLine ("<tr><td bgcolor=#4A9586 ><font color=white><b>Test Script ID:</b></font></td>")
		objHtmlFile.WriteLine ("<td bgcolor=#D6C485><font color=#000000> "&clsTestCaseID&"</font></td>")
		objHtmlFile.WriteLine ("<td bgcolor=#4A9586 ><font color=white><b>Start Time</b></font></td>")
		objHtmlFile.WriteLine ("<td bgcolor=#D6C485><font color=#000000>"&tExecutionStartTime&" "& tExecutionTimeZone&"</font></td></tr>")
		'Row2 (Environment, End Time)
		objHtmlFile.WriteLine ("<tr><td bgcolor=#4A9586 ><font color=white><b>Environment</b></font></td>")
		objHtmlFile.WriteLine ("<td bgcolor=#D6C485><font color=#000000>" &ExecutionEnvironment&"</font></td> ") 
		objHtmlFile.WriteLine ("<td bgcolor=#4A9586 ><font color=white><b>End Time </b></font></td>")
		objHtmlFile.WriteLine ("<td id=""endtime"" bgcolor=#D6C485><font color=#000000>"&tExecutionEndTime&" "& tExecutionTimeZone&"</font></td></tr>")
		'Row3 (Module Name, Execution Time)
		objHtmlFile.WriteLine ("<tr><td bgcolor=#4A9586 ><font color=white><b>Module Name </b></font></td>")
		objHtmlFile.WriteLine ("<td bgcolor=#D6C485><font color=#000000>"&MODULE_NAME&"</font></td>")
		objHtmlFile.WriteLine ("<td bgcolor=#4A9586 ><font color=white><b>Execution Time </b></font></td>")
		objHtmlFile.WriteLine ("<td id=""etime"" bgcolor=#D6C485><font color=#000000>"&tExecutionTime&" Sec</font></td></tr>")
		'Row4 (Project Name, Execution Status)
		objHtmlFile.WriteLine ("<tr><td bgcolor=#4A9586 ><font color=white><b>Project Name</b></font></font></td>")
		objHtmlFile.WriteLine ("<td bgcolor=#D6C485><font color=#000000>R+F</font></td>")
		objHtmlFile.WriteLine ("<td bgcolor=#4A9586 ><font color=white><b>Execution Status</b></font></font></td>")
		
			Select case QTPRunStatus
				case "WARNING"
					fColor="FF9900"
				Case "DONE"
					fColor="#000000"
				case "FAIL"
					fColor="CC0000"
				case "PASS"
					fColor="#000000"
			End Select
		objHtmlFile.WriteLine ("<td id=""status"" bgcolor=#D6C485><font color="&fColor&"><b>"&QTPRunStatus&"</b></font></td></tr>")
		
		'Row5 (Machine Name,Browser Version)
		objHtmlFile.WriteLine ("<tr><td bgcolor=#4A9586 ><font color=white><b>System Name </b></font></td>")
		objHtmlFile.WriteLine ("<td bgcolor=#D6C485><font color=#000000>"&Environment("LocalHostName")&"</font></td>")

		objHtmlFile.WriteLine ("<td bgcolor=#4A9586 ><font color=white><b>Client </b></font></td>")
		objHtmlFile.WriteLine ("<td id=""etime"" bgcolor=#D6C485><font color=#000000>"&EXECUTION_Client&" </font></td></tr>")
		objHtmlFile.WriteLine ("<td bgcolor=#E6F0B8><font color=#E6F0B8></font></td></tr></table><p> </p> ")
		REM 'Row6 (Scenarios IDs)
		REM objHtmlFile.WriteLine ("<td bgcolor=#4A9586 ><font color=white><b>Scenarios IDs</b></font></td>")
		REM objHtmlFile.WriteLine ("<td bgcolor=#D6C485><font color=#000000>"&ScenariosIDs&"</font></td></tr></table><p> </p> ")
		
		'Result Columns
		objHtmlFile.WriteLine ("<table align=center  id="&chr(34)&clsTestCaseID&chr(34)&" style=""font-family: Georgia, Arial;"" width= 1600>")
		objHtmlFile.WriteLine ("<tr bgcolor=#4A9586>")
		objHtmlFile.WriteLine ("<td><font color=white><b>S.No</b></font></td>")
		objHtmlFile.WriteLine ("<td><font color=white><b>Step Name</b></font></td>")
		objHtmlFile.WriteLine ("<td><font color=white><b>Description</b></font></td>")
		objHtmlFile.WriteLine ("<td><font color=white><b>Status</b></font></td>")
		objHtmlFile.WriteLine ("<td><font color=white><b>ScreenShot</b></font></td></tr>")
		
		dtRowCount=ReportSheet.GetRowCount
		dtColumnCount=ReportSheet.GetParameterCount
		
		For dtIndex=1 to dtRowCount

			ReportSheet.SetCurrentRow(dtIndex)
			dtStepName=sName.ValuebyRow(dtIndex)
			dtDescription=sDes.ValuebyRow(dtIndex)
			dtStatus=UCase(sStatus.ValuebyRow(dtIndex))
			dtScreen=sScreen.ValuebyRow(dtIndex)

				Select case dtStatus
					case "WARNING"
						reportColor="FF9900"
					Case "DONE"
						reportColor="#000000"
					case "FAIL"
						reportColor="CC0000"
					case "PASS"
						reportColor="#000000"
					case "INFO"
						reportColor="#CCCCCC"
				End Select
		If InStr(1,dtDescription,"Began")=1 Then

			HTMLStepStart="<TR bgColor=#99CCFF>"
			HTMLStepIndex="<td align='center' ><font color=#7A297A>"&dtIndex&"</font></td>"
			HTMLStepName="<td ><font color=#7A297A>"&dtStepName&"</font></td>"
			HTMLStepDescription="<td ><font color=#7A297A>"&dtDescription&"</font></td>"
			HTMLStepStatus="<td ><font color=#7A297A>"&dtStatus&"</font></td>"
			HTMLStepScreenPath="<td><font color=#7A297A>-</font></td>"
		
		Else

		
			HTMLStepStart="<TR bgColor=#D6C485>"
			HTMLStepIndex="<td align='center' bgcolor='#D6C485'><font color="&reportColor&">"&dtIndex&"</font></td>"
			HTMLStepName="<td bgcolor='#D6C485'><font color="&reportColor&">"&dtStepName&"</font></td>"
			HTMLStepDescription="<td bgcolor=#D6C485><font color="&reportColor&">"&dtDescription&"</font></td>"
			HTMLStepStatus="<td bgcolor=#D6C485><font color=	"&reportColor&">"&dtStatus&"</font></td>"
	
			If dtScreen<>"" Then
				'RelImgPath=replace(dtScreen,PROJECT_DIR,"file:../../../")
				RelImgPath=replace(dtScreen,PROJECT_DIR,"file://"&PROJECT_DIR)
				RelImgPath=Replace(RelImgPath,"\","/")
				HTMLStepScreenPath="<td bgcolor=#D6C485><a href='"&RelImgPath&"' target=""_blank"">View Error</a></td>"
			else
				HTMLStepScreenPath="<td bgcolor=#D6C485>-</td>"
			End If
	
			HTMLStepEnd="</TR>"
		
		End If

		HTMLStep=HTMLStepStart&HTMLStepIndex&HTMLStepName&HTMLStepDescription&HTMLStepStatus&HTMLStepScreenPath&HTMLStepEnd
		objHtmlFile.WriteLine(HTMLStep)
		
		Next
		
		objHtmlFile.WriteLine ("</table></body></html>")
		objHtmlFile.Close
		'Send Email of the execution status
		MailSmokeReport tHtmlResultFilePath,TEAMMEMBER_EMAILIDs
	End Function
'########################################################################################
	Function GetTimeZone()
	
		Set ws=CreateObject("WScript.Shell")
	    
	    If InStr(GetMachineVersion(),"7")>0 Then
	        IntVersion=ws.RegRead ("HKEY_LOCAL_MACHINE\SYSTEM\ControlSet001\Control\TimeZoneInformation\TimeZoneKeyName")
	    Else
	        IntVersion=ws.RegRead ("HKEY_LOCAL_MACHINE\SYSTEM\ControlSet001\Control\TimeZoneInformation\StandardName")
	    End If
	    
	    If IntVersion="Pacific Standard Time" Then
	        GetTimeZone="PST"
	    ElseIf IntVersion="India Standard Time" Then
	        GetTimeZone="IST"
	    Else
	        GetTimeZone=IntVersion
	    End If  
	   Set ws=Nothing
   End Function

'########################################################################################
	Function ScriptExecutionTimeInSeconds(StartTime,EndTime)
		StartHour = Hour(StartTime)
		StartMin = Minute(StartTime)
		StartSec = Second(StartTime)
		EndHour = Hour(EndTime)
		EndMin = Minute(EndTime)
		EndSec = Second(EndTime)
		
		StartingSeconds = (StartSec + (StartMin * 60) + (StartHour * 3600))
		EndingSeconds = (EndSec + (EndMin * 60) + (EndHour * 3600))
	
		ScriptExecutionTimeInSeconds = EndingSeconds - StartingSeconds
	End Function
'########################################################################################
	Function AppendDataToLogFile()

	Dim lFso
	' Get instance of FileSystemObject.
	Set lFso = CreateObject("Scripting.FileSystemObject")
	Set lMyFile = lFso.CreateTextFile (eLogFilePath, True)
	lMyFile.WriteLine("*************************"&clsTestCaseID&"*************************")

	dtRowCount=ReportSheet.GetRowCount
	dtColumnCount=ReportSheet.GetParameterCount
	
	For dtIndex=1 to dtRowCount

		ReportSheet.SetCurrentRow(dtIndex)
		dtStepName=sName.ValuebyRow(dtIndex)
		dtDescription=sDes.ValuebyRow(dtIndex)
		dtStatus=sStatus.ValuebyRow(dtIndex)
		dtScreen=sScreen.ValuebyRow(dtIndex)
		lMyFile.WriteLine ("Step-"& CInt(dtIndex) &vbTab & dtStatus&vbTab & dtDescription & vbTab & Time )

		If UCase(dtStatus) = "FAIL" Or clsErrDescription<>"" Then
			If CurrentFunctionName<>"" Then
				ErrFunction=vbnewline&"Error at Function: "&CurrentFunctionName
				lMyFile.WriteLine ErrFunction
			ElseIf clsErrDescription<>"" Then
				lMyFile.WriteLine clsErrDescription			
			End If
		End If

	Next

	lMyFile.Close

	Set lFso=Nothing
	Set lMyFile=Nothing

	End Function
'########################################################################################
	Function StoreTestdata(eData)
		If not isobject(eData) Then
			   eArray=Split(eData,",")
				For each edValue in eArray
					If  ExecutionData<>"" Then
						ExecutionData=ExecutionData&vbnewline&edValue
					Else
						ExecutionData=edValue
					End If
				Next
		Else
	
			dKeys=eData.Keys
			dItems=eData.Items
	
			For dItemCount=0 to eData.Count-1
					If  ExecutionData<>"" Then
						ExecutionData=ExecutionData&vbnewline&dKeys(dItemCount)&":"&dItems(dItemCount)
					Else
						ExecutionData=dKeys(dItemCount)&":"&dItems(dItemCount)
					End If
			Next
				
		End If
	
	End Function
'########################################################################################
	Function WriteTestResultsIntoExcel ()
	'SystemUtil.CloseProcessByName "EXCEL.EXE"
	err.clear
	On error resume next

	Set fs=CreateObject("Scripting.FileSystemObject")
	Set objExcel=CreateObject ("Excel.Application")
	If err.description<>"" Then
		Exit Function
	End If
	On error goto 0

	bFound = False

	If (fs.FileExists(tExcelFilePath)) Then
        Set objWorkBook=objExcel.Workbooks.Open(tExcelFilePath)
		sSheetName = replace(date,"/","_")

		For i = 1 to objWorkBook.Worksheets.Count

			If  objWorkBook.Worksheets(i).Name = sSheetName Then

				Set sNewSheet=objWorkBook.Worksheets(sSheetName)
				bFound = True
				Exit for
				
            End If
		Next
		If bFound = False Then
			Set sNewSheet=objWorkBook.Worksheets.Add
			sNewSheet.Name=sSheetName
		End If
	Else
		Set objWorkBook=objExcel.Workbooks.Add
		Set sNewSheet=objWorkBook.Worksheets.Add
		sNewSheet.Name=replace(date,"/","_")
		objWorkBook.SaveAs tExcelFilePath
	End If
'======	
	'Set objSheet=objExcel.Worksheets(sSheetName)
	'aa.DisplayAlerts=False
	rc=sNewSheet.UsedRange.Rows.count
	
		Dim MyDate
		MyDate = Date   
		DateTime = Date & "  "&  Time
		
		rc=rc+1
		
		sNewSheet.Cells(1,1) = "Script ID"
		sNewSheet.Cells(1,1).Font.Bold = True
		sNewSheet.Cells(1,1).Font.ColorIndex = 1
		sNewSheet.Cells(1,1).Interior.ColorIndex=48
		
		
		sNewSheet.Cells(1,2) = "Priority"
		sNewSheet.Cells(1,2).Font.Bold = True
		sNewSheet.Cells(1,2).Font.ColorIndex = 1
		sNewSheet.Cells(1,2).Interior.ColorIndex=48
		
		sNewSheet.Cells(1,3) = "Run Status"
		sNewSheet.Cells(1,3).Font.Bold = True
		sNewSheet.Cells(1,3).Font.ColorIndex = 1
		sNewSheet.Cells(1,3).Interior.ColorIndex=48
		
		sNewSheet.Cells(1,4) = "Execution Time"
		sNewSheet.Cells(1,4).Font.Bold = True
		sNewSheet.Cells(1,4).Font.ColorIndex = 1
		sNewSheet.Cells(1,4).Interior.ColorIndex=48
	
		sNewSheet.Cells(1,5) = "DateTime"
		sNewSheet.Cells(1,5).Font.Bold = True
		sNewSheet.Cells(1,5).Font.ColorIndex = 1
		sNewSheet.Cells(1,5).Interior.ColorIndex=48
		sNewSheet.Cells(1,5).Interior.ColorIndex=48
	
		sNewSheet.Cells(1,6) = "TestData"
		sNewSheet.Cells(1,6).Font.Bold = True
		sNewSheet.Cells(1,6).Font.ColorIndex = 1
		sNewSheet.Cells(1,6).Interior.ColorIndex=48
	
		sNewSheet.Cells(1,7) = "Execution Environment"
		sNewSheet.Cells(1,7).Font.Bold = True
		sNewSheet.Cells(1,7).Font.ColorIndex = 1
		sNewSheet.Cells(1,7).Interior.ColorIndex=48
	
		sNewSheet.Cells(1,8) = "DetailedResult"
		sNewSheet.Cells(1,8).Font.Bold = True
		sNewSheet.Cells(1,8).Font.ColorIndex = 1
		sNewSheet.Cells(1,8).Interior.ColorIndex=48
	
	' Store Test Name in Excel
		sNewSheet.Cells(rc,1).value=Environment.Value("TestName")
	'sNewSheet.Cells(rc, 1).Font.Bold = TRUE
		iStatus= QTPRunStatus

	'Specify Priority
		sNewSheet.Cells(rc,2).value=TestCasePriority

	' Store Test status in Excel	
		If iStatus = "FAIL"  Then
			sNewSheet.Cells(rc,3).Font.Bold = TRUE
			'Font color to Red
			'sNewSheet.Cells(rc, 2).Font.ColorIndex = 1
			sNewSheet.Cells(rc, 3).Interior.ColorIndex=3
			
			sNewSheet.Cells(rc,3).value=iStatus
		Else
			sNewSheet.Cells(rc,3).Font.Bold = True
			'Font green color
			'sNewSheet.Cells(rc, 2).Font.ColorIndex = 4
			'Fill background color to green
			sNewSheet.Cells(rc, 3).Interior.ColorIndex=4
			sNewSheet.Cells(rc,3).value=iStatus
		End If
	
	' Store Execution Time	
		sNewSheet.Cells(rc,4) = tExecutionTime
	
	' Store Date and Time
		sNewSheet.Cells(rc,5) = DateTime
	
	' Store Execution Data
		sNewSheet.Cells(rc,6) = ExecutionData
	
	' Store Execution Environment
		sNewSheet.Cells(rc,7) = ExecutionEnvironment
	
	' Specify Detailed Result Link
		sNewSheet.Cells(rc,8) = "View Result"
		sNewSheet.Cells(rc,8).select
		sNewSheet.Hyperlinks.Add objExcel.selection,tHtmlResultFilePath
	
		sNewSheet.Columns("A:A").EntireColumn.AutoFit
		sNewSheet.Columns("B:B").EntireColumn.AutoFit
		sNewSheet.Columns("C:C").EntireColumn.AutoFit
		sNewSheet.Columns("D:D").EntireColumn.AutoFit
		sNewSheet.Columns("E:E").EntireColumn.AutoFit
		sNewSheet.Columns("F:F").EntireColumn.AutoFit
		sNewSheet.Columns("G:G").EntireColumn.AutoFit
		sNewSheet.Columns("H:H").EntireColumn.AutoFit

		objWorkBook.Save
		objExcel.Quit
			
		Set objExcel=Nothing
		Set objSheet = Nothing
		Set sSheetName = Nothing
		Set objWorkBook= nothing
	End Function
'########################################################################################
	Function GetExecutionEnvironment()
	   EnvArray=split(EXECUTION_ENVIRONMENT,"-")
		GetExecutionEnvironment= EnvArray(ubound(EnvArray))
	End Function
'########################################################################################
	Function StartStatusTrack()
		TrackStatus=True
		lGroupStatus=""
	End Function
'########################################################################################
	Function EndStatusTrack()
		TrackStatus=False
	End Function
'########################################################################################
	Private Sub Class_Terminate
		
		AppendDataToLogFile()
			
		If UCase(QTPRunStatus) = "FAIL" Or clsErrDescription<>"" Then
			If CurrentFunctionName<>"" Then
				ErrFunction="Error at Function: "&CurrentFunctionName
				ReportEvent "Fail","Function Failure",ErrFunction,INDEX_VALUE_ZERO
				
			ElseIf clsErrDescription<>"" Then
				ReportEvent "Fail","Script Failure",clsErrDescription,INDEX_VALUE_ZERO
				
			ElseIf Err.Description<>"" Then
				ReportEvent "Fail","Script Failure",Err.Description,INDEX_VALUE_ZERO
			End If
		End If
		
		If UCase(GENERATE_HTML_REPORT)="TRUE" Then
			GenerateHTMLFile()
			'WriteTestResultsIntoExcel ()
		End If
		
		DataTable.DeleteSheet "eReport"
	End Sub
'########################################################################################
Function GetBrowserVersion()

If BROWSER_NAME="IE" Then
   
    Set ws=CreateObject("WScript.Shell")
    IntVersion=ws.RegRead ("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Internet Explorer\Version")
    IntVersion= Mid(IntVersion,1,3)
    IntVersion=BROWSER_NAME&" "&IntVersion&".x"
    GetBrowserVersion=IntVersion
    Set ws=Nothing

ElseIf BROWSER_NAME="FF" Then

	Set ws=CreateObject("WScript.Shell")
	IntVersion=ws.RegRead ("HKEY_LOCAL_MACHINE\SOFTWARE\Mozilla\Mozilla Firefox\CurrentVersion")
	IntVersion= Mid(IntVersion,1,3)
	IntVersion=BROWSER_NAME&" "&IntVersion&".x"
	GetBrowserVersion=IntVersion
	Set ws=Nothing
End If

End Function
'########################################################################################
Function GetMachineVersion()

Set objWMIService = GetObject("winmgmts:{impersonationLevel=impersonate}!\\.\root\cimv2")
 
Set colOSes = objWMIService.ExecQuery("Select * from Win32_OperatingSystem")

For Each objOS in colOSes
  
  If InStr(UCase(objOS.Caption),"XP")<>0 Then
  	OSName="WinXP"
  ElseIf InStr(UCase(objOS.Caption),"VISTA")<>0 Then
  	OSName="Win Vista"
  ElseIf InStr(UCase(objOS.Caption),"7")<>0 Then
  	OSName="Windows 7"
  Else
  	OSName=objOS.Caption
  End If
  
  spVersion=objOS.ServicePackMajorVersion
Next

OSName=OSName& " SP"& spVersion
GetMachineVersion=OSName

End Function
'########################################################################################
Function StartFunction(cFunName,oSendReport)
	CurrentFunctionName=cFunName
	If LCase(oSendReport)="true" Then
		ReportEvent "Done",cFunName, "Began of "& CurrentFunctionName,0
	End If
End Function
'########################################################################################
Function EndFunction
	CurrentFunctionName=""
End Function
'########################################################################################
End Class
