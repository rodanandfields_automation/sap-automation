


Function fnOTCStandard_Flow ()
	'@ Help                    :
	'@ Group                   : Common
	'@ Method Name             : fnOTCStandard_Flow(oDetails)
	'@ Pre - Condition/Screen  : Data should be there in accessdb
	'@ Purpose                 : Processing all standard orders
	'@ Paramerters	           : None
	'@ Returns                 : None

	'Start Reporting 
	Reporter.StartStatusTrack	
	Reporter.StartFunction "fnOTCFlow standard order","True"
	''Loggin in to SAP system
	SAPLogon ()
	DataTable.Import (pFolderPath & "\R_and_F\Data\OTCStandard_Flow.xls")
	''creating standard sales order
	iSO = fnVA01SO_StandardOrder ()
	Wait(2)
	VA03_DisplaySO(iSO)
	iDel = VL01N_CreateDel(iSO)
	Wait(2)
	VL02N_UpdateAdditionalData(iDel)
	Wait(2)
	iPGI = VL02N_ChangeDelevery_PGI(iDel)
	Wait(2)
	iBilling = VF01_CreateBilling(iDel)	
    'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
End Function


Function fnOTC_Flow_With_ACK_and_ASN_Mocking ()
	'@ Help                    :
	'@ Group                   : Common
	'@ Method Name             : fnOTCStandard_Flow(oDetails)
	'@ Pre - Condition/Screen  : Data should be there in accessdb
	'@ Purpose                 : Processing all standard orders
	'@ Paramerters	           : None
	'@ Returns                 : None
	
	sDate = Date
	sYear = Year(sDate)
	sMonth = month(sDate)
	If len(sMonth) = 1 Then
	sMonth = "0"& month(sDate)	
	End If
	sDay = Day(sDate)
	If len(sDay) = 1 Then
	sDay = "0"&Day(sDate)	
	End If
	sReqDate = sYear & sMonth & sDay
	
	'Start Reporting 
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Standard OTC Flow With ACK and ASN Mocking","True"
'	
	SAPLogon ()
	''creating standard sales order
	iSO = fnVA01SO_StandardOrder ()
	Wait(5)
	VA03_DisplaySO(iSO)
	iDel = VL01N_CreateDel(iSO)
	Wait(2)
	WE19_Mock_ACK_Process "47012458", iDel, "10", sReqDate, ""
	Wait(2)
	VL03N_ValidateDeliveryStatus "", "10", iDel
	WE19_Mock_ACK_Process "47012458", iDel, "20", sReqDate, ""
	Wait(2)
	VL03N_ValidateDeliveryStatus "", "20", iDel
	WE19_Mock_ASN_Process "47012459", iDel, sReqDate
	Wait(2)
	VL03N_ValidateDeliveryStatus ucase("AutomationText"), "20", iDel
	VL03N_ValidatePickingRequest (iDel)	
	iPGI = VL02N_ChangeDelevery_PGI(iDel)
	If isnumeric(iPGI) Then
		Wait(5)
		VL03N_ValidateDeliveryOutput (iDel)
		iBilling = VF01_CreateBilling(iDel)	
		VF03_ValidateAccountingDocument (iBilling)	
	End If
'    'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
End Function

Function OTC_Flow_DeliveryDeletion_DebitMemoValidation (iSO)

	'Start Reporting 
	Reporter.StartStatusTrack	
	Reporter.StartFunction "ProcessInbound IDocs And Process SalesOrder","True"
'	SAPLogon ()
	BD87_Process_IndividualIDocs iSO
	VA03_DisplaySO iSO
	
	statusVA03 = oControls.GetSAPStatusBarInfo("messagetype")
	
	If statusVA03 = "E" Then
		oControls.UpdateExecutionReport micFail, "Order Validation", "Sales order " & iSO & " didnot reach ECC Server"
	Else
		oControls.UpdateExecutionReport micPAss, "Order Validation", "Sales order " & iSO & " reach ECC Server"
		iDel = VL01N_CreateDel(iSO)
		Wait(2)
		WE19_Mock_ACK_Process "64316006", iDel, "99", sReqDate, "RF"
'		Wait(2)
		Update_OrderStatus ()
		Wait(2)
		VA03_ValidateDeliveryCancellation_and_DebitMemoCreation(iSO)
	End  If
	Reporter.EndFunction
	Reporter.EndStatusTrack
	
End Function


Function Process_Inbound_IDocs_And_Validate_SalesOrder ()
	SAPLogon ()
	BD87_ProcessIDocs ()
	Wait(5)
	VA02_UpdateSO("1220272039")
	Wait(2)
	VA03_GetDelNum("1220272039")
End Function

Function Process_Inbound_IDocs_And_Process_SalesOrder(arrCSVData)
	
	sDate = Date
	sYear = Year(sDate)
	sMonth = month(sDate)
	If len(sMonth) = 1 Then
	sMonth = "0"& month(sDate)	
	End If
	sDay = Day(sDate)
	If len(sDay) = 1 Then
	sDay = "0"&Day(sDate)	
	End If
	sReqDate = sYear & sMonth & sDay
	
	'Start Reporting 
	Reporter.StartStatusTrack	
	Reporter.StartFunction "ProcessInbound IDocs And Process SalesOrder","True"
	
	SAPLogon ()
	Wait(5)
	For iCurVal = 1 To UBound(arrCSVData)
		BD87_Process_IndividualIDocs Split(arrCSVData(iCurVal),",")(1)
		VA03_DisplaySO Split(arrCSVData(iCurVal),",")(1)
		
		statusVA03 = oControls.GetSAPStatusBarInfo("messagetype")
		If statusVA03 = "E" Then
			oControls.UpdateExecutionReport micFail, "Order Validation", "Sales order " & Split(arrCSVData(iCurVal),",")(1) & " didnot reach ECC Server"
		Else
			oControls.UpdateExecutionReport micPAss, "Order Validation", "Sales order " & Split(arrCSVData(iCurVal),",")(1) & " reach ECC Server"
			
			iDel = VL01N_CreateDel(Split(arrCSVData(iCurVal),",")(1))
			Wait(2)
			WE19_Mock_ACK_Process "90022586", iDel, "10", sReqDate, ""
			Wait(2)
			WE19_Mock_ACK_Process "88724058", iDel, "20", sReqDate, ""
			Wait(2)
			WE19_Mock_ASN_Process "88724058", iDel,sReqDate, Split(arrCSVData(iCurVal),",")(1)
			Wait(2)
			VL03N_ValidatePickingRequest (iDel)	 
			iPGI = VL02N_ChangeDelevery_PGI(iDel)

			If IsNumeric(iPGI) Then
				Wait(5)
				Update_OrderStatus
				intIDocNumber = SE16N_ReadIDocNumber (Split(arrCSVData(1),",")(1))
				strOrderStausDetails = WE02_ReadOrderStatusDetails(intIDocNumber)
				iBilling = VF01_CreateBilling(iDel)	
				VF03_ValidateAccountingDocument (iBilling)	
			End If
		End If
		strSalesOrderData = strSalesOrderData & arrCSVData(iCurVal) & "," & strOrderStausDetails & vbCrLf
	Next
	Process_Inbound_IDocs_And_Process_SalesOrder = strSalesOrderData
	
    'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
			

End  Function


Function Process_Inbound_IDocs_And_Process_SalesOrder_OR(aOrderArray)
	
	sDate = Date
	sYear = Year(sDate)
	sMonth = month(sDate)
	If len(sMonth) = 1 Then
	sMonth = "0"& month(sDate)	
	End If
	sDay = Day(sDate)
	If len(sDay) = 1 Then
	sDay = "0"&Day(sDate)	
	End If
	sReqDate = sYear & sMonth & sDay
	
	'Start Reporting 
	Reporter.StartStatusTrack	
	Reporter.StartFunction "ProcessInbound IDocs And Process SalesOrder","True"
'	
	SAPLogon ()
'	BD87_ProcessIDocs ()
'	Wait(5)
	For iCurVal = 0 To ubound(aOrderArray)
'		BD87_Process_IndividualIDocs aOrderArray(iCurVal,8)
		VA03_DisplaySO aOrderArray(iCurVal,8)
'			
'		statusVA03 = oControls.GetSAPStatusBarInfo("messagetype")
'		If statusVA03 = "E" Then
'			oControls.UpdateExecutionReport micFail, "Order Validation", "Sales order " & aOrderArray(iCurVal,8) & " didnot reach ECC Server"
'		Else
'			oControls.UpdateExecutionReport micPAss, "Order Validation", "Sales order " & aOrderArray(iCurVal,8) & " reach ECC Server"
			'VA03_ValidateSOInfo aOrderArray(iCurVal,8), aOrderArray(iCurVal,7)
			
			iDel = VL01N_CreateDel(aOrderArray(iCurVal,8))'"9000507644"'
			Wait(2)
			WE19_Mock_ACK_Process "64316006", iDel, "10", sReqDate, ""
			Wait(2)
''			VL03N_ValidateDeliveryStatus "", "10", iDel
			WE19_Mock_ACK_Process "64316006", iDel, "20", sReqDate, ""
			Wait(2)
'''			VL03N_ValidateDeliveryStatus "", "20", iDel
			WE19_Mock_ASN_Process "67517380", iDel,sReqDate, aOrderArray(iCurVal,8)
			Wait(2)
''			VL03N_ValidateDeliveryStatus ucase("TEST"), "20", iDel
''			VL03N_ValidatePickingRequest (iDel)	
			iPGI = VL02N_ChangeDelevery_PGI(iDel)
'''			If isnumeric(iPGI) Then
'''				Wait(5)
'''				VL03N_ValidateDeliveryOutput (iDel)
				iBilling = VF01_CreateBilling(iDel)	
'''				VF03_ValidateAccountingDocument (iBilling)	
'''			End If
''				
''		End If		
	Next
'    'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
			

End  Function


Function Order_Validation ()
	'@ Help                    :
	'@ Group                   : Common
	'@ Method Name             : fnOTCStandard_Flow(oDetails)
	'@ Pre - Condition/Screen  : Data should be there in accessdb
	'@ Purpose                 : Processing all standard orders
	'@ Paramerters	           : None
	'@ Returns                 : None

	'Start Reporting 
	Reporter.StartStatusTrack	
	Reporter.StartFunction "OrderValidation","True"
	''Loggin in to SAP system
	SAPLogon ()
	''creating standard sales order
	
	Call OrderValidation ()

    'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
End Function




Function Delivery_Validation ()
	'@ Help                    :
	'@ Group                   : Common
	'@ Method Name             : fnOTCStandard_Flow(oDetails)
	'@ Pre - Condition/Screen  : Data should be there in accessdb
	'@ Purpose                 : Processing all standard orders
	'@ Paramerters	           : None
	'@ Returns                 : None

	'Start Reporting 
	Reporter.StartStatusTrack	
	Reporter.StartFunction "DeliveryValidation","True"
	''Loggin in to SAP system
	SAPLogon ()
	''creating standard sales order
	
	Call DeliveryValidation()

    'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
End Function


Function OrderType_Validation ()
	'@ Help                    :
	'@ Group                   : Common
	'@ Method Name             : fnOTCStandard_Flow(oDetails)
	'@ Pre - Condition/Screen  : Data should be there in accessdb
	'@ Purpose                 : Processing all standard orders
	'@ Paramerters	           : None
	'@ Returns                 : None

	'Start Reporting 
	Reporter.StartStatusTrack	
	Reporter.StartFunction "OrderTypeValidation","True"
	''Loggin in to SAP system
	SAPLogon ()
	''creating standard sales order
	
	Call OrderTypeValidation()

    'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
End Function




Function ReturnOrderValidation ()
	'@ Help                    :
	'@ Group                   : Common
	'@ Method Name             : fnOTCStandard_Flow(oDetails)
	'@ Pre - Condition/Screen  : Data should be there in accessdb
	'@ Purpose                 : Processing all standard orders
	'@ Paramerters	           : None
	'@ Returns                 : None

	'Start Reporting 
	Reporter.StartStatusTrack	
	Reporter.StartFunction "ReturnOrderValidation","True"
	''Loggin in to SAP system
	SAPLogon ()
	DataTable.Import (pFolderPath & "\R_and_F\Data\OTCStandard_Flow.xls")
	''creating standard sales order
	iDel = OrderTypeValidation ()
	Wait(2)
	iBilling = VF01_CreateBilling(iDel)	
    'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
End Function




Function InboundIDoc_OTC_ValidateSO(aOrderArray)
	
	sDate = Date
	sYear = Year(sDate)
	sMonth = month(sDate)
	If len(sMonth) = 1 Then
	sMonth = "0"& month(sDate)	
	End If
	sDay = Day(sDate)
	If len(sDay) = 1 Then
	sDay = "0"&Day(sDate)	
	End If
	sReqDate = sYear & sMonth & sDay
	
	'Start Reporting 
	Reporter.StartStatusTrack	
	Reporter.StartFunction "ProcessInbound IDoc, Validate SalesOrder and process it till billing","True"

	SAPLogon ()
	For iCurVal = 0 To ubound(aOrderArray)	
		BD87_Process_IndividualIDocs aOrderArray(iCurVal,8)
		Wait(2)
		OrderValidation ()
		iDel = VL01N_CreateDel(aOrderArray(iCurVal,8))
		Wait(2)
		WE19_Mock_ACK_Process "64316006", iDel, "10", sReqDate, ""
		Wait(2)
''		VL03N_ValidateDeliveryStatus "", "10", iDel
		WE19_Mock_ACK_Process "64316006", iDel, "20", sReqDate, ""
		Wait(2)
''		VL03N_ValidateDeliveryStatus "", "20", iDel
		WE19_Mock_ASN_Process "67517380", iDel,sReqDate, aOrderArray(iCurVal,8)
		Wait(2)
''		VL03N_ValidateDeliveryStatus ucase("TEST"), "20", iDel
''		VL03N_ValidatePickingRequest (iDel)	
		iPGI = VL02N_ChangeDelevery_PGI(iDel)
'		If len(trim(iPGI)) > 0 Then
'		Wait(2)
		Update_OrderStatus ()
''		VL03N_ValidateDeliveryOutput (iDel)
		iBilling = VF01_CreateBilling(iDel)
'		wait(2)
'		VF03_ValidateAccountingDocument (iBilling)	
'		End If	
	Next
'    'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack

End  Function
