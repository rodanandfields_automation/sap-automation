

Function fnP2PStandard_Flow ()
	'@ Help                    :
	'@ Group                   : Common
	'@ Method Name             : fnP2PStandard_Flow()
	'@ Pre - Condition/Screen  : Data should be there in accessdb
	'@ Purpose                 : Processing all standard orders
	'@ Paramerters	           : None
	'@ Returns                 : None

	'Start Reporting 
	Reporter.StartStatusTrack
	Reporter.StartFunction "fnOTCFlow standard order","True"
	
	'Loggin in to SAP system
	SAPLogon ()
	DataTable.Import (pFolderPath & "\R_and_F\Data\P2PStandard_Flow.xls")
	
	'Create Purchase Order
	strPONumber = fnME21N_CreatePurchaseOrder()
	
	''strPONumber = "4500008641"
	'Display Purchase Order
	fnME29N_ReleasePurchaseOrder(strPONumber)
	
	'Goods Receipt
	strMaterialDocument = fnMIGO_GoodsReceipt(strPONumber)
	
	'Display Material Document
	fnMB03_DisplayMaterialDocument (strMaterialDocument)
	
	'Invoice Receipt
	strInvoiceReceiptDocument = fnMIROInvoiceReceipt(strPONumber)
	
	'Display Invoice Document
	fnMIR4DisplayInvoiceDocument strInvoiceReceiptDocument
	
    'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
	
End Function



Function fnME21N_CreatePurchaseOrder()

	''Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Creating Purchase Order","True"
	
	'Entering T code
	oControls.SetSAPOKCode ("/nME21N")
	
	'Select PO Type
	oControls.SelectSAPComboBoxKey "MEPO_TOPLINE-BSART", DataTable.Value("POType") 
	
	'Enter Vendor
	oControls.SetSAPEdit "MEPO_TOPLINE-SUPERFIELD", DataTable.Value("Vendor") 
	
	'Enter Org.Date
	oControls.SetSAPEdit "MEPO1222-EKORG", DataTable.Value("PurchOrg") 
	oControls.SetSAPEdit "MEPO1222-EKGRP", DataTable.Value("PurchGroup") 
	oControls.SetSAPEdit "MEPO1222-BUKRS", DataTable.Value("CompanyCode") 
	
	'Click Enter on Header Screen
	oControls.ClickSAPEnter ()
	
	'Read Material Data
	arrMaterialData = Split(DataTable.Value("MaterialData"),"||")
	
	'Entering Material Data in Item Overview Table
	For intArrCnt = LBound(arrMaterialData) To UBound(arrMaterialData)
		oControls.SetSAPTableCellData "SAPLMEGUITC_1211", intArrCnt + 1, "Material", Split(arrMaterialData(intArrCnt),";")(0)
		oControls.SetSAPTableCellData "SAPLMEGUITC_1211", intArrCnt + 1, "PO Quantity", Split(arrMaterialData(intArrCnt),";")(1) 
		oControls.SetSAPTableCellData "SAPLMEGUITC_1211", intArrCnt + 1, "Plnt", Split(arrMaterialData(intArrCnt),";")(2)
		oControls.SetSAPTableCellData "SAPLMEGUITC_1211", intArrCnt + 1, "Stor. Location", Split(arrMaterialData(intArrCnt),";")(3)
	Next
	
	'Click Enter on Header Screen
	oControls.ClickSAPEnter ()
	
	Wait 3
	'Click on Save Button
	oControls.SendSAPkey F11
	
	'Read & Verify PO has created or not
	strPONumber = oControls.GetSAPStatusBarInfo("item2")
	
	If IsNumeric(strPONumber) Or strPONumber <> "" Then
		fnME21N_CreatePurchaseOrder = strPONumber
		oControls.UpdateExecutionReport micPass, "PO Creation","Successfully PO has been created: '" & strPONumber & "' "
	Else  oControls.UpdateExecutionReport micFail, "PO Creation","PO creation is Failed"
	End If
	
	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
	
End function


Function fnME29N_ReleasePurchaseOrder(strPONumber)

	'Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Release Purchase Order","True"
	
	'Entering T code
	oControls.SetSAPOKCode ("/nME29N")
	
	'Navigat to created Purchase Order 
	fnOtherPurchaseOrder strPONumber
	
	'Navigat to Relase Startegy Tab
	oControls.SelectSAPTabStrip "HEADER_DETAIL", "Release strategy"
	
	'Click on Release button
	For Iterator = 1 To 4 Step 1
		oControls.ClickSAPGridCell Iterator, "Release Options"
	Next
	
	Wait(5)
	
	'Read & Verify 'Release effected with release code JA'
	strReleaseText = oControls.GetSAPStatusBarInfo("text")
	
	'Click on Save Button
	oControls.SendSAPkey F11
	Wait(5)
	
	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
	
End function


Function fnOtherPurchaseOrder(strPONumber)
	
	'Click on Other Purchase Order button in Menu
	oControls.ClickSAPButton "btn[17]"
	
	'Emter PO NUmber
	oControls.SetSAPEdit "MEPO_SELECT-EBELN", strPONumber
	
	'Click on Other Document Button in Popup window
	oControls.ClickSAPButton "btn[0]"
	
End Function


Function fnMIGO_GoodsReceipt(strPONumber)

	'Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Goods Receipt","True"
	
	'Entering T code
	oControls.SetSAPOKCode ("/nMIGO")
	
	'Get Data for Created PO 
	oControls.SelectSAPComboBox "GODYNPRO-ACTION", "Goods Receipt"
	oControls.SelectSAPComboBox "GODYNPRO-REFDOC", "Purchase Order"
	oControls.SetSAPEdit "GODYNPRO-PO_NUMBER", strPONumber
	
	'Click Enter on Header Screen
	oControls.ClickSAPEnter ()
	
	'Read Material Data to Click on Item OK Check Box
	arrMaterialData = Split(DataTable.Value("MaterialData"),"||")
	
	For intArrCnt = LBound(arrMaterialData) To UBound(arrMaterialData)
		oControls.SelectSAPCheckBox "GODYNPRO-DETAIL_TAKE", "ON"
		If intArrCnt <> 0 Then oControls.ClickSAPButton "OK_NEXT_ITEM"
	Next
	
	'Click on Check Button
	oControls.ClickSAPButton "btn[7]"
	
	'Verify 'Document is O.K.' Text
	strDocumentIsOK = oControls.GetSAPStatusBarInfo("text")
	
	'Click on Post Button
	If InStr(strDocumentIsOK,"Document is O.K.") > 0 Then oControls.ClickSAPButton "btn[23]"
	
	'Read Material Document Posted is O.K.' Text
	strMaterialDocument = oControls.GetSAPStatusBarInfo("item1")
	fnMIGO_GoodsReceipt = strMaterialDocument  ''5000126966 
	
	
	If strMaterialDocument <> "" Then
		oControls.UpdateExecutionReport micPass, "Goods Receipt:", "Goods Receipt has been created, Goods Receipt: '"&strMaterialDocument&"'"
	Else
		oControls.UpdateExecutionReport micFail, "Goods Receipt:", "Goods Receipt has not created"
	End If
	
	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
	
End  Function



Function fnMB03_DisplayMaterialDocument(strMaterialDocument)
	
	'Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Display Material Document","True"
	
	'Entering T code
	oControls.SetSAPOKCode ("/nMB03")
	
	'Emter Material Document NUmber
	oControls.SetSAPEdit "RM07M-MBLNR", strMaterialDocument
	
	'Click Enter on Header Screen
	oControls.ClickSAPEnter ()
	
	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
	
End  Function

Function fnMIROInvoiceReceipt(strPONumber)
	
	'Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Invoice Receipt","True"
	
	'Entering T code
	oControls.SetSAPOKCode ("/nMIRO")
	
	'Enter Invoice Date
	oControls.SetSAPEdit "INVFO-BLDAT", Date
	
	'Enter Reference Data
	oControls.SetSAPEdit "INVFO-XBLNR", "QATesting" & RandomNumber(1000,9999)
	
	'Enter PO Number
	oControls.SetSAPEdit "RM08M-EBELN", strPONumber
	
	'Click Enter on Header Screen
	oControls.ClickSAPEnter ()
	
	'Read Balance Amount
	strBalance = oControls.GetSAPEditValue("RM08M-DIFFERENZ")
	
	'Enter Amount
	oControls.SetSAPEdit "INVFO-WRBTR", Left(strBalance,Len(strBalance)-1)

	'Click on Save Button
	oControls.SendSAPkey F11
	
	'Click Enter on Header Screen
	''oControls.ClickSAPEnter ()
	
	If SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").SAPGuiEdit("name:=BSET-FWSTE","guicomponenttype:=31").Exist Then oControls.ClickSAPEnter ()
	
	'Read Material Document Posted is O.K.' Text
	strInvoiceReceiptDocument = oControls.GetSAPStatusBarInfo("text")
	
	fnMIROInvoiceReceipt = Trim(Split(strInvoiceReceiptDocument," ")(2))
	
	If Trim(Split(strInvoiceReceiptDocument," ")(2)) <> "" Then
		oControls.UpdateExecutionReport micPass, "Invoice Receipt:", "Invoice Receipt has been created, Invoice Receipt: '"&Trim(Split(strInvoiceReceiptDocument," ")(2))&"'"
	Else
		oControls.UpdateExecutionReport micFail, "Invoice Receipt:", "Invoice Receipt has not created"
	End If
	
	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
	
End Function

Function fnMIR4DisplayInvoiceDocument(strInvoiceReceiptDocument)

	'Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Display Invoice Receipt Document","True"
	
	'Entering T code
	oControls.SetSAPOKCode ("/nMIR4")
	
	'Enter Invoice Document No
	oControls.SetSAPEdit "RBKP-BELNR", strInvoiceReceiptDocument
	
	'Click Enter on Header Screen
	oControls.ClickSAPEnter ()
	
	'MIR4 Display Invoice Document 
	VerifySAPModalWindowExistance()
	
	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
End Function


Function VerifySAPModalWindowExistance ( )
	' @HELP
	' @class:		Controls_SAP
	' @method:		VerifySAPModalWindowExistance (sSAPTabStrip)
	' @notes:		returns modal window existance  
	' @END 
	On error resume next
	If SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=22").Exist Then
		VerifySAPModalWindowExistance = True
		sScreenName = SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=22").GetROProperty("text")
		oControls.UpdateExecutionReport micPass, "VerifySAPModalWindowExistance", "Active window is Modal Window: '"&sScreenName&"'"
		Reporter.ReportEvent micPass, "VerifySAPModalWindowExistance", "Active window is Modal Window: '"&sScreenName&"'"
	else
		VerifySAPModalWindowExistance = False
		sScreenName = SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").GetROProperty("text")
		oControls.UpdateExecutionReport micWarning, "VerifySAPModalWindowExistance", "Active window is Main Window: '"&sScreenName&"'"
	End If
	
	If err.description <> "" Then
		Reporter.ReportEvent micFail, "VerifySAPModalWindowExistance","Error Description: " & err.description
	End If
	
	Err.Clear
	On error goto 0	
End Function
