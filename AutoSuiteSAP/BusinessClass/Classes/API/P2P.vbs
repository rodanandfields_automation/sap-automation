

Function fnP2PStandard_Flow (oP2PTestData)
	'@ Help                    :
	'@ Group                   : Common
	'@ Method Name             : fnP2PStandard_Flow()
	'@ Pre - Condition/Screen  : Data should be there in accessdb
	'@ Purpose                 : Processing all standard orders
	'@ Paramerters	           : None
	'@ Returns                 : None
  
  	''DataTable.Import (pFolderPath & "\R_and_F\Data\P2P\" & strDataSheet &".xls")
	
	'Start Reporting 
	Reporter.StartStatusTrack
	Reporter.StartFunction Trim(oP2PTestData("Description")),"True"
	''Reporter.StartFunction "fnP2PFlow standard order","True"
	
	CreateFolder oP2PTestData("TestCaseID")
	Environment.Value("ScreenShotPath") = SCREENSHOT_DIR & oP2PTestData("TestCaseID")

	'Loggin in to SAP system
	SAPLogon ()
	
	'Create Purchase Order
	strPONumber = fnME21N_CreatePurchaseOrder(oP2PTestData)
	Wait 10
	
	If Trim(oP2PTestData("TestCaseID")) = "TC_R+F_PTP_03" Or Trim(oP2PTestData("TestCaseID")) = "TC_R+F_PTP_08" _ 
	Or Trim(oP2PTestData("TestCaseID")) = "TC_US_R+F_PTP_12" Or Trim(oP2PTestData("TestCaseID")) = "TC_US_R+F_PTP_17" Then
		'Create Outbound Delivery
		strOutboundDelivery = fnVL10DCreateOutboundDelivery (strPONumber)
		
		'VL02N - Post Goods Issue
		fnVL02NPostGoodsIssue strOutboundDelivery, oP2PTestData
		                      
		'MB03 - Display PGI MaterialDocument
		fnMB03DisplayPGIMaterialDocument


		'VF01 Billing Document(strOutboundDelivery)
		fnVF01BillingDocument(strOutboundDelivery)
	End If
	
	'Release Purchase Order
	If Trim(oP2PTestData("TestCaseID")) = "TC_R+F_PTP_01" Or Trim(oP2PTestData("TestCaseID")) = "TC_R+F_PTP_02" Or Trim(oP2PTestData("TestCaseID")) = "TC_R+F_PTP_07" _ 
	Or Trim(oP2PTestData("TestCaseID")) = "TC_US_R+F_PTP_10" Or Trim(oP2PTestData("TestCaseID")) = "TC_US_R+F_PTP_11" Or Trim(oP2PTestData("TestCaseID")) = "TC_US_R+F_PTP_16" _
	Or Trim(oP2PTestData("TestCaseID")) = "TC_US_R+F_PTP_18" Or Trim(oP2PTestData("TestCaseID")) = "TC_R+F_PTP_09" Then
		fnME29N_ReleasePurchaseOrder(strPONumber)
	End  IF	
	
	'Goods Receipt
	strMaterialDocument = fnMIGO_GoodsReceipt(strPONumber, oP2PTestData)
	
	'Display Material Document
	fnMB03_DisplayMaterialDocument (strMaterialDocument)
	
	'Invoice Receipt
	strInvoiceReceiptDocument = fnMIROInvoiceReceipt(strPONumber, Trim(oP2PTestData("TestCaseID")))
	Environment.Value("InvoiceReceiptDocument") = strInvoiceReceiptDocument
	'Display Invoice Document
	fnMIR4DisplayInvoiceDocument strInvoiceReceiptDocument

	'Display PO Number after completing all functionalities
	fnME23NDisplayPONumberDetails strPONumber
	
	If NOT IsEmpty(Trim(oP2PTestData("OtherLanguageLogin"))) And Trim(oP2PTestData("OtherLanguageLogin")) <> "" Then
		SAPLoginWithOterLanguage oP2PTestData
		
		'Display PO Number after completing all functionalities
		fnME23NDisplayPONumberDetailsforOtherLanguage strPONumber
		
	End If
	
	If NOT IsEmpty(Trim(oP2PTestData("ValidationData"))) And Trim(oP2PTestData("ValidationData")) <> "" Then _
		fnRunValidationPoints oP2PTestData  'Validations method calling

	
	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
	
End Function



Function fnME21N_CreatePurchaseOrder(oP2PTestData)

	''Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Creating Purchase Order","True"
	
	'Entering T code
	oControls.SetSAPOKCode ("/nME21N")
	
	'Select PO Type
	oControls.SelectSAPComboBoxKey "MEPO_TOPLINE-BSART", oP2PTestData("POType") 
	
	'Enter Vendor
	oControls.SetSAPEdit "MEPO_TOPLINE-SUPERFIELD", oP2PTestData("Vendor") 
	
	'Navigat to Org. Data Tab
	oControls.SelectSAPTabStrip "HEADER_DETAIL", "Org. Data"
		
	'Enter Org.Date
	oControls.SetSAPEdit "MEPO1222-EKORG", oP2PTestData("PurchOrg") 
	oControls.SetSAPEdit "MEPO1222-EKGRP", oP2PTestData("PurchGroup") 
	oControls.SetSAPEdit "MEPO1222-BUKRS", oP2PTestData("CompanyCode") 
	
	'Click Enter on Header Screen
	oControls.ClickSAPEnter ()
	
	If Not SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").SAPGuiTable("name:=SAPLMEGUITC_1211").Exist(5) Or _
		SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").SAPGuiGrid("guicomponenttype:=201","name:=shell").Exist Then
		SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").SAPGuiButton("name:=DYN_4000-BUTTON","index:=1").Highlight
		SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").SAPGuiButton("name:=DYN_4000-BUTTON","index:=1").Click
	End If 

	If Trim(oP2PTestData("POType")) = "ZNB"  Then
		fnEnterMaterialDatainItemsTable (oP2PTestData("MaterialData"))
	Else
		'Read Material Data
		arrMaterialData = Split(oP2PTestData("MaterialData"),"||")
		
		If Not SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").SAPGuiGrid("guicomponenttype:=201","name:=shell").Exist Then
			'Entering Material Data in Item Overview Table
			For intArrCnt = LBound(arrMaterialData) To UBound(arrMaterialData)
				oControls.SetSAPTableCellData "SAPLMEGUITC_1211", intArrCnt + 1, "Material", Split(arrMaterialData(intArrCnt),";")(0)
				oControls.SetSAPTableCellData "SAPLMEGUITC_1211", intArrCnt + 1, "PO Quantity", Split(arrMaterialData(intArrCnt),";")(1) 
				oControls.SetSAPTableCellData "SAPLMEGUITC_1211", intArrCnt + 1, "Plnt", Split(arrMaterialData(intArrCnt),";")(2)
				oControls.SetSAPTableCellData "SAPLMEGUITC_1211", intArrCnt + 1, "Stor. Location", Split(arrMaterialData(intArrCnt),";")(3)
			Next
		Else
		
			'Entering Material Data in Item Overview Table
			For intArrCnt = LBound(arrMaterialData) To UBound(arrMaterialData)
				oControls.SetSAPGridCellData intArrCnt + 1, "Material", Split(arrMaterialData(intArrCnt),";")(0)
				oControls.SetSAPGridCellData intArrCnt + 1, "Order Quantity", Split(arrMaterialData(intArrCnt),";")(1) 
				oControls.SetSAPGridCellData intArrCnt + 1, "Plant", Split(arrMaterialData(intArrCnt),";")(2)
				oControls.SetSAPGridCellData intArrCnt + 1, "Storage Location", Split(arrMaterialData(intArrCnt),";")(3)
			Next
		End If
	End If
	
	If Trim(oP2PTestData("POType")) = "YRB"  Then  _
	SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").SAPGuiTable("name:=SAPLMEGUITC_1211").Object.Rows(0).Item(22).Selected = True
	
	'Click Enter on Header Screen
	oControls.ClickSAPEnter ()
	Wait 3
	If Trim(oP2PTestData("POType")) = "ZNB"  Then
		
		'Navigat to Relase Startegy Tab
		oControls.SelectSAPTabStrip "ITEM_DETAIL", "Conditions"
		
		'Enter Amount for ZNB PO Type
		oControls.SetSAPTableCellData "SAPLV69ATCTRL_KONDITIONEN", 1, "Amount", 10
		
		'Enter Account Assignment Data
		'Navigat to Relase Startegy Tab
		oControls.SelectSAPTabStrip "ITEM_DETAIL", "Account Assignment"
		
		'Select AccAssCat
		oControls.SelectSAPComboBox "MEACCT1200-KNTTP","Cost center"
		
		'Enter G/L Account
		oControls.SetSAPEdit "MEACCT1100-SAKTO", oP2PTestData("GLAcccount")
		
		'Enter Cost Center
		oControls.SetSAPEdit "COBL-KOSTL", oP2PTestData("CostCenter")
		
	End  If		
	
	If Trim(oP2PTestData("POType")) = "YRB"  Then
		
		'Navigat to Relase Startegy Tab
		oControls.SelectSAPTabStrip "ITEM_DETAIL", "Conditions"
		
		'Enter Amount for ZNB PO Type
		oControls.SetSAPTableCellData "SAPLV69ATCTRL_KONDITIONEN", 1, "Amount", 10
		
	End  If
	
	If Trim(oP2PTestData("POType")) = "ZKT" Or Trim(oP2PTestData("TestCaseID")) = "TC_US_R+F_PTP_14" Then
		
		'Navigat to Conditions Tab
		oControls.SelectSAPTabStrip "ITEM_DETAIL", "Conditions"
		
		'Enter Amount for ZNB PO Type
		oControls.SetSAPTableCellData "SAPLV69ATCTRL_KONDITIONEN", 1, "Amount", 10
		
		'Navigat to Material Data Tab
		oControls.SelectSAPTabStrip "ITEM_DETAIL", "Material Data"
		
		'Click on BOM Button under Material Data Tab
		oControls.ClickSAPButton "BOM"
		
		'Enter Component Overview Data
		oControls.SetSAPTableCellData "SAPLM61QTC_517", 1, "Material", Split(Split(oP2PTestData("MaterialData"),"||")(0),";")(0)  'Enter Component Material as Material Name
		oControls.SetSAPTableCellData "SAPLM61QTC_517", 1, "Requirement qty", Split(Split(oP2PTestData("MaterialData"),"||")(0),";")(1) 'Enter Component Material as Material Name
		
		'Click Enter on Header Screen
		oControls.ClickSAPEnter ()
		Wait(3)
		
		'Click Enter on Header Screen
		oControls.ClickSAPEnter ()
		Wait(3)
		
		'Click on Save Button
		oControls.SendSAPkey F3
		Wait(3)
	
	End  If
	
	'Click on Save Button
	oControls.SendSAPkey F11
	Wait 3
	
	'Read & Verify PO has created or not
	strPONumber = oControls.GetSAPStatusBarInfo("item2")
	Environment.Value("PONumber") = strPONumber
	'Capture Screen Shot for Create PO 
	CaptureScreenShotAsBitmap Environment.Value("ScreenShotPath"), "fnME21N_CreatePurchaseOrder"
	
	If IsNumeric(strPONumber) Or strPONumber <> "" Then
		fnME21N_CreatePurchaseOrder = strPONumber
		oControls.UpdateExecutionReport micPass, "PO Creation","Successfully PO has been created: '" & strPONumber & "' "
	Else  oControls.UpdateExecutionReport micFail, "PO Creation","PO creation is Failed"
	End If
	
	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
	
End function


Function fnEnterMaterialDatainItemsTable(strMaterialData)
	
	'Read Material Data
	arrMaterialData = Split(strMaterialData,"||")
	
	If Not SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").SAPGuiGrid("guicomponenttype:=201","name:=shell").Exist Then
		'Entering Material Data in Item Overview Table
		For intArrCnt = LBound(arrMaterialData) To UBound(arrMaterialData)
			oControls.SetSAPTableCellData "SAPLMEGUITC_1211", intArrCnt + 1, "A", Split(arrMaterialData(intArrCnt),";")(0)
			oControls.SetSAPTableCellData "SAPLMEGUITC_1211", intArrCnt + 1, "I", Split(arrMaterialData(intArrCnt),";")(1)
			oControls.SetSAPTableCellData "SAPLMEGUITC_1211", intArrCnt + 1, "Material", Split(arrMaterialData(intArrCnt),";")(2)
			oControls.SetSAPTableCellData "SAPLMEGUITC_1211", intArrCnt + 1, "Short Text", Split(arrMaterialData(intArrCnt),";")(3)
			oControls.SetSAPTableCellData "SAPLMEGUITC_1211", intArrCnt + 1, "PO Quantity", Split(arrMaterialData(intArrCnt),";")(4) 
			oControls.SetSAPTableCellData "SAPLMEGUITC_1211", intArrCnt + 1, "OUn", Split(arrMaterialData(intArrCnt),";")(5) 
			oControls.SetSAPTableCellData "SAPLMEGUITC_1211", intArrCnt + 1, "Matl Group", Split(arrMaterialData(intArrCnt),";")(6) 
			oControls.SetSAPTableCellData "SAPLMEGUITC_1211", intArrCnt + 1, "Plnt", Split(arrMaterialData(intArrCnt),";")(7)
			oControls.SetSAPTableCellData "SAPLMEGUITC_1211", intArrCnt + 1, "Stor. Location", Split(arrMaterialData(intArrCnt),";")(8)
		Next
	Else
	
		'Entering Material Data in Item Overview Table
		For intArrCnt = LBound(arrMaterialData) To UBound(arrMaterialData)
			oControls.SetSAPGridCellData intArrCnt + 1, "A", Split(arrMaterialData(intArrCnt),";")(0)
			oControls.SetSAPGridCellData intArrCnt + 1, "I", Split(arrMaterialData(intArrCnt),";")(1)
			oControls.SetSAPGridCellData intArrCnt + 1, "Material", Split(arrMaterialData(intArrCnt),";")(2)
			oControls.SetSAPGridCellData intArrCnt + 1, "Short Text", Split(arrMaterialData(intArrCnt),";")(3)
			oControls.SetSAPGridCellData intArrCnt + 1, "PO Quantity", Split(arrMaterialData(intArrCnt),";")(4) 
			oControls.SetSAPGridCellData intArrCnt + 1, "OUn", Split(arrMaterialData(intArrCnt),";")(5) 
			oControls.SetSAPGridCellData intArrCnt + 1, "Matl Group", Split(arrMaterialData(intArrCnt),";")(6) 
			oControls.SetSAPGridCellData intArrCnt + 1, "Plnt", Split(arrMaterialData(intArrCnt),";")(7)
			oControls.SetSAPGridCellData intArrCnt + 1, "Stor. Location", Split(arrMaterialData(intArrCnt),";")(8)
		Next
	End If
	
End Function

Function fnME29N_ReleasePurchaseOrder(strPONumber)

	'Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Release Purchase Order","True"
	
	'Entering T code
	oControls.SetSAPOKCode ("/nME29N")
	
	'Navigat to created Purchase Order 
	fnOtherPurchaseOrder strPONumber
	
	'Navigat to Relase Startegy Tab
	oControls.SelectSAPTabStrip "HEADER_DETAIL", "Release strategy"
	
	If SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").SAPGuiGrid("guicomponenttype:=201","name:=shell").Exist(7) Then
		intColumnCnt = SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").SAPGuiGrid("guicomponenttype:=201","name:=shell").ColumnCount
		If intColumnCnt = 5 Or intColumnCnt = 4 Then
			intRowCount = oControls.GetSAPGridRowCount()
		
			'Click on Release button
			For Iterator = 1 To intRowCount Step 1
				oControls.ClickSAPGridCell Iterator, "Release Options"
			Next
			
			'Read & Verify 'Release effected with release code JA'
			strReleaseText = oControls.GetSAPStatusBarInfo("text")
			
			'Capture Screen Shot for Release Purchase Order
			CaptureScreenShotAsBitmap Environment.Value("ScreenShotPath"), "fnME29N_ReleasePurchaseOrder"
			
			'Click on Save Button
			oControls.SendSAPkey F11
			Wait(5)
		
		End If
		Wait(5)
	End If	
	
	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
	
End function


Function fnOtherPurchaseOrder(strPONumber)
	
	'Click on Other Purchase Order button in Menu
	oControls.ClickSAPButton "btn[17]"
	
	'Emter PO NUmber
	oControls.SetSAPEdit "MEPO_SELECT-EBELN", strPONumber
	
	'Click on Other Document Button in Popup window
	oControls.ClickSAPButton "btn[0]"
	
End Function


Function fnMIGO_GoodsReceipt(strPONumber, oP2PTestData)

	'Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Goods Receipt","True"
	
	Wait(20)
	'Entering T code
	oControls.SetSAPOKCode ("/nMIGO")
	
	'Get Data for Created PO 
	oControls.SelectSAPComboBox "GODYNPRO-ACTION", "Goods Receipt"
	oControls.SelectSAPComboBox "GODYNPRO-REFDOC", "Purchase Order"
	oControls.SetSAPEdit "GODYNPRO-PO_NUMBER", strPONumber
	
	'Click Enter on Header Screen
	oControls.ClickSAPEnter ()
	
	'Read Material Data to Click on Item OK Check Box
	arrMaterialData = Split(oP2PTestData("MaterialData"),"||")
	
	For intArrCnt = LBound(arrMaterialData) To UBound(arrMaterialData)
		oControls.SelectSAPCheckBox "GODYNPRO-DETAIL_TAKE", "ON"
		If intArrCnt <> 0 Then oControls.ClickSAPButton "OK_NEXT_ITEM"
	Next
	
	'Click on Check Button
	oControls.ClickSAPButton "btn[7]"
	
	If SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").SAPGuiButton("type:=GuiButton","name:=btn[0]").Exist Then
		SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").SAPGuiButton("type:=GuiButton","name:=btn[0]").Click
	End If
	
	'Verify 'Document is O.K.' Text
	strDocumentIsOK = oControls.GetSAPStatusBarInfo("text")
	
	'Click on Post Button
	''If InStr(strDocumentIsOK,"Document is O.K.") > 0 Then 
	oControls.ClickSAPButton "btn[23]"
	
	'Read Material Document Posted is O.K.' Text
	strMaterialDocument = oControls.GetSAPStatusBarInfo("item1")
	fnMIGO_GoodsReceipt = strMaterialDocument  ''5000126966 
	
	'Capture Screen Shot for MIGO_GoodsReceipt
	CaptureScreenShotAsBitmap Environment.Value("ScreenShotPath"), "fnMIGO_GoodsReceipt"
	
	If strMaterialDocument <> "" Then
		oControls.UpdateExecutionReport micPass, "Goods Receipt:", "Goods Receipt has been created, Goods Receipt: '"&strMaterialDocument&"'"
	Else
		oControls.UpdateExecutionReport micFail, "Goods Receipt:", "Goods Receipt has not created"
	End If
	
	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
	
End  Function



Function fnMB03_DisplayMaterialDocument(strMaterialDocument)
	
	'Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Display Material Document","True"
	Wait(40)
	
	'Entering T code
	oControls.SetSAPOKCode ("/nMB03")
	
	'Emter Material Document NUmber
	oControls.SetSAPEdit "RM07M-MBLNR", strMaterialDocument
	
	'Click Enter on Header Screen
	oControls.ClickSAPEnter ()
	Wait 5
	
	intIncrement = 1
	Do While NOT SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").SAPGuiEdit("name:=MSEG-EBELN","guicomponenttype:=32","index:=0").Exist 
		'Click Enter on Header Screen
		oControls.ClickSAPEnter ()
		If intIncrement > 20 Then 
			Exit Do
		End If
		Wait(15)
		intIncrement = intIncrement + 1
	Loop 
	
	'Capture Screen Shot for Display Material Document
	CaptureScreenShotAsBitmap Environment.Value("ScreenShotPath"), "fnMB03_DisplayMaterialDocument"
	
	'Read Movement Type
	Environment.Value("MovementType") = SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").SAPGuiEdit("name:=MSEG-BWART","index:=0").GetROProperty("text")
	''Environment.Value("MovementType") = oControls.GetSAPEditValue("MSEG-BWART")
	
	'Click on Accounting Documents Button
	oControls.ClickSAPButton "btn[7]"

	Set objGuiGrid = SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=22").SAPGuiGrid("guicomponenttype:=201","name:=shell")
	If objGuiGrid.Exist(5) Then
		objGuiGrid.Highlight
		objGuiGrid.Object.DoubleClick 0,0
	End If

	'Read G/L Account
	Environment.Value("Account_1") = oControls.GetSAPGridCellData(1,"Account")
	Environment.Value("Account_2") = oControls.GetSAPGridCellData(2,"Account")

	'Capture Screen Shot for Display Material Document
	CaptureScreenShotAsBitmap Environment.Value("ScreenShotPath"), "fnMB03_Accounting Documents"
		
	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
	
End  Function

Function fnMIROInvoiceReceipt(strPONumber, TestCaseID)
	
	'Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Invoice Receipt","True"
	
	'Entering T code
	oControls.SetSAPOKCode ("/nMIRO")
	
	'Select trasanction
	If InStr(TestCaseID,"TC_R+F_PTP_06") > 0 Or InStr(TestCaseID,"TC_US_R+F_PTP_15") > 0 Then
		oControls.SelectSAPComboBox "RM08M-VORGANG", "Credit Memo"
	Else
		oControls.SelectSAPComboBox "RM08M-VORGANG", "Invoice"
	End If
	
	'Enter Invoice Date
	oControls.SetSAPEdit "INVFO-BLDAT", Date
	oControls.ClickSAPEnter ()
	Wait 3
	
	'Enter Reference Data
	''oControls.SetSAPEdit "INVFO-XBLNR", "QATesting" & RandomNumber(1000,9999)
	oControls.SetSAPEdit "INVFO-XBLNR", strPONumber
	oControls.ClickSAPEnter ()
	Wait 3
	
	'Enter PO Number
	oControls.SetSAPEdit "RM08M-EBELN", strPONumber
	
	'Click Enter on Header Screen
	oControls.ClickSAPEnter ()
	Wait 3
	
	'Read Balance Amount
	strBalance = oControls.GetSAPEditValue("RM08M-DIFFERENZ")

	'Enter Amount
	oControls.SetSAPEdit "INVFO-WRBTR", Left(strBalance,Len(strBalance)-1)
	
	'Click on Save Button
	oControls.SendSAPkey F11
	
	'Click Enter on Header Screen
	''oControls.ClickSAPEnter ()
	
	If SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").SAPGuiEdit("name:=BSET-FWSTE","guicomponenttype:=31").Exist Then _
	oControls.ClickSAPEnter ()
	Wait 7
	
	'Read Material Document Posted is O.K.' Text
	strInvoiceReceiptDocument = oControls.GetSAPStatusBarInfo("text")
	
	fnMIROInvoiceReceipt = Trim(Split(strInvoiceReceiptDocument," ")(2))
	
	'Capture Screen Shot for MIRO Invoice Receipt
	CaptureScreenShotAsBitmap Environment.Value("ScreenShotPath"), "fnMIRO_InvoiceReceipt"
	
	If Trim(Split(strInvoiceReceiptDocument," ")(2)) <> "" Then
		oControls.UpdateExecutionReport micPass, "Invoice Receipt:", "Invoice Receipt has been created, Invoice Receipt: '"&Trim(Split(strInvoiceReceiptDocument," ")(2))&"'"
	Else
		oControls.UpdateExecutionReport micFail, "Invoice Receipt:", "Invoice Receipt has not created"
	End If
	
	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
	
End Function

Function fnMIR4DisplayInvoiceDocument(strInvoiceReceiptDocument)

	'Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Display Invoice Receipt Document","True"
	
	'Entering T code
	oControls.SetSAPOKCode ("/nMIR4")
	
	'Enter Invoice Document No
	oControls.SetSAPEdit "RBKP-BELNR", strInvoiceReceiptDocument
	
	'Click Enter on Header Screen
	oControls.ClickSAPEnter ()
	Wait(5)
	
	intIncrement = 1
	Do While NOT SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").SAPGuiEdit("name:=RBKPV-BELNR","guicomponenttype:=31").Exist(7) 
		'Click Enter on Header Screen
		oControls.ClickSAPEnter ()
		If intIncrement > 5 Then 
			Exit Do
		End If
		Wait(15)
		intIncrement = intIncrement + 1
	Loop 
	
	If SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").SAPGuiEdit("name:=RBKPV-BELNR","guicomponenttype:=31").Exist(7) Then
		sScreenName = SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").GetROProperty("text")
		oControls.UpdateExecutionReport micPass, "Invoice Receipt:", "Invoice Receipt Details has been displayed, Invoice Receipt: '"&strInvoiceReceiptDocument&"'"
	Else
		oControls.UpdateExecutionReport micFail, "Invoice Receipt:", "Invoice Receipt Details not displayed"
	End If
	
	'Capture Screen Shot for MIR4D isplay Invoice Document
	CaptureScreenShotAsBitmap Environment.Value("ScreenShotPath"), "fnMIR4_DisplayInvoiceDocument"
	
	'Click on Follow-on Documents Button
	oControls.ClickSAPButton "btn[8]"
	
	'Read G/L Account
	Environment.Value("InvoiceAccount_1") = oControls.GetSAPGridCellData(1,"Account")
	Environment.Value("InvoiceAccount_2") = oControls.GetSAPGridCellData(2,"Account")
	
	'Capture Screen Shot for Display Material Document
	CaptureScreenShotAsBitmap Environment.Value("ScreenShotPath"), "fnMIR4_Accounting Documents"
	
	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
End Function


Function VerifySAPModalWindowExistance ( )
	' @HELP
	' @class:		Controls_SAP
	' @method:		VerifySAPModalWindowExistance (sSAPTabStrip)
	' @notes:		returns modal window existance  
	' @END 
	On error resume next
		
	If err.description <> "" Then
		Reporter.ReportEvent micFail, "VerifySAPModalWindowExistance","Error Description: " & err.description
	End If
	
	Err.Clear
	On error goto 0	
End Function

Function fnVL10DCreateOutboundDelivery(strPONumber)
	
	Wait(30)
	'Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Create Outbound Delivery","True"
	
	'Entering T code
	oControls.SetSAPOKCode ("/nVL10D")
	
	'Navigate Purchase Orders Tab
	oControls.SelectSAPTabStrip "TABSTRIP_ORDER_CRITERIA", "Purchase Orders"
	
	'Entering T code
	oControls.SetSAPEdit "ST_EBELN-LOW", strPONumber
	
	Wait 3
	'Click on Execute Button
	oControls.SendSAPkey F8
	
	'Select Check Box/Selet All Button
	oControls.SendSAPkey F5
	
	'Click on Back Ground Button
	oControls.ClickSAPButton "btn[19]"
	Wait 5
	
	'Entering T code
	oControls.SetSAPOKCode ("/nME23N")
	
	'Navigat to created Purchase Order 
	fnOtherPurchaseOrder strPONumber
	Wait(5)

	Set objItemDetailsBtn = SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").SAPGuiButton("name:=DYN_4000-BUTTON","index:=2")

	If objItemDetailsBtn.GetROProperty("tooltip") = "Expand Item Details Ctrl+F4" Then
		objItemDetailsBtn.Click
	End  If 

	'Navigat to Relase Startegy Tab
	oControls.SelectSAPTabStrip "ITEM_DETAIL", "Purchase Order History"
	Wait(5)
	
	'Read Outbound Delivery
	strOutboundDelivery = oControls.GetSAPGridCellData(1,"Material Document")
	
	strOutboundDeliveryText = oControls.GetSAPGridCellData(2,"Short Text")
	fnVL10DCreateOutboundDelivery = strOutboundDelivery
	
	'Capture Screen Shot for VL10D Creat eOutbound Delivery
	CaptureScreenShotAsBitmap Environment.Value("ScreenShotPath"), "fnVL10D_CreateOutboundDelivery"
	
	
	If strOutboundDelivery <> "" And IsNumeric(strOutboundDelivery) Then
		oControls.UpdateExecutionReport micPass, "Ouboud Delivery:", "Outbound Delivery has been created, Outbound Delivery: '"& strOutboundDelivery &"'"
	Else
		oControls.UpdateExecutionReport micFail, "Ouboud Delivery:", "Outbound Delivery not created" 
	End If
		
	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
End Function


Function fnVL02NPostGoodsIssue(strOutboundDelivery, oP2PTestData)

	'Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Change Delivery - Post Goods Issue","True"
	
	'Entering T code
	oControls.SetSAPOKCode ("/nVL02N")

	'Entering Outbound Delivery 
	oControls.SetSAPEdit "LIKP-VBELN", strOutboundDelivery

	'Click Enter on Header Screen
	oControls.ClickSAPEnter ()
	
	'Enter Storge Location
	oControls.SetSAPTableCellData "SAPMV50ATC_LIPS_OVER", 1, "SLoc", oP2PTestData("SLoc")
	
	'Click on Header Details 
	oControls.SendSAPkey F8
	
	'Navigate to Shipment Tab
	oControls.SelectSAPTabStrip "TAXI_TABSTRIP_HEAD", "Shipment"
	
	'Enter BillofLad
	oControls.SetSAPEdit "LIKP-BOLNR", "9999999999" 
	
	'Navigate to Shipment Tab
	oControls.SelectSAPTabStrip "TAXI_TABSTRIP_HEAD", "Addtional Data"
	
	'Enter Carrier Name
	oControls.SetSAPEdit "GV_CRNAM", "FedEx"
	
	'Click on Post Goods Issue Button
	oControls.ClickSAPButton "btn[20]"
	
	'Read Material Document Posted is O.K.' Text
	strPGIData = oControls.GetSAPStatusBarInfo("text")
	
	'Capture Screen Shot for VL02NPostGoodsIssue
	CaptureScreenShotAsBitmap Environment.Value("ScreenShotPath"), "fnVL02N_PostGoodsIssue"
	
	If InStr(strPGIData, strOutboundDelivery) > 0 Then
		oControls.UpdateExecutionReport micPass, "Change delivery - PGI", "PGI material document should createed for, Outbound Delivery: '"& strOutboundDelivery &"'"
	Else
		oControls.UpdateExecutionReport micFail, "Change delivery - PGI", "PGI material document not createed" 
	End If
	
	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
End Function

Function fnMB03DisplayPGIMaterialDocument()
	
	'Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Display PGI material document","True"
	
	'Entering T code
	oControls.SetSAPOKCode ("/nME23N")
	
	'Navigat to created Purchase Order 
	fnOtherPurchaseOrder strPONumber
	
	Set objItemDetailsBtn = SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").SAPGuiButton("name:=DYN_4000-BUTTON","index:=2")

	If objItemDetailsBtn.GetROProperty("tooltip") = "Expand Item Details Ctrl+F4" Then
		objItemDetailsBtn.Click
	End  If 
	
	'Navigat to Relase Startegy Tab
	oControls.SelectSAPTabStrip "ITEM_DETAIL", "Purchase Order History"

	'Read Outbound Delivery
	strPGIMaterialDocument = oControls.GetSAPGridCellData(1,"Material Document")
	
	'Entering T code
	oControls.SetSAPOKCode ("/nMB03")
	
	'Entering Outbound Delivery 
	oControls.SetSAPEdit "RM07M-MBLNR", strPGIMaterialDocument
	
	'Click Enter on Header Screen
	oControls.ClickSAPEnter ()
	Wait 15
	
	If SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").SAPGuiEdit("name:=RM07M-MTSNR","guicomponenttype:=31").Exist(7) Then
		sScreenName = SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").GetROProperty("text")
		oControls.UpdateExecutionReport micPass, "Display PGI Material Document:", "PGI Material Document: has been displayed, Outbound Delivery (PGI): '"& strPGIMaterialDocument &"'"
	Else
		oControls.UpdateExecutionReport micFail, "Display PGI Material Document:", "PGI Material Document: Details not displayed"
	End If
	
	'Capture Screen Shot for MB03 Display PGI MaterialDocument
	CaptureScreenShotAsBitmap Environment.Value("ScreenShotPath"), "fnMB03_DisplayPGIMaterialDocument"
	
	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
End Function

Function fnVF01BillingDocument(strOutboundDelivery)

	'Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Billing Document (VI CC Billing to STO)","True"
	
	'Entering T code
	oControls.SetSAPOKCode ("/nVF01")
	
	'Enter Document Number
	oControls.SetSAPTableCellData "SAPMV60ATCTRL_ERF_FAKT", 1, "Document", strOutboundDelivery
	
	'Click Enter on Header Screen
	oControls.ClickSAPEnter ()
	Wait 10
	
	If SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").SAPGuiEdit("name:=VBRK-VBELN","guicomponenttype:=32").Exist(7) Then
		sScreenName = SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").GetROProperty("text")
		oControls.UpdateExecutionReport micPass, "Billing Document (VI CC Billing to STO):", "Billing Document (VI CC Billing to STO), Outbound Delivery : '"& strOutboundDelivery &"'"
	Else
		oControls.UpdateExecutionReport micFail, "Billing Document (VI CC Billing to STO):", "Billing Document (VI CC Billing to STO) Details not displayed"
	End If
	
	'Capture Screen Shot for VF01 Billing Document
	CaptureScreenShotAsBitmap Environment.Value("ScreenShotPath"), "fnVF01_BillingDocument"
	
End Function


Function fnME23NDisplayPONumberDetails(strPONumber)
	
	'Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Display PO Details","True"
	
	'Entering T code
	oControls.SetSAPOKCode ("/nME23N")
	
	'Navigat to created Purchase Order 
	fnOtherPurchaseOrder strPONumber
	
	Set objHeaderBtn = SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").SAPGuiButton("name:=DYN_4000-BUTTON","index:=0")
	Set objItemsBtn = SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").SAPGuiButton("name:=DYN_4000-BUTTON","index:=1")
	Set objItemDetailsBtn = SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").SAPGuiButton("name:=DYN_4000-BUTTON","index:=2")
	
	If objHeaderBtn.GetROProperty("tooltip") = "Expand Item Details Ctrl+F4" Then
		objHeaderBtn.Click
	End  If 
		
	'Navigat to Org. Data Tab
	oControls.SelectSAPTabStrip "HEADER_DETAIL", "Org. Data"
	
	If objItemDetailsBtn.GetROProperty("tooltip") = "Expand Item Details Ctrl+F4" Then
		objItemDetailsBtn.Click
	End  If 
	
	'Navigat to Relase Startegy Tab
	oControls.SelectSAPTabStrip "ITEM_DETAIL", "Purchase Order History"

	'Capture Screen Shot for fnME23NDisplayPONumberDetails
	CaptureScreenShotAsBitmap Environment.Value("ScreenShotPath"), "fnME23NDisplayPONumberDetails"
	
	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
End Function

Function CaptureScreenShotAsBitmap (strFolderPath, strFunctionality)

		' @HELP
		' @group	: Functions	
		' @method	: CaptureErrorAsBitmap
		' @returns	: None
		' @parameter: 
		' @notes	: To Capture an Error as Bitmap Image
		' @END
		
		Dim sImageName
	
		sImageName	= strFolderPath & "\" & strFunctionality &".bmp"
	
		SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").CaptureBitmap sImageName, TRUE
		ReportWriter micDone, "Desktop Image", "The Desktop Image location is: " &sImageName,0
	
End Function

Function CreateFolderPath(byval fldPath)

		Dim bFso
		Dim PathArray
		Dim rfldPath
		Dim pIndex
		Dim fPath
		
		Set bFso=CreateObject("scripting.filesystemobject")
	
		If not bFso.FolderExists(fPath) then
			bFso.CreateFolder(fPath)
		End If
		
		Set bFso=nothing
	
End Function

Function CreateFolder(strTestCaseID)
		'Backup reporting files
		Set folderFso=CreateObject("scripting.filesystemobject")
		If folderFso.FolderExists(SCREENSHOT_DIR&strTestCaseID) Then
			Set oFile=folderFso.GetFolder(SCREENSHOT_DIR&strTestCaseID)
			sFileCreatedTime=oFile.DateLastModified
			Existfiles=strTestCaseID&sFileCreatedTime
			Existfiles=Replace(Existfiles,"/","")
			Existfiles=Replace(Existfiles,"\","")
			Existfiles=Replace(Existfiles,"-","")
			Existfiles=Replace(Existfiles,":","")
			Existfiles=Replace(Existfiles," ","_")
		End If	
		
		If  folderFso.FolderExists(SCREENSHOT_DIR & strTestCaseID) then
		      folderFso.MoveFolder SCREENSHOT_DIR & strTestCaseID,SCREENSHOT_DIR & Existfiles
		End If
		
		
		If not folderFso.FolderExists(SCREENSHOT_DIR & strTestCaseID) then
			folderFso.CreateFolder(SCREENSHOT_DIR & strTestCaseID)
		End If
		
		Set folderFso=nothing
		
End Function


Function fnVerifyTillTextExist(strOrignialText, strVerifyText, blnText)

	bTextExist = blnText
	While bTextExist
		If InStr(strOrignialText,strVerifyText) > 0 Then 
			bTextExist = False
		Elseh
			'Click Enter on Header Screen
			oControls.ClickSAPEnter ()
			i = i + 1
			Wait 2
		End If 
		If i > 500 Then bTextExist = False
	Wend
	
End Function



Function SAPLoginWithOterLanguage(oP2PTestData)
	
	
	
	'Start Reporting 
	Reporter.StartStatusTrack	
	Reporter.StartFunction "SAP Login Other Language","True"
	
	sSQLSAPAccessData = "SELECT * from Execution_Environment where ExecutionTag = 'T';"
	Set oSAPAccessData = oDataAccess.ExecSQLStatementWithWhereClass(sSQLSAPAccessData)
	
	'Closing all existing session of SAP to launch new session for login
	CloseExistingSessions
	SAPServer = oSAPAccessData("sEnvironment")

	SAPClient = oSAPAccessData("iClient")
	SAPUser = oSAPAccessData("sUserName")
	SAPPassword = oSAPAccessData("sPassword")
	SAPLanguage = oSAPAccessData("sLanguage")
	
	SAPGuiUtil.AutoLogon SAPServer, SAPClient, SAPUser, SAPPassword, oP2PTestData("OtherLanguageLogin")
	
	Reporter.EndFunction
	Reporter.EndStatusTrack
	
	End Function
	
	Function fnME23NDisplayPONumberDetailsforOtherLanguage(strPONumber)
	
	'Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Display PO Details","True"
	
	'Entering T code
	oControls.SetSAPOKCode ("/nME23N")
	
	'Navigat to created Purchase Order 
	fnOtherPurchaseOrder strPONumber
	
	Set objItemDetailsBtn = SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").SAPGuiButton("name:=DYN_4000-BUTTON","index:=2")
	
	If objItemDetailsBtn.GetROProperty("tooltip") = "明細詳細展開 Ctrl + F4" Then
		objItemDetailsBtn.Click
	End  If 
	
	'Navigat to Relase Startegy Tab
	oControls.SelectSAPTabStrip "ITEM_DETAIL", "購買発注履歴"

	'Capture Screen Shot for fnME23NDisplayPONumberDetails
	CaptureScreenShotAsBitmap Environment.Value("ScreenShotPath"), "fnME23NDisplayPONumberDetails"
	
	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
End Function
	


Function fnRunValidationPoints(oP2PTestData)
	
	'Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Validation Points through tables","True"
	
	
	Dim objSAPWnd, objSE16NTable, strTableName, strColumnName
	
	Set objSAPWnd = SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21")
	Set objSE16NTable = objSAPWnd.SAPGuiTable("name:=SAPLSE16NSELFIELDS_TC")

	For V_Iterator = LBound(Split(oP2PTestData("ValidationData"),"||")) To UBound(Split(oP2PTestData("ValidationData"),"||")) 
		
		'Read Table Name
		strTableName = Split(Split(Split(oP2PTestData("ValidationData"),"||")(V_Iterator),";")(0),"=")(1)
		
		'Starting the reporting
		Reporter.StartStatusTrack	
		Reporter.StartFunction "Validation Points for "& strTableName  & ": Table","True"
		
		'Entering T code  & Enter Table Name
		oControls.SetSAPOKCode ("/nSE16N")
		oControls.SetSAPEdit "GD-TAB", strTableName

		'Click Enter on Header Screen
		oControls.ClickSAPEnter ()
		Wait(5)

		'Enter Field Values
		For FldNameIterator = 1 To UBound(Split(Split(oP2PTestData("ValidationData"),"||")(V_Iterator),";"))
			intFldNameRowNo = objSE16NTable.FindRowByCellContent("Fld name",Trim(Split(Split(Split(oP2PTestData("ValidationData"),"||")(V_Iterator),";")(FldNameIterator),"=")(0)))
			strFieldValue = Split(Split(Split(oP2PTestData("ValidationData"),"||")(V_Iterator),";")(FldNameIterator),"=")(1)
			
			If Trim(strFieldValue) = "Env_PO" Then strFieldValue = Environment.Value("PONumber")
			If Trim(strFieldValue) = "Env_MT" Then strFieldValue = Environment.Value("MovementType")
			If Trim(strFieldValue) = "Env_Account" Then 
				strFieldValue = Trim(Environment.Value("Account_1")) & "#" & Trim(Environment.Value("Account_2")) 
			End If
			If Trim(strFieldValue) = "Env_InvoiceAccount" Then strFieldValue = Trim(Environment.Value("InvoiceAccount_1")) & "#" & Trim(Environment.Value("InvoiceAccount_2"))
			
			If InStr(strFieldValue,"#") > 0 Then
				fnEnterGLAccountDetailsforSE16NTable strFieldValue, intFldNameRowNo
				''strTableName = ""
			Else
				oControls.SetSAPTableCellData "SAPLSE16NSELFIELDS_TC", intFldNameRowNo, "Fr.Value", Trim(strFieldValue)
			End If 
			
		Next
		
		'Click on Execute Button
		oControls.SendSAPkey F8
		Wait(10)
		
		For FldNameIterator = 1 To UBound(Split(Split(oP2PTestData("ValidationData"),"||")(V_Iterator),";"))
	
			strColumnName = Split(Split(Split(oP2PTestData("ValidationData"),"||")(V_Iterator),";")(FldNameIterator),"=")(0)
			If InStr(strColumnName,"Stor. Location") > 0 Then   strColumnName = "Storage Location"
			If InStr(strColumnName,"Inv. Doc. No.") > 0 Then   strColumnName = "Invoice Document No."
			
			strColumnValue = Split(Split(Split(oP2PTestData("ValidationData"),"||")(V_Iterator),";")(FldNameIterator),"=")(1)
			strResultTblCelData = oControls.GetSAPGridCellData (1, strColumnName)
			
			If Trim(strColumnValue) = "Env_PO" Then strColumnValue = Environment.Value("PONumber")
			If Trim(strColumnValue) = "Env_MT" Then strColumnValue = Environment.Value("MovementType")
			If Trim(strColumnValue) = "Env_Account" Then 
				strColumnValue = Trim(Environment.Value("Account_1")) & "#" & Trim(Environment.Value("Account_2")) 
			End If
			If Trim(strColumnValue) = "Env_InvoiceAccount" Then strColumnValue = Trim(Environment.Value("InvoiceAccount_1")) & "#" & Trim(Environment.Value("InvoiceAccount_2"))
			
			If InStr(strColumnValue,"#") > 0 Then
				strResultTblCelData_1 = oControls.GetSAPGridCellData (1, strColumnName)
				strResultTblCelData_2 = oControls.GetSAPGridCellData (2, strColumnName)
				If InStr(Trim(strResultTblCelData_1 & "#" & strResultTblCelData_2),Trim(strColumnValue)) > 0 Or InStr(Trim(strColumnValue), Trim(strResultTblCelData_1)) > 0 Then
					oControls.UpdateExecutionReport micPass, strTableName & " Table Validation: - Verify '" &  strResultTblCelData &"'", "'" & strResultTblCelData &"' value exist for '" & strColumnName & "' column under " & strTableName& " table"
				Else
					oControls.UpdateExecutionReport micFail, strTableName & " Table Validation: "   , "Table Value: " & strResultTblCelData_1 &" Actual Value: " & strColumnValue 
				End If
			Else
				If (StrComp(Trim(strResultTblCelData),Trim(strColumnValue))) = 0 Then
					oControls.UpdateExecutionReport micPass, strTableName & " Table Validation: - Verify '" &  strResultTblCelData &"'", "'" & strResultTblCelData &"' value exist for '" & strColumnName & "' column under " & strTableName& " table"
				Else
					oControls.UpdateExecutionReport micFail, strTableName & " Table Validation: "   , "Table Value: " & strResultTblCelData &" Actual Value: " & strColumnValue 
				End If
			End If
		Next
		
		'End Reporting 
		Reporter.EndFunction
		Reporter.EndStatusTrack
			
		'Capture Screen Shot for fnME23NDisplayPONumberDetails
		CaptureScreenShotAsBitmap Environment.Value("ScreenShotPath"), strTableName & " Table Validation"
	Next

	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
	
End Function


Function fnEnterGLAccountDetailsforSE16NTable(strFieldValue, intFldNameRowNo)
	
	'Click More Button
	oControls.ClickSAPTableCell "SAPLSE16NSELFIELDS_TC", intFldNameRowNo, "More"
	
	oControls.SetSAPTableCellData "SAPLSE16NMULTI_TC", 1, "Fr.Value", Split(strFieldValue,"#")(0)
	oControls.SetSAPTableCellData "SAPLSE16NMULTI_TC", 2, "Fr.Value", Split(strFieldValue,"#")(1)
	
	'Click on Execute Button
	oControls.SendSAPkey F8
End Function


Function fnDemandPlanningAndMRP()
	
	'Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "JPL-3293 Demand Planning And MRP","True"
	
	arrTestCaseDescription = Array("Execute for MD01 for plant 15","Execute for MD02 for plant 1510","Execute MD02 for material with special procurement type 2" _
	,"Execute MD02 for material with special procurement type 20")
	
	arrTestData = Array("MD01;;1510","MD02;6099-02-0051;1510","MD02;8003-02-0006;1510","MD02;8003-02-0006;1510")
	
	For intTCDesc = LBound(arrTestCaseDescription) To UBound(arrTestCaseDescription) 
		
		Reporter.StartStatusTrack	
		Reporter.StartFunction arrTestCaseDescription(intTCDesc),"True"
	
		'Entering T code  & Enter Table Name
		oControls.SetSAPOKCode ("/n" & Split(arrTestData(intTCDesc),";")(0)	)
		
		If oControls.VerifySAPEdit("RM61X-MATNR","Material") Then _
			oControls.SetSAPEdit "RM61X-MATNR", Split(arrTestData(intTCDesc),";")(1) 'Enter Material
		
		'Enter Plant
		oControls.SetSAPEdit "RM61X-WERKS", Split(arrTestData(intTCDesc),";")(2)
		
		For IteratorEnter = 1 To 3 
			'Click Enter on Header Screen
			oControls.ClickSAPEnter ()

			Wait(5)
			If SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=22").SAPGuiButton("name:=btn[0]").Exist Then _
				SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=22").SAPGuiButton("name:=btn[0]").Click
		Next

		
		'Validation for PRs creation
		Set objLabel =  SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").SAPGuiLabel("content:=Create Purchase Requisition").Object

		For intElement = 4 To objLabel.Parent.Children.Count 
			If objLabel.Parent.Children.Item(CInt(intElement)).text = "Create Purchase Requisition" Then 
				intPurchaseRequisition =  objLabel.Parent.Children.Item(CInt(intElement + 2)).text  
				Exit For
			End If
		Next
		
		If CInt(intPurchaseRequisition) > 0 Then
			oControls.UpdateExecutionReport micPass, " Create Purchase Requisition Validation:", "Create Purchase Requisition Value is - " &intPurchaseRequisition
		Else
			oControls.UpdateExecutionReport micFail, " Create Purchase Requisition Validation:", "Create Purchase Requisition Value is - " &intPurchaseRequisition & "should be more than 0"
		End If
				
		'End Reporting 
		Reporter.EndFunction
		Reporter.EndStatusTrack
	Next

	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
	
	
End Function


Function fnJPL885ReceivingReport_InboundReceiptsinto3PL()
	
	'Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "JPL-885-Receiving Report (Daily) - Inbound receipts into 3PL","True"
	
	arrTestCaseDescription = Array("Create inventory document (reprocessing IDOC)", _
	"Display material document list for Plant-1510 with date range", _
	"Display purchasing documents for Plant-1510", _
	"Display purchasing documents for Plant-1510 with date range", _
	"Display purchasing documents for Plant-1510 with Material", _
	"Display purchasing documents for Plant-1510 with Material")
	
	For intTCDesc = LBound(arrTestCaseDescription) To UBound(arrTestCaseDescription) 
		
		Reporter.StartStatusTrack	
		Reporter.StartFunction arrTestCaseDescription(intTCDesc),"True"
	
		If intTCDesc = 0 Or intTCDesc = 1 Then
		
			'Entering T code  & Enter Table Name
			 oControls.SetSAPOKCode ("/nMB51")
	
			'Enter Plant
			oControls.SetSAPEdit "WERKS-LOW", "1510"
			
			'Enter movement Type
			oControls.SetSAPEdit "BWART-LOW", "101"
			
			'Select Display Option Radio button
			oControls.SelectSAPRadioButton "RFLAT_L", "Flat List"
			
			If intTCDesc = 1 Then
				oControls.SetSAPEdit "BUDAT-LOW", DateAdd("m",-3,Date) 'Enter Option Flat List
				oControls.SetSAPEdit "BUDAT-HIGH", Date 'Enter Option Flat List
			End If
			
			'Enter Option Flat List
			oControls.SetSAPEdit "ALV_DEF", "/CJ"
			oControls.SendSAPkey F8  'Click on Execute Button
			Wait(3)
			fnVerifyMB51MaterialDocumentListTableExistence
		Else		
			'Entering T code  & Enter Table Name
			 oControls.SetSAPOKCode ("/nME80FN")
			 
			 If intTCDesc = 2 Then
			 	oControls.SetSAPEdit "SP$00011-LOW", "1510"  'Enter Option Flat List
			 ElseIf intTCDesc = 3 Then
				oControls.SetSAPEdit "SP$00001-LOW", DateAdd("m",-3,Date) 'Enter Option Flat List
				oControls.SetSAPEdit "SP$00001-HIGH", Date 'Enter Option Flat List
			 ElseIf intTCDesc = 4 Then
			 	oControls.SetSAPEdit "S_MATNR-LOW", "6099-02-0051" 'Enter Material
			 ElseIf intTCDesc = 5 Then
			 	oControls.SetSAPEdit "SP$00007-LOW", "491750" 'Enter Vendor
			 End If
			 oControls.SendSAPkey F8  'Click on Execute Button
			 Wait(3)
			 fnVerifyME80FNMaterialDocumentListTableExistence
		End If
		
		'End Reporting 
		Reporter.EndFunction
		Reporter.EndStatusTrack
	Next
	
End Function


Function fnVerifyMB51MaterialDocumentListTableExistence()
	
	Set objMtrlDcmntTbl = SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").SAPGuiGrid("name:=shell","type:=GuiShell", "guicomponenttype:=201")

	If objMtrlDcmntTbl.Exist then
		objMtrlDcmntTbl.SelectAll ()
		oControls.UpdateExecutionReport micPass, "Verfy Material Document List Table Existence","Material Document List Table is displaying"
	Else
		oControls.UpdateExecutionReport micPass, "Verfy Material Document List Table Existence","Material Document List Table is not displaying"
	End If
	
	strPlant = objMtrlDcmntTbl.GetCellData(1,"Plant")
	strMovementType = objMtrlDcmntTbl.GetCellData(1,"Movement Type")
	strMaterialDoc = objMtrlDcmntTbl.GetCellData(1,"Material Document")

	If CInt(Trim(strPlant)) = "1510" And CInt(Trim(strMovementType)) = "101" And strMaterialDoc <> "" Then
		oControls.UpdateExecutionReport micPass, "Verify plant, Movement Type & Material Document in Result Table","Plant: " & strPlant & vbCrLf & _
		"Movement Type: " & strMovementType & vbCrLf & "Material Document: " & strMaterialDoc
	Else
		oControls.UpdateExecutionReport micFail, "Verify plant, Movement Type & Material Document in Result Table","Plant, Movement Type & Material Document details are not displaying"
	End If
					
	Set objMtrlDcmntTbl = Nothing
End Function

Function fnVerifyME80FNMaterialDocumentListTableExistence()
	
	Set objMtrlDcmntTbl = SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").SAPGuiGrid("name:=shell","type:=GuiShell", "guicomponenttype:=201")

	If objMtrlDcmntTbl.Exist then
		oControls.UpdateExecutionReport micPass, "Verfy Material Document List Table Existence","Material Document List Table is displaying"
	Else
		oControls.UpdateExecutionReport micPass, "Verfy Material Document List Table Existence","Material Document List Table is not displaying"
	End If
	
	strPlant = objMtrlDcmntTbl.GetCellData(1,"Plant")
	strVendor = objMtrlDcmntTbl.GetCellData(1,"Vendor")
	strMaterial = objMtrlDcmntTbl.GetCellData(1,"Material")
	strPuchasingDocument = objMtrlDcmntTbl.GetCellData(1,"Purchasing Document")

	If CInt(Trim(objMtrlDcmntTbl.GetCellData(1,"Plant"))) = "1510" And CLng(Trim(objMtrlDcmntTbl.GetCellData(1,"Vendor"))) = "491750" Or CLng(Trim(objMtrlDcmntTbl.GetCellData(1,"Vendor"))) = "101323" And strPuchasingDocument <> "" Then
		oControls.UpdateExecutionReport micPass, "Verify plant, Movement Type & Material Document in Result Table","Plant: " & strPlant & vbCrLf & _
		"Vendor: " & objMtrlDcmntTbl.GetCellData(1,"Vendor") & vbCrLf & "Material: " & strMaterial  & vbCrLf & "Puchasing Document: " & strPuchasingDocument
	Else
		oControls.UpdateExecutionReport micFail, "Verify plant, Movement Type & Material Document in Result Table","Plant: " & strPlant & vbCrLf & _
		"Vendor: " & strVendor & vbCrLf & "Material: " & strMaterial  & vbCrLf & "Puchasing Document: " & strPuchasingDocument
	End If
					
	Set objMtrlDcmntTbl = Nothing
End Function


Function fnJPL3242IntegrationWith3PLGoodsReciept (oP2PTestData)

	'Start Reporting 
	Reporter.StartStatusTrack
	Reporter.StartFunction Trim(oP2PTestData("Description")),"True"

	CreateFolder oP2PTestData("TestCaseID")
	Environment.Value("ScreenShotPath") = SCREENSHOT_DIR & oP2PTestData("TestCaseID")

	'Loggin in to SAP system
	SAPLogon ()
	
	'Create Purchase Order
	strPONumber = fnME21N_CreatePurchaseOrder(oP2PTestData)
	Wait 10
	stop
	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
		
End Function 

