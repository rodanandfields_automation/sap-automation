''Creating sap object
'
set oControls	= new clsControlsSAP
set oUtils		= new clsUtils
set oAccessData = new clsDataAccess
'


'=============================================================================================================
Function SAPLogon ()

	'@ Help                    :
	'@ Group                   : Common
	'@ Method Name             : SAPLogon
	'@ Pre - Condition/Screen  : None
	'@ Purpose                 : Login to the SAP system
	'@ Paramerters	           : None
	'@ Returns                 : None
	
	sSQLSAPAccessData = "SELECT * from Execution_Environment where ExecutionTag = 'T';"
	Set oSAPAccessData = oDataAccess.ExecSQLStatementWithWhereClass(sSQLSAPAccessData)
	
	'Start Reporting 
	Reporter.StartStatusTrack	
	Reporter.StartFunction "SAP Login","True"
	'Closing all existing session of SAP to launch new session for login
	CloseExistingSessions
	SAPServer = oSAPAccessData("sEnvironment")

	SAPClient = oSAPAccessData("iClient")
	SAPUser = oSAPAccessData("sUserName")
	SAPPassword = oSAPAccessData("sPassword")
	SAPLanguage = oSAPAccessData("sLanguage")
	
	SAPGuiUtil.AutoLogon  SAPServer, SAPClient, SAPUser, SAPPassword, SAPLanguage
	
	REM ''msgbox err.description
    If err.description = "" Then
		oControls.UpdateExecutionReport micPass, "Logging in to SAP application","Successfully logged into '" & SAPServer & "' system"
	Else
		oControls.UpdateExecutionReport MicFail, "Logging in to SAP application","Unable logged into '" & SAPServer & "' system"
	End If 
    'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
End Function
'=============================================================================================================
Sub CloseExistingSessions ()
	' @HELP
	' @class	:	clsControlsSAP
	' @method	:   CloseExistingSessions ()
	' @returns	:  
	' @parameter:	
	' @notes:   	Closes all the existing SAP sessions
	' @END
	SAPGuiUtil.CloseConnections
	ProExist = False 
	Set AllProcess = getobject("winmgmts:")
	For Each Process In AllProcess.InstancesOf("Win32_process") 
		If Instr ((Process.Name),ProcessName) = 1 Then 
			SystemUtil.CloseProcessByName ("pcsfe.exe") 
			ProExist = True 
			Exit for 
		End If 
	Next
	ProExist = False 
	Set AllProcess = getobject("winmgmts:")
	For Each Process In AllProcess.InstancesOf("Win32_process") 
		If Instr ((Process.Name),ProcessName) = 1 Then 
			SystemUtil.CloseProcessByName ("pcsws.exe") 
			ProExist = True 
			Exit for 
		End If 
	Next 
	ProExist = False 
	Set AllProcess = getobject("winmgmts:")
	For Each Process In AllProcess.InstancesOf("Win32_process") 
		If Instr ((Process.Name),ProcessName) = 1 Then 
			SystemUtil.CloseProcessByName ("pcscm.exe") 
			ProExist = True 
			Exit for 
		End If 
	Next 
	ProExist = False 
	Set AllProcess = getobject("winmgmts:")
	For Each Process In AllProcess.InstancesOf("Win32_process") 
		If Instr ((Process.Name),ProcessName) = 1 Then 
			SystemUtil.CloseProcessByName ("TeRun.exe") 
			ProExist = True 
			Exit for 
		End If 
	Next
End Sub
'=============================================================================================================
Function fnVA01SO_StandardOrder()

	''Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Creating Standard Sales Order","True"
	'' Éntering T code
	oControls.SetSAPOKCode ("/nVA01")
		
	''Entering Order Type
	oControls.SetSAPEdit "VBAK-AUART", DataTable.Value("OrderType")'"ZOR1"
	''Entering Sales Organization
	oControls.SetSAPEdit "VBAK-VKORG", DataTable.Value("SalesOrg")'"1710"
	''Entering Distribution Channel
	oControls.SetSAPEdit "VBAK-VTWEG", DataTable.Value("Division")'"10"
	''Entering Division
	oControls.SetSAPEdit "VBAK-SPART", DataTable.Value("DistributionChannel")'"10"	
	''Click Enter on Initial Screen
	oControls.ClickSAPEnter ()
	''Entering Sold To Party
	oControls.SetSAPEdit "KUAGV-KUNNR", DataTable.Value("SoldToParty")'"95002195"
	
	''Click Enter on Header Screen
	oControls.ClickSAPEnter ()
	
	'enter Material no in Material Column
	oControls.SetSAPTableCellData "SAPMV45ATCTRL_U_ERF_AUFTRAG","1","Material",DataTable.Value("Material")'"9700-08-0110"
	
	'enter Order Quantity in Quantity Column
	oControls.SetSAPTableCellData "SAPMV45ATCTRL_U_ERF_AUFTRAG","1","Order Quantity",DataTable.Value("Quantity")'"1"	
	
	''Click Enter on Header Screen
	oControls.ClickSAPEnter ()

	'Navigate to Item Conditions Screen
	oControls.ClickSAPButton "BT_PKON"

	'enter Item condition in Condition Type Column
	oControls.SetSAPTableCellData "SAPLV69ATCTRL_KONDITIONEN","11","CnTy",DataTable.Value("PricingCondition")'"PR00"
	
	'enter Order Quantity in Quantity Column
	oControls.SetSAPTableCellData "SAPLV69ATCTRL_KONDITIONEN","11","Amount",DataTable.Value("PricingValue")'"29"
	
	''Click Enter on Header Screen
	oControls.ClickSAPEnter ()
	
	oControls.SendSAPKey F3
	
	''Clicking on save button
	oControls.SelectSAPMenuItem "Sales document;Save"
	
	'Clicking on save button
	statusVA01 = oControls.GetSAPStatusBarInfo("item2")
	
	DataTable.Value("SalesOrder") = statusVA01
	
	fnVA01SO_StandardOrder = statusVA01
	
	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
	
End function
'=============================================================================================================
Function VA03_DisplaySO(sSO)
	
	''Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Display Sales Order VA03","True"
	'' Éntering T code
	oControls.SetSAPOKCode ("/nVA03")
	''Entering Sales Order

	oControls.SetSAPEdit "VBAK-VBELN", sSO
	''Enter
	oControls.ClickSAPEnter ()
	''Get the status bar information
	statusVA03 = oControls.GetSAPStatusBarInfo("itemscount")
	
	If statusVA03=0 Then
      oControls.UpdateExecutionReport micPass, "GetSAPStatusBarInfo", "Sales Order was successfully displayed in VA03"
     Else
	 varTmpstsVA03n=oControls.GetSAPStatusBarInfo("text")
	 oControls.UpdateExecutionReport micFail, "GetSAPStatusBarInfo", "Unable to display Sales order in VA03-Error message '"&varTmpstsVA03n&"'" 
	End if
	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
End function
'=============================================================================================================
Function VL01N_CreateDel(sSO)
''Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Create Delivery VL01N","True"
	'' Éntering T code
		
	oControls.SetSAPOKCode ("/nVL01N")
	
	''oControls.SetSAPEdit "LIKP-VSTEL", DataTable.Value("ShippingPoint")'"1755" 'Commented by Suresh Dontha
	oControls.SetSAPEdit "LIKP-VSTEL", "1755"  'Enable this line
	sTPD = Today
	oControls.SetSAPEdit "LV50C-DATBI", (Date + 7)

	oControls.SetSAPEdit "LV50C-VBELN", sSO

	oControls.ClickSAPEnter ()
	
'	''msgbox "test"
	
	sDel = oControls.GetSAPStatusBarInfo("messagetype")
	
'	''msgbox sDel
	
	If sDel = "E" Then
	
'	''msgbox "in"
	oControls.SendSAPKey F3
		
	oControls.SetSAPOKCode ("/nVA02")
	
'	''msgbox "va02"
		
	oControls.SetSAPEdit "VBAK-VBELN",sSO
	
'	''msgbox "c"

	oControls.SendSAPKey SHIFT_F5
	
	oControls.ClickSAPToolBarButton "&FIND"
	
	oControls.SetSAPModalEdit "LVC_S_SEA-STRING", "Cell Content", "Delivry"
	
	oControls.ClickSAPModalEnter ()
	
	oControls.CloseSAPModalWindow ()
	
	oControls.SendSAPKey F2
	
	VL01N_CreateDel = oControls.GetSAPGridCellData ( 1,"Doc.no." )
	
	''DataTable.Value("Delivery") = VL01N_CreateDel  Modified below line by Suresh Dontha
	Environment.Value("Delivery") = VL01N_CreateDel
	
'	If sDeliveryNumber <> "" Then
'		ocontrols.UpdateExecutionReport micPass,"Delivery to Should be created", "Delivery Number '" & sDeliveryNumber & "' created"
'	Else
'		ocontrols.UpdateExecutionReport micFail,"Delivery to Should be created", "Delivery to Should not created"
'	End If
'	
	Else 
'	
'	ocontrols.SelectSAPTabStrip "TAXI_TABSTRIP_OVERVIEW","Picking"
'	
'	oControls.SetSAPTableCellData "SAPMV50ATC_LIPS_PICK", "1", "Picked Qty",DataTable.Value("Quantity")
'	
	oControls.ClickSAPEnter ()
	
	''Clicking on save button
	oControls.SelectSAPMenuItem "Outbound Delivery;Save"
	
	VL01N_CreateDel = oControls.GetSAPStatusBarInfo("item2")
	
	''DataTable.Value("Delivery") = VL01N_CreateDel

	End If
	
	If VL01N_CreateDel="" Then
		
		ocontrols.UpdateExecutionReport micFail,"Delivery to Should be created", "Delivery to Should not created"
	End If
	
'End Reporting 
Reporter.EndFunction
Reporter.EndStatusTrack

End Function
'=============================================================================================================
Function VL02N_UpdateAdditionalData(sDelNo)
	''Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Change Delivery VL02N","True"
	'' Éntering T code		
	oControls.SetSAPOKCode ("/nVL02N")
	
	'enter outbound delivery no
	oControls.SetSAPEdit "LIKP-VBELN", sDelNo
	
	oControls.ClickSAPEnter ()
	
	''Clicking on save button
	oControls.SelectSAPMenuItem "Goto;Item;Processing"
	
	ocontrols.SelectSAPTabStrip "TAXI_TABSTRIP_ITEM","Additional Data"
	
	oControls.SetSAPEdit "LIPS-ZZCARRIER","Fed Ex"
	
	oControls.SetSAPEdit "LIPS-ZZBOLNR", "20"
	
	oControls.ClickSAPEnter ()
		
	oControls.SendSAPKey F3
	
	''Clicking on save button
	oControls.SelectSAPMenuItem "Outbound Delivery;Save"
	
	VL01N_CreateDel = oControls.GetSAPStatusBarInfo("item2")
		
	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack

End Function
'=============================================================================================================
Function WE19_Mock_ACK_Process(sRefIDoc, sDelNo, sStatus, sReqDateString,sRejectReason)
	''Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Mock ACK Process for status - "& sStatus &" - WE19","True"
	'' Éntering T code		
	oControls.SetSAPOKCode ("/nWE19")
	''MsgBox sRefIDoc
	'enter outbound delivery no
	oControls.SetSAPEdit "MSED7START-EXIDOCNUM", sRefIDoc
	
	oControls.SendSAPKey F8
		
	oControls.SetSAPLabelFocus "EDIDC"
	
	oControls.SendSAPKey Ctrl_Shift_F11
	
	oControls.SetSAPLabelFocus "E1EDL20"
	
	oControls.SendSAPKey F2
	
	oControls.SetSAPModalOffsetEdit "SVALD-VALUE","31","0",sDelNo
	
	oControls.ClickSAPModalEnter ()
	
	oControls.SetSAPLabelFocus "ZE1MISC"
	
	oControls.SendSAPKey F2
	
'	''msgbox "before status"
	
	oControls.SetSAPModalOffsetEdit "SVALD-VALUE","31","3",sStatus
'	''msgbox "After status"
	If trim(sRejectReason) <> "" Then
		oControls.SetSAPModalOffsetEdit "SVALD-VALUE","31","4",sRejectReason
	End If
	
	oControls.ClickSAPModalEnter ()
	'
	oControls.SetSAPLabelFocus "E1EDT13"
	
	oControls.SendSAPKey F2
	
	oControls.SetSAPModalOffsetEdit "SVALD-VALUE","31","11",sReqDateString
	
	oControls.ClickSAPModalEnter ()
	
	oControls.SendSAPKey F8
		
	oControls.ClickSAPModalEnter ()
	
	sIDocText = oControls.GetSAPModalEditValue ( "MESSTXT1","Information message")
	''MsgBox sIDocText
	aIDocArray = split(sIDocText, " ")
	
	Environment.Value ("IDoc99") = aIDocArray(2)
	''MsgBox aIDocArray(2)
	oControls.ClickSAPModalEnter ()
	
	'End Reporting 
	Reporter.EndFunction
	
	Reporter.EndStatusTrack

End Function
'=============================================================================================================
Function WE19_Mock_ASN_Process(sRefIDoc, sDelNo, sReqDateString, sSO)
	''Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Mock ASN Process - WE19","True"
	
	''entering T code
	oControls.SetSAPOKCode "/nVl02n"
	
	'enter outbound delivery no
	oControls.SetSAPEdit "LIKP-VBELN", sDelNo
	
	oControls.ClickSAPEnter ()
	
	oControls.SelectSAPTabStrip "TAXI_TABSTRIP_OVERVIEW","Picking"
	
	Set objTable = SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").SAPGuiTable("name:=SAPMV50ATC_LIPS_PICK","guicomponenttype:=80")
	sMaterialString = "" 
	irow = 1
	While objTable.GetCellData(irow,"Material")<>""
	
		If objTable.GetCellData(irow,"SLoc") <> "" Then
			
			sMaterialString = sMaterialString & objTable.GetCellData(irow,"Material")
	
		End If
		
		irow = irow + 1
		
	Wend
	
	sRowCount = irow - 1
	
	If mid(sMaterialString,1,1) = "#" Then
		sMaterialString = mid(sMaterialString,2)
	End If

	'' Éntering T code	
	oControls.SetSAPOKCode ("/nWE19")
	
	'enter outbound delivery no
	oControls.SetSAPEdit "MSED7START-EXIDOCNUM", sRefIDoc
	
	oControls.SendSAPKey F8
	
	oControls.SetSAPLabelFocus "EDIDC"
	
	oControls.SendSAPKey Ctrl_Shift_F11
	
	oControls.SetSAPLabelFocus "E1EDL20"
	
	oControls.SendSAPKey F2
	
	oControls.SetSAPModalOffsetEdit "SVALD-VALUE","31","0",sDelNo
	
	oControls.SetSAPModalOffsetEdit "SVALD-VALUE","31","9","TEST"
	
	oControls.ClickSAPModalEnter ()
	
	oControls.SetSAPLabelFocusUsingContent_Index "E1EDT13","0"

	oControls.SendSAPKey F2
	
	oControls.SetSAPModalOffsetEdit "SVALD-VALUE","31","11",sReqDateString
	
	oControls.SetSAPModalOffsetEdit "SVALD-VALUE","31","13",sReqDateString
	
	oControls.SetSAPModalOffsetEdit "SVALD-VALUE","31","16",sReqDateString
	
	oControls.ClickSAPModalEnter ()
	
	oControls.SetSAPLabelFocusUsingContent_Index "E1EDT13","1"

	oControls.SendSAPKey F2
	
	oControls.SetSAPModalOffsetEdit "SVALD-VALUE","31","11",sReqDateString
	
	oControls.SetSAPModalOffsetEdit "SVALD-VALUE","31","13",sReqDateString
	
	oControls.SetSAPModalOffsetEdit "SVALD-VALUE","31","16",sReqDateString
	
	oControls.ClickSAPModalEnter ()
	

aMaterialArray = Split(sMaterialString, "#")

iCount = ubound(aMaterialArray)+1

For i = 1 To iCount

	index = i-1
	''focus sap label
	oControls.SetSAPLabelFocusUsingContent_Index "E1EDL24",index
	''activate the label
	oControls.SendSAPKey F2
	
	sString  = "00" & i &"0"	
	iStringLen = len(sString)
	iStartString = iStringLen - 3	
	sItemNo = mid(sString,iStartString,4)
	
	oControls.SetSAPModalOffsetEdit "SVALD-VALUE","31",0,sItemNo
	''set the value
	oControls.SetSAPModalOffsetEdit "SVALD-VALUE","31",1,aMaterialArray(index)
		
	'set the value
	oControls.SetSAPModalOffsetEdit "SVALD-VALUE","31","12","Material Text"
	
	''click enter
	oControls.ClickSAPModalEnter ()	
	
	''label focus 
	oControls.SetSAPLabelFocusUsingContent_Index "E1EDL24",index
	
		If i < iCount Then
			''click copy button
			oControls.SendSAPKey SHIFT_F5
			''click paste button
			oControls.SendSAPKey SHIFT_F6
			''click at the same level button
			oControls.ClickSAPButton "SPOP-VAROPTION2"
			
		Else 
			
			Exit for
			
		End If 
		
	Next
	'	''''''''''	updated for multiple lineitems

	
	oControls.SendSAPKey F8
		
	oControls.ClickSAPModalEnter ()
	
	sIDocText = oControls.GetSAPModalEditValue ( "MESSTXT1","Information message")
	
	oControls.ClickSAPModalEnter ()
	
	'End Reporting 
	Reporter.EndFunction
	
	Reporter.EndStatusTrack

End Function
'=============================================================================================================
Function VL03N_DisplayOBDel(sOBDel)
	''Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Display Outbound Delivery - VL03N","True"
	'' Éntering T code
	oControls.SetSAPOKCode ("/nVL03N")
	
	oControls.SetSAPEdit "LIKP-VBELN", sOBDel
	oControls.ClickSAPEnter ()
	''Get Materials details
	arrtemp = GetSAPTabledata("SAPMV50ATC_LIPS_OVER", sOBDel)	
	REM ''msgbox "out get data"&arrtemp(1,1)
	oControls.SelectSAPMenuItem "Extras;Delivery Output;Header"
	''Need to insert validation for output type

	
	''Selecting the Lava row
	oControls.SelectSAPTableRow "SAPDV70ATC_NAST3", 1	
	''Clicking on Processing log
	oControls.ClickSAPButton "btn\[26\]"
	varIdocNo=oControls.GetSAPLabelContent("wnd\[1\]/usr/lbl\[6,6\]")
	REM ''msgbox varIdocNo
	oControls.ClickSAPButton "btn\[0\]"
	VL03N_DisplayOBDel=arrtemp	
'End Reporting 
Reporter.EndFunction
Reporter.EndStatusTrack
End Function
'=============================================================================================================
Function VL02N_ChangeDelevery_PGI(sDelNo)

''Starting the reporting
	Reporter.StartStatusTrack           
	Reporter.StartFunction "Post goods Issue - VL02N","True" 

	''entering T code
	oControls.SetSAPOKCode "/nVl02n"
	
	'enter outbound delivery no
	oControls.SetSAPEdit "LIKP-VBELN", sDelNo
	
	oControls.ClickSAPEnter ()
	
	oControls.SelectSAPTabStrip "TAXI_TABSTRIP_OVERVIEW", "Picking"
	
	
	Set objTable = SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").SAPGuiTable("name:=SAPMV50ATC_LIPS_PICK","guicomponenttype:=80")
	
	irow = 1
	While objTable.GetCellData(irow,"Material")<>""
	
	If objTable.GetCellData(irow,"Picked Qty") = "" Then
		
		objTable.SetCellData irow,"Picked Qty", objTable.GetCellData(irow,"Deliv. Qty")
		
	End If
		
	irow = 1 + irow
		
	Wend
	
	oControls.ClickSAPEnter ()
	
	'pgi
	oControls.ClickSAPButton "Post Goods Issue"
	
	var_order_no1 = oControls.GetSAPStatusBarInfo("item2")
	
	If var_order_no1 <> "" Then
		ocontrols.UpdateExecutionReport micPass,"PGI should be processed successfully", "Delivery Number '" & var_order_no1 & "' processed successfully"
	Else
		ocontrols.UpdateExecutionReport micFail,"PGI should be processed successfully", "Delivery Number '" & var_order_no1 & "' not processed successfully"
	End If
	VL02N_ChangeDelevery_PGI = var_order_no1
Reporter.EndFunction
Reporter.EndStatusTrack         
End Function 
'=============================================================================================================
Function VF01_CreateBilling(sDelNo)

''Starting the reporting
	Reporter.StartStatusTrack           
	Reporter.StartFunction "Create billing document and Tcode-VF01","True"
	
''entering T code
	oControls.SetSAPOKCode "/nVf01"         
	
'enter orderno in document field
	oControls.SetSAPTableCellData "SAPMV60ATCTRL_ERF_FAKT","1","Document",sDelNo

'press enter
	oControls.SendSAPkey ENTER	


	''Clicking on save button
	oControls.SelectSAPMenuItem "Billing document;Save"
	
	'to get the status message
	strCretedBilling = oControls.GetSAPStatusBarInfo("item1")
	
	
	''VF01_CreateBilling = "9005081926"
	If isnumeric(strCretedBilling) Then
		ocontrols.UpdateExecutionReport micPass,"Billing Document Number Should be created :"&VF01_CreateBilling ,"Billing Document Number Should be created :"&VF01_CreateBilling
	Else 
		ocontrols.UpdateExecutionReport micFail,"Billing Number Should not be created","Billing Document Number not created"
	End If
	
	'DataTable.Value("Billing") = VF01_CreateBilling ''Commented and changed to Environment.Value("Billing") = VF01_CreateBilling by Suresh
	VF01_CreateBilling = strCretedBilling
	Environment.Value("Billing") = VF01_CreateBilling
	
	
'End Reporting 
Reporter.EndFunction
Reporter.EndStatusTrack  
End Function 
 '=============================================================================================================
Function VA03_GetDelNum(oDetails)
	''Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Checking if the order got delevered if not delivery and get Delivery number","True"
	'' Éntering T code
	oControls.SetSAPOKCode ("/nVA03")
	''Entering Sales Order
	sSO=oDetails.item("ORDER_NUMBER")
	REM ''msgbox "in common" & sSO
	oControls.SetSAPEdit "VBAK-VBELN", sSO
	''Enter
	oControls.ClickSAPEnter ()
	''Get the status bar information
	statusVA03=oControls.GetSAPStatusBarInfo("itemscount")
	VA03_GetDelNum=""
	If statusVA03=0 Then
		''Validating Order reason
		varOrderReasonHY=oDetails.item("OrderType")
		varOrderReasonECC=oControls.GetSAPComboBoxValue("VBAK-AUGRU")
		If Ucase(varOrderReasonHY)=Ucase(replace(varOrderReasonECC, "_", "")) Then
		 oControls.UpdateExecutionReport micPass, "VALIDATION-ORDERREASON", "OrderReason in ECC is matching with Hybris " & varOrderReasonHY & "=" & varOrderReasonECC 
		 Else
		 oControls.UpdateExecutionReport micFail, "VALIDATION-ORDERREASON", "OrderReason in ECC is not matching with Hybris " & varOrderReasonHY & "=" & varOrderReasonECC 
		End if
		
		
		''VALIDATION-ORDER PAYMENT CARD TYPE
		varPayCardTypeHY=oDetails.item("CARD_TYPE")
		varPayCardTypeECC=oControls.GetSAPEditValue("CCDATA-CCINS")
		If ucase(varPayCardTypeHY)=Ucase(varPayCardTypeECC) Then
		 oControls.UpdateExecutionReport micPass, "VALIDATION-ORDER PAYMENT CARD TYPE", "Payment card type in ECC is matching with Hybris " & varPayCardTypeHY & "=" & varPayCardTypeECC 
		 Else
		 oControls.UpdateExecutionReport micFail, "VALIDATION-ORDER PAYMENT CARD TYPE", "Payment card type is not matching with Hybris " & varPayCardTypeHY & "=" & varPayCardTypeECC 
		End if	
		
		
		''VALIDATION-PRICING GROUP
		''Clicking on Display header details button
		oControls.ClickSAPButton "BT_HEAD"
		''Selecting "Sales"
		oControls.SelectSAPTabStrip "TAXI_TABSTRIP_HEAD", "Sales"
		varPiceGroupHY=oDetails.item("CustomerType")
		varPiceGroupECC=oControls.GetSAPComboBoxValue("VBKD-KONDA")
		If varPiceGroupHY=varPiceGroupECC Then
		 oControls.UpdateExecutionReport micPass, "VALIDATION-PRICING GROUP", "Priceing Group(Customer type) in ECC is matching with Hybris " & varPiceGroupHY & "=" & varPiceGroupECC 
		 Else
		 oControls.UpdateExecutionReport micFail, "VALIDATION-PRICING GROUP", "Priceing Group(Customer type) in ECC is not matching with Hybris " & varPiceGroupHY & "=" & varPiceGroupECC 
		End if	
		
		''Validating Order Amount
		''Selecting "Payment cards"
		oControls.SelectSAPTabStrip "TAXI_TABSTRIP_HEAD", "Payment cards"
		varOrdAmountHY=oDetails.item("TOTAL_PRICE")
		varOrdAmountECC=oControls.GetSAPTableCellData ("SAPLV60FTCTRL_ZAHLUNGSKARTEN", 2, "Authorized amt")
		If varOrdAmountHY=varOrdAmountECC Then
		 oControls.UpdateExecutionReport micPass, "VALIDATION-ORDER AMOUNT", "Order amount in ECC is matching with Hybris " & varOrdAmountHY & "=" & varOrdAmountECC 
		 Else
		 oControls.UpdateExecutionReport micFail, "VALIDATION-ORDER AMOUNT", "Order amount in ECC is not matching with Hybris " & varOrdAmountHY & "=" & varOrdAmountECC 
		End if	
		
		''Validating Net Amount and Tax
		''Selecting "Conditions"
		oControls.SelectSAPTabStrip "TAXI_TABSTRIP", "Conditions"
		varNetAmountHY=oDetails.item("TOTAL_PRICE")
		varEstTaxHY=oDetails.item("TOTAL_PRICE")
		
		varNetAmountECC=oControls.GetSAPEditValue("KOMP-NETWR")
		varNetAmountECC=cdbl(varNetAmountECC)
		varEstTaxECC=oControls.GetSAPEditValue("KOMP-MWSBP") 
		varEstTaxECC=cdbl(varEstTaxECC)
		varConAmountECC=varNetAmountECC+varEstTaxECC
		If varNetAmountHY=varConAmountECC Then
		 oControls.UpdateExecutionReport micPass, "VALIDATION-ORDER AMOUNT", "Order amount in ECC is matching with Hybris " & varNetAmountHY & "=" & varConAmountECC 
		 Else
		 oControls.UpdateExecutionReport micFail, "VALIDATION-ORDER AMOUNT", "Order amount in ECC is not matching with Hybris " & varNetAmountHY & "=" & varConAmountECC 
		End if

	
		
		
		oControls.SelectSAPTabStrip "TAXI_TABSTRIP_HEAD", "Sales"		
		oControls.ClickSAPButton "btn\[5\]"
		set ObjSAPGuiTree=SAPGuiSession("micclass:=SAPGuiSession").SAPGuiWindow("micclass:=SAPGuiWindow").SAPGuiTree("micclass:=SAPGuiTree").Object
		Set ObjKeyValues = ObjSAPGuiTree.GetAllNodeKeys
		intNodeCount = ObjKeyValues.Count
			flagDel=False
			For i = 0 to intNodeCount-1
				'Get the node text
				strNodeText=ObjSAPGuiTree.GetNodeTextByKey(ObjKeyValues(i))
				If Instr(strNodeText,"Delivery")>0 Then
					'Select the node and double click on it
					'This is equivalent to ActivateItem
					ObjSAPGuiTree.SelectNode ObjKeyValues(i)
					''''msgbox strNodeText
					arrDel=split(strNodeText, " ")
					tempDelNo= arrDel(1)
					flagDel=True
					Exit For
				End If
			Next 
		if flagDel=False Then
		REM ''msgbox "not Delivered"
		REM tempDelNo=VL01N_CreateDel(sSO)
		End if
		if tempDelNo<>"" then 
			oControls.UpdateExecutionReport micPass, "GetSAPStatusBarInfo", "Sales Order was successfully displayed in VA03"
			VA03_GetDelNum=tempDelNo
		Else
			 oControls.UpdateExecutionReport micFail, "GetSAPStatusBarInfo", "Unable to display Sales order in VA03-Error message '"&varTmpstsVA03n&"'" 
		End if
     Else
	 varTmpstsVA03n=oControls.GetSAPStatusBarInfo("text")
	 oControls.UpdateExecutionReport micFail, "GetSAPStatusBarInfo", "Unable to display Sales order in VA03-Error message '"&varTmpstsVA03n&"'" 
	 Exittest
	End if
    'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
End function
'=============================================================================================================
Function GetSAPTabledata(sSAPTable, sOBDel)
	' @HELP
	' @class:		Controls_SAP
	' @method:   	GetSAPTabledata(sSAPTable)
	' @returns:  	GetSAPTabledata	:	Array
	' @parameter:	sWindow		:	Name of the Session
	' @parameter:   sSAPTable		: 	Name of the Table
	' @notes:   	Returns the data present in the table into an array
	' @END  
	

	If InStr(sSAPTable,"_") > 0 then
		Set oDesc = Description.Create()
		oDesc("name").Value = sSAPTable
	Else
		Set oDesc = Description.Create()
		oDesc("text").Value = sSAPTable
	End If
	oSAPObj=""
	If 	SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=22").Exist Then	
		Set oSAPObj=SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=22")
		sScreenName = oSAPObj.GetROProperty("text")
	Elseif SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").Exist Then	
		Set oSAPObj=SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21")
		sScreenName = oSAPObj.GetROProperty("text")
	End if
	iRowCount=oSAPObj.SAPGuiTable(oDesc).RowCount	
		datacnt=0
		For rw = 1 to iRowCount
		temp=oSAPObj.SAPGuiTable(oDesc).ValidRow(rw)
		If temp Then
			datacnt=datacnt+1
		Else
		Exit for
		End If    
		Next
	REM ''msgbox datacnt
	'' Éntering T code
	oControls.SetSAPOKCode ("/nVL03N")
	oControls.SetSAPEdit "LIKP-VBELN", sOBDel
	oControls.ClickSAPEnter ()
	ReDim arrdata(datacnt, 5)
	For i=1 to datacnt
	arrdata(i, 1)=oSAPObj.SAPGuiTable(oDesc).GetCellData(i,"Itm")
	print arrdata(i, 1)
	arrdata(i, 2)=oSAPObj.SAPGuiTable(oDesc).GetCellData(i,"Material")
	print arrdata(i, 2)
	arrdata(i, 3)=oSAPObj.SAPGuiTable(oDesc).GetCellData(i,"Deliv. Qty")
	print arrdata(i, 3)
	arrdata(i, 4)=oSAPObj.SAPGuiTable(oDesc).GetCellData(i,"Description")
	print arrdata(i, 4)
	arrdata(i, 5)=oSAPObj.SAPGuiTable(oDesc).GetCellData(i,"Picked Qty")
	print arrdata(i, 5)
	next 
	GetSAPTabledata=arrdata
	REM ''msgbox "in get data"&arrdata(1,1)
End Function
'=============================================================================================================
Function fnVA01SO_WithRef()

	''Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Creating Sales Order with referance","True"
	'' Éntering T code
	oControls.SetSAPOKCode ("/nVA01")
	''Entering Order Type
	oControls.SetSAPEdit "VBAK-AUART", "ZUS1"
	''Entering Sales Organization
	oControls.SetSAPEdit "VBAK-VKORG", "1710"
	''Entering Distribution Channel
	oControls.SetSAPEdit "VBAK-VTWEG", "10"
	''Entering Division
	oControls.SetSAPEdit "VBAK-SPART", "10"	
	''Clicking on Create with referance button
	oControls.ClickSAPButton "btn\[8\]"
	''Selecting Order tab
	oControls.SelectSAPTabStrip "MYTABSTRIP", "Order"
	''Entering order number
	oControls.SetSAPEdit "LV45C-VBELN", "6236469"
	oControls.ClickSAPButton "Copy"
	oControls.ClickSAPButton "btn\[0\]"
	oControls.ClickSAPButton "btn\[0\]"
	REM oControls.ClickSAPButton "btn\[0\]"
	REM oControls.ClickSAPButton "btn\[0\]"
	''Entering CC Type
	oControls.SetSAPEdit "CCDATA-CCINS", "VISA"	
	''Entering CC no
	oControls.SetSAPEdit "CCDATA-CCNUM", "-E803-1111-FWZW7FT203ME6W"		
	''Entering CC exp
	oControls.SetSAPEdit "CCDATE-EXDATBI", "07/20"		
	''Entering CC CVV
	oControls.SetSAPEdit "CCARD_CVV-CVVAL", "123"	
	''Clicking on Display header details button
	oControls.ClickSAPButton "BT_HEAD"
	''Selecting Additional data B tab
	oControls.SelectSAPTabStrip "TAXI_TABSTRIP_HEAD", "Additional data B"
	''Entering Account Number
	oControls.SetSAPEdit "VBAK-ZZACCOUNTID", "12345"		
	''Entering Consultant ID
	oControls.SetSAPEdit "VBAK-ZZCONSULTID", "1234"		
	''Clicking on save button
	oControls.ClickSAPSaveButton()
	''Clicking on save button
	statusVA01=oControls.GetSAPStatusBarInfo("item2")
	fnVA01SO_WithRef=statusVA01
'End Reporting 
Reporter.EndFunction
Reporter.EndStatusTrack
End function
'=============================================================================================================
Function VL03N_ValidateDeliveryStatus (sExpectedBillofLadding, sExpectedStatusCode, sDelNo)
	''Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Validate Delivery VL03N","True"
	'' Éntering T code		
	oControls.SetSAPOKCode ("/nVL03N")
	
	'enter outbound delivery no
	oControls.SetSAPEdit "LIKP-VBELN", sDelNo
	
	oControls.ClickSAPEnter ()
	
	''Clicking on save button
	oControls.SelectSAPMenuItem "Goto;Item;Processing"
	
	ocontrols.SelectSAPTabStrip "TAXI_TABSTRIP_ITEM","Additional Data"
	
	sActualBillOfLadding = ocontrols.GetSAPEditValue ( "LIPS-ZZBOLNR")
	
	If lcase(trim(sActualBillOfLadding)) = lcase(trim(sExpectedBillofLadding)) Then
		ocontrols.UpdateExecutionReport micPass,"Bill of ladding value should be '" & sExpectedBillofLadding & "'", "Bill of ladding value is '" & sActualBillOfLadding & "'"
	Else
		ocontrols.UpdateExecutionReport micFail,"Bill of ladding value should be '" & sExpectedBillofLadding & "'", "Bill of ladding value is '" & sActualBillOfLadding & "'"
	End If
		
	oControls.SendSAPKey F3
	
	''Clicking on save button
	oControls.SelectSAPMenuItem "Goto;Header;Processing"
	
	ocontrols.SelectSAPTabStrip "TAXI_TABSTRIP_HEAD","Addtional Data"
	
	sActualStatusCode = ocontrols.GetSAPEditValue ( "LIKP-ZZSTATUS_CODE")
	
	If lcase(trim(sExpectedStatusCode)) = lcase(trim(sActualStatusCode)) Then
		ocontrols.UpdateExecutionReport micPass,"Status Code value should be '" & sExpectedStatusCode & "'", "Bill of ladding value is '" & sActualStatusCode & "'"
	Else
		ocontrols.UpdateExecutionReport micFail,"Status Code value should be '" & sExpectedStatusCode & "'", "Bill of ladding value is '" & sActualStatusCode & "'"
	End If
	
	oControls.SendSAPKey F3
	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
End Function
'=============================================================================================================
Function VL03N_ValidatePickingRequest (sDelNo)
	''Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Display Delivery VL03N","True"
	'' Éntering T code		
	oControls.SetSAPOKCode ("/nVL03N")
	
	'enter outbound delivery no
	oControls.SetSAPEdit "LIKP-VBELN", sDelNo
	
	oControls.ClickSAPEnter ()
	
	oControls.SendSAPKey F7
	
	oControls.ClickSAPToolBarButton "&FIND"
	
	oControls.SetSAPModalEdit "LVC_S_SEA-STRING", "Cell Content", "Picking request"
	
	oControls.ClickSAPModalEnter ()
	
	oControls.CloseSAPModalWindow ()
	
	oControls.SendSAPKey F2
	
	sPickingRequest = oControls.GetSAPGridCellData ( 1,"Doc.no." )
	
	If sPickingRequest <> "" Then
		ocontrols.UpdateExecutionReport micPass,"Picking Request to be created", "Picking Request '" & sPickingRequest & "' created"
	Else
		ocontrols.UpdateExecutionReport micFail,"Picking Request to be created", "Picking Request not created"
	End If
	
	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
End Function
'=============================================================================================================
Function VL03N_ValidateDeliveryOutput (sDelNo)
	''Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Change Delivery VL02N","True"
	'' Éntering T code		
	oControls.SetSAPOKCode ("/nVL03N")
	
	'enter outbound delivery no
	oControls.SetSAPEdit "LIKP-VBELN", sDelNo
	
	oControls.ClickSAPEnter ()
	
	oControls.SelectSAPMenuItem "Extras;Delivery Output;Header"
	
	
	
	sStatus1 = oControls.GetSAPTableCellData ("SAPDV70ATC_NAST3",1, "Status")
	sStatus2 = oControls.GetSAPTableCellData ("SAPDV70ATC_NAST3",2, "Status")
	
	sOutput1 = oControls.GetSAPTableCellData ("SAPDV70ATC_NAST3",1, "Output Type")
	sOutput2 = oControls.GetSAPTableCellData ("SAPDV70ATC_NAST3",2, "Output Type")
	
	sDescription1 = oControls.GetSAPTableCellData ("SAPDV70ATC_NAST3",1, "Description")
	sDescription2 = oControls.GetSAPTableCellData ("SAPDV70ATC_NAST3",2, "Description")
	
	If sStatus1 = "S_TL_G" Then
		ocontrols.UpdateExecutionReport micPass,"Row 1 - Delivery Output for " & sDelNo, "Status :: Pass # Output Type :: " & sOutput1 & " # Description :: " & sDescription1
	Else
		ocontrols.UpdateExecutionReport micFail,"Row 1 - Delivery Output for " & sDelNo, "Status :: Pass # Output Type :: " & sOutput1 & " # Description :: " & sDescription1
	End If
	
	If sStatus2 = "S_TL_G" Then
		ocontrols.UpdateExecutionReport micPass,"Row 2 - Delivery Output for " & sDelNo, "Status :: Pass # Output Type :: " & sOutput2 & " # Description :: " & sDescription2
	Else
		ocontrols.UpdateExecutionReport micFail,"Row 2 - Delivery Output for " & sDelNo, "Status :: Pass # Output Type :: " & sOutput2 & " # Description :: " & sDescription2
	End If
	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
End Function
'=============================================================================================================
Function VF03_ValidateAccountingDocument (sBilling)
	''Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Display Billing Document","True"
	'' Éntering T code		
	oControls.SetSAPOKCode ("/nVF03")
	
	'enter outbound delivery no
	oControls.SetSAPEdit "VBRK-VBELN", sBilling
	
	oControls.ClickSAPEnter ()
	
	oControls.SendSAPKey Shift_F7
	
	oControls.ClickSAPToolBarButton "&FIND"
	
	oControls.SetSAPModalEdit "LVC_S_SEA-STRING", "Cell Content", "Accounting document"
	
	oControls.ClickSAPModalEnter ()
	
	oControls.CloseSAPModalWindow ()
	
	oControls.SendSAPKey F2
	
	sAccountingDocument = oControls.GetSAPGridCellData ( 1,"Doc.no." )
	
	If sAccountingDocument <> "" Then
		ocontrols.UpdateExecutionReport micPass,"Accounting Document to be created", "AccountingDocument " & sAccountingDocument & " created"
	Else
		ocontrols.UpdateExecutionReport micFail,"Accounting Document to be created", "AccountingDocument not created"
	End If
	
	
	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
End Function
'=============================================================================================================
Function SE16N_ReadIDocNumber(strSalesOrder)

	'Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Read IDoc number from ZSD_ORDSTATUS Table","True"
	'' Éntering T code
	oControls.SetSAPOKCode ("/nse16n")
	
	oControls.SetSAPEdit "GD-TAB", "ZSD_ORDSTATUS"

	'Click Enter on Header Screen
	oControls.ClickSAPEnter ()
	Wait(5)

	Set objSE16NTable = SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").SAPGuiTable("name:=SAPLSE16NSELFIELDS_TC")
	intFldNameRowNo = objSE16NTable.FindRowByCellContent ("Fld name","Sales Document")
	oControls.SetSAPTableCellData "SAPLSE16NSELFIELDS_TC", intFldNameRowNo, "Fr.Value", Trim(strSalesOrder)
	
	'Click on Execute Button
	oControls.SendSAPkey F8
	Wait(5)
	
	SE16N_ReadIDocNumber = oControls.GetSAPGridCellData (1, "IDoc number")
	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
	
End Function

'=============================================================================================================
Function WE02_ReadOrderStatusDetails(intIDocNumber)

	'Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Read Order Stauts Details from WE02","True"
	'' Éntering T code
	oControls.SetSAPOKCode ("/nwe02")

	oControls.SetSAPEdit "CREDAT-LOW", ""
	oControls.SetSAPEdit "CREDAT-HIGH", ""
	
	oControls.SetSAPEdit "DOCNUM-LOW", intIDocNumber 
	oControls.SendSAPKey F8
	Wait(5)
	iIndex = 0
	SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").SAPGuiTree("guicomponenttype:=200","index:="&iIndex).SelectItem "IDoc 00000000" & intIDocNumber & ";Data records;Segment 000001","ZHEADER"
	SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").SAPGuiTree("guicomponenttype:=200","index:="&iIndex).ClickLink "IDoc 00000000" & intIDocNumber & ";Data records;Segment 000001","ZHEADER"
	
	strOrderStatus = oControls.GetSAPTableCellData ("IDOC_TREE_CONTROLINT_SEG_CONTROL",2, "Fld Cont.")
	
	SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").SAPGuiTree("guicomponenttype:=200","index:="&iIndex).ClickLink "IDoc 00000000" & intIDocNumber & ";Data records;Segment 000002;Segment 000003","ZDELITEM"
	
	strCarrier = oControls.GetSAPTableCellData ("IDOC_TREE_CONTROLINT_SEG_CONTROL",4, "Fld Cont.") 
	intTrackingNumber  = oControls.GetSAPTableCellData ("IDOC_TREE_CONTROLINT_SEG_CONTROL",5, "Fld Cont.")
	
	WE02_ReadOrderStatusDetails = strOrderStatus &"," & strCarrier & "," & intTrackingNumber

	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
	
End Function
'=============================================================================================================
Function VA03_ValidateDeliveryCancellation_and_DebitMemoCreation(sSO)
	''Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Display Sales Order VA03","True"
	'' Éntering T code
	oControls.SetSAPOKCode ("/nVA03")
	''Entering Sales Order

	oControls.SetSAPEdit "VBAK-VBELN", sSO
	''Enter
	oControls.ClickSAPEnter ()
	
	oControls.SendSAPKey F5
	
	oControls.ClickSAPToolBarButton "&FIND"
	
	oControls.SetSAPModalEdit "LVC_S_SEA-STRING", "Cell Content", "Delivery"
	
	oControls.ClickSAPModalEnter ()
	
	If oControls.VerifySAPModalEdit ( "MESSTXT1","Information Message") = True Then		
		ocontrols.UpdateExecutionReport micPass,"Delivery should be canceled", "No Delivery found"	
	Else		
		ocontrols.UpdateExecutionReport micFail,"Delivery should be cancelled", "Delivery not cancelled"	
	End If 
	
	oControls.CloseSAPModalWindow ()
	
	oControls.ClickSAPToolBarButton "&FIND"
	
	oControls.SetSAPModalEdit "LVC_S_SEA-STRING", "Cell Content", "Debit Memo"
	
	oControls.ClickSAPModalEnter ()
	
	oControls.CloseSAPModalWindow ()
	
	oControls.SendSAPKey F2
	
	sDebitMemo = oControls.GetSAPGridCellData ( 1,"Doc.no." )
	
	If sDebitMemo <> "" Then
	
		ocontrols.UpdateExecutionReport micPass,"Debit Memo to be created", "Debit Memo " & sDebitMemo & " created"
		
	Else
	
		ocontrols.UpdateExecutionReport micFail,"Debit Memo to be created", "Debit Memo not created"
		
	End If
	
	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
End function
'=============================================================================================================
Function VA02_UpdateSO(sSO)
	''Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Change Sales Order - VA02","True"
	'' Éntering T code
	oControls.SetSAPOKCode ("/nVA02")
	''Entering Sales Order
	oControls.SetSAPEdit "VBAK-VBELN", sSO
	''Enter
	oControls.ClickSAPEnter ()
	
	If oControls.VerifySAPModalWindowExistance () Then		
		oControls.ClickSAPModalEnter ()	
	End If
	
	''Get the status bar information
	statusVA03 = oControls.GetSAPStatusBarInfo("itemscount")
	
	If statusVA03=0 Then
      oControls.UpdateExecutionReport micPass, "GetSAPStatusBarInfo", "Sales Order was successfully displayed in VA02"
     Else
	 varTmpstsVA03n=oControls.GetSAPStatusBarInfo("text")
	 oControls.UpdateExecutionReport micFail, "GetSAPStatusBarInfo", "Unable to display Sales order in VA02-Error message '"&varTmpstsVA03n&"'" 
	End if
	
	iCurRow = 1
	While oControls.GetSAPTableCellData("SAPMV45ATCTRL_U_ERF_AUFTRAG",iCurRow, "Material" ) <> ""
		If oControls.GetSAPTableCellData("SAPMV45ATCTRL_U_ERF_AUFTRAG",iCurRow, "Order Quantity") = "" Then			
			oControls.SetSAPTableCellData "SAPMV45ATCTRL_U_ERF_AUFTRAG",iCurRow,"Order Quantity", "1"
		End If
		iCurRow = iCurRow + 1
	Wend
	
	''Enter
	oControls.ClickSAPEnter ()
	
	oControls.SelectSAPMenuItem "Edit;Incompletion log"
	''Get the status bar information
	If oControls.GetSAPStatusBarInfo("text") = "S:Document is complete" Then
		oControls.SelectSAPMenuItem "Sales document;Save"
		oControls.UpdateExecutionReport micPass, "GetSAPStatusBarInfo", oControls.GetSAPStatusBarInfo("text")
	End  If
	
	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
End function
'=============================================================================================================
Function VA03_GetDelNum(sSO)
	''Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Validate Sales Order - VA03","True"
	
	oControls.SetSAPOKCode ("/nVA03")
	''Entering Sales Order

	oControls.SetSAPEdit "VBAK-VBELN", sSO
	
	''Enter
	oControls.ClickSAPEnter ()
	''Get the status bar information
	
	varOrderReasonECC=oControls.GetSAPComboBoxValue("VBAK-AUGRU")
	
	varPayCardTypeECC=oControls.GetSAPEditValue("CCDATA-CCINS")
	
	''Clicking on Display header details button
	oControls.ClickSAPButton "BT_HEAD"
	
	''Selecting "Sales"
	oControls.SelectSAPTabStrip "TAXI_TABSTRIP_HEAD", "Sales"
	
	varPiceGroupECC=oControls.GetSAPComboBoxValue("VBKD-KONDA")
		
	''Validating Order Amount
	''Selecting "Payment cards"
	oControls.SelectSAPTabStrip "TAXI_TABSTRIP_HEAD", "Payment cards"
		
	varOrdAmountECC=oControls.GetSAPTableCellData ("SAPLV60FTCTRL_ZAHLUNGSKARTEN", 2, "Authorized amt")
	
	''Validating Net Amount and Tax
	''Selecting "Conditions"
	oControls.SelectSAPTabStrip "TAXI_TABSTRIP", "Conditions"
	
	varNetAmountECC=oControls.GetSAPEditValue("KOMP-NETWR")

	varEstTaxECC=oControls.GetSAPEditValue("KOMP-MWSBP") 

	varConAmountECC = cdbl(trim(varNetAmountECC)) +  cdbl(trim(varEstTaxECC))
	
    'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
End function
'=============================================================================================================
Function BD87_ProcessIDocs()

	''Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Process IDocs ready to be transfer to Application - BD87","True"
	
	oControls.SetSAPOKCode ("/nBD87")
	''Entering Sales Order

	oControls.SendSAPKey F8
	
	oControls.SelectSAPTreeNode "0", "ECQ Client 100;IDoc in inbound processing;IDoc ready to be transferred to application;ZADR2MAS"
	
	oControls.ClickSAPButton "Process"
	
	statusVA03 = oControls.GetSAPStatusBarInfo("messagetype")
	
	If statusVA03 <> "E" Then
		oControls.SendSAPKey F3
		oControls.UpdateExecutionReport micPass, "IDocs of type 'ZADR2MAS' should be processed", "IDocs of type 'ZADR2MAS' are processed"
	Else
		oControls.UpdateExecutionReport micPass, "IDocs of type 'ZADR2MAS' should be processed",oControls.GetSAPStatusBarInfo("text")
	End  IF
	
	oControls.SelectSAPTreeNode "0", "ECQ Client 100;IDoc in inbound processing;IDoc ready to be transferred to application;ZDEBMAS"
	
	oControls.ClickSAPButton "Process"
	
	statusVA03 = oControls.GetSAPStatusBarInfo("messagetype")
	
	If statusVA03 <> "E" Then
		oControls.SendSAPKey F3
		oControls.UpdateExecutionReport micPass, "IDocs of type 'ZDEBMAS' should be processed", "IDocs of type 'ZDEBMAS' are processed"
	Else
		oControls.UpdateExecutionReport micPass, "IDocs of type 'ZDEBMAS' should be processed",oControls.GetSAPStatusBarInfo("text")
	End  IF
	
	oControls.SelectSAPTreeNode "0", "ECQ Client 100;IDoc in inbound processing;IDoc ready to be transferred to application;ZSALESORDER_CREATEFROMDAT2"
	
	oControls.ClickSAPButton "Process"
	
	statusVA03 = oControls.GetSAPStatusBarInfo("messagetype")
	
	If statusVA03 <> "E" Then
		oControls.SendSAPKey F3
		oControls.UpdateExecutionReport micPass, "IDocs of type 'ZSALESORDER_CREATEFROMDAT2' should be processed", "IDocs of type 'ZSALESORDER_CREATEFROMDAT2' are processed"
	Else
		oControls.UpdateExecutionReport micPass, "IDocs of type 'ZSALESORDER_CREATEFROMDAT2' should be processed",oControls.GetSAPStatusBarInfo("text")
	End  IF
	
	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
	
End function

'=============================================================================================================

Function OrderValidation (arrCSVData, Iterator)
	
	''Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Order Validation for " & Iterator & " - Row of CSV File","True"
	
	' Éntering T code
	oControls.SetSAPOKCode ("/nVA03")
	''Entering Sales Order
	
	Set myxl = createobject("excel.application")
	myxl.Workbooks.Open "C:\RF_Share\Order_Validation.xlsx" 
	myxl.Application.Visible = FALSE
	Set mysheet = myxl.ActiveWorkbook.Worksheets("Sheet1")
	
	''oControls.SetSAPEdit "VBAK-VBELN",mysheet.cells(2,2).value
	oControls.SetSAPEdit "VBAK-VBELN", Split(arrCSVData,",")(1)
	
	''Enter
	oControls.ClickSAPEnter ()
	oControls.ClickSAPEnter ()
	
	sDeliveryBlock = oControls.GetSAPComboBoxValue ("VBAK-LIFSK")
	
	sBillingBlock = oControls.GetSAPComboBoxValue ("VBAK-FAKSK")
	
	If sDeliveryBlock = ""  Then
		ocontrols.UpdateExecutionReport micPass,"ScreenValidation: There should be no Delivery Block","There is no delivery block"
	Else 
		ocontrols.UpdateExecutionReport micFail,"ScreenValidation: There should be no Delivery Block","There is a delivery block - " & sDeliveryBlock
	End If
	
	If sBillingBlock = ""  Then
		ocontrols.UpdateExecutionReport micPass,"ScreenValidation: There should be no Billing Block","There is no Billing block"
	Else 
		ocontrols.UpdateExecutionReport micFail,"ScreenValidation: There should be no Billing Block","There is a delivery block - " & sBillingBlock
	End If
	
	oControls.ClickSAPButton "BT_HEAD"
	''order type
	OrderType = oControls.GetSAPEditValue ("VBAK-AUART")
	
	''order Reason
	OrderReason = oControls.GetSAPComboBoxValue ("VBAK-AUGRU")
	
	
	''Order Reason NOT provide in given CSV File
	''OrderReasonFrmxl = mysheet.cells(2,3).value
	
'	If OrderReason = OrderReason  Then
'		ocontrols.UpdateExecutionReport micPass,"ScreenValidation: Hybris Order Reason is:"&OrderReasonFrmxl ,"ECC Order Reason is:"&OrderReason
'	Else 
'		ocontrols.UpdateExecutionReport micFail,"ScreenValidation: Hybris Order Reason is:"&OrderReasonFrmxl ,"Hybris Order Reason is:"&OrderReason
'	End If
	
	PricingGroup = oControls.GetSAPComboBoxValue ("VBKD-KONDA")
	
	'Price Group NOT provide in given CSV File
	''PricingGroupFrmxl = mysheet.cells(2,4).value
	
'	If instr(1,PricingGroupFrmxl,PricingGroup) > 0  Then
'		ocontrols.UpdateExecutionReport micPass,"ScreenValidation: Hybris Order Pricing Group is:"&PricingGroupFrmxl ,"Hybris Order Pricing Group is:"&PricingGroup
'	Else 
'		ocontrols.UpdateExecutionReport micFail,"ScreenValidation: Hybris Order Pricing Group is:"&PricingGroupFrmxl ,"Hybris Order Pricing Group is:"&PricingGroup
'	End If
'	
	''select Shipping tab
	oControls.SelectSAPTabStrip "TAXI_TABSTRIP_HEAD","Shipping"
	
	''order ShippingCondition
	ShippingCondition = oControls.GetSAPComboBoxValue ("VBAK-VSBED")
	
	''Shiping Condition NOT provide in given CSV File
	''ShippingConditionFrmxl = mysheet.cells(2,5).value
	
'	If instr(1,ShippingConditionFrmxl,ShippingCondition) > 0  Then
'		ocontrols.UpdateExecutionReport micPass,"ScreenValidation: Hybris Order Shipping Condition is:"&ShippingConditionFrmxl ,"Hybris Order Shipping Condition is:"&ShippingCondition
'	Else 
'		ocontrols.UpdateExecutionReport micFail,"ScreenValidation: Hybris Order Shipping Condition is:"&ShippingConditionFrmxl ,"Hybris Order Shipping Condition is:"&ShippingCondition
'	End If
	
	
	''select Payment cards tab
	oControls.SelectSAPTabStrip "TAXI_TABSTRIP_HEAD","Payment cards"
	
	oControls.SelectSAPTableRow "SAPLV60FTCTRL_ZAHLUNGSKARTEN",2
	
	oControls.SendSAPkey F2

	AuthorizedAmt = oControls.GetSAPEditValue ("FPLTC-AUTWR")
	AuthorizedAmt = CDbl(AuthorizedAmt)
	
	'CreditCard_authorizedAmount NOT provided in CSV file
	'AuthorizedAmtFrmxl = mysheet.cells(2,8).value
'	If replace(AuthorizedAmt, "$", "") = replace(AuthorizedAmtFrmxl, "$", "") Then
'		ocontrols.UpdateExecutionReport micPass,"ScreenValidation: Hybris OrderAuthorized Amount is:"&AuthorizedAmtFrmxl ,"Hybris OrderAuthorized Amount is:"&AuthorizedAmt
'	Else 
'		ocontrols.UpdateExecutionReport micFail,"ScreenValidation: Hybris OrderAuthorized Amount is:"&AuthorizedAmtFrmxl ,"Hybris OrderAuthorized Amount is:"&AuthorizedAmt
'	End If
	
	CardType = oControls.GetSAPEditValue ("FPLTC-CCINS")
	'CreditCard_Type NOT provided in CSV file
	''CardTypeFrmxl = mysheet.cells(2,9).value
	
'	If CardType = CardTypeFrmxl  Then
'		ocontrols.UpdateExecutionReport micPass,"ScreenValidation: Hybris Order Credit Card Type is:"&CardTypeFrmxl ,"Hybris Order Credit Card Type is:"&CardType
'	Else 
'		ocontrols.UpdateExecutionReport micFail,"ScreenValidation: Hybris Order Credit Card Type is:"&CardTypeFrmxl ,"Hybris Order Credit Card Type is:"&CardType
'	End If
'	
	NetValue = oControls.GetSAPEditValue ("KOMPAX-NETWR")
	
	NetValue = CDbl(NetValue)
	TaxAmount = oControls.GetSAPEditValue ("FPLAA-MWSBK")
	TaxAmount = CDbl(TaxAmount)
	''TaxAmountFrmxl = mysheet.cells(2,11).value
	TaxAmountFrmxl = Split(arrCSVData,",")(4)
	
	If replace(TaxAmount,"$","") = replace(TaxAmountFrmxl,"$","")   Then
		ocontrols.UpdateExecutionReport micPass,"ScreenValidation: Hybris Order Tax Amount is:"&TaxAmountFrmxl ,"ECC Order Tax Amount is :"&TaxAmount
	Else 
		ocontrols.UpdateExecutionReport micFail,"ScreenValidation: Hybris Order Tax Amount is:"&TaxAmountFrmxl ,"ECC Order Tax Amount is :"&TaxAmount
	End If
	
	
	iTotalAmount = TaxAmount + NetValue
	if iTotalAmount = AuthorizedAmt then
		ocontrols.UpdateExecutionReport micPass,"ScreenValidation: ECC Order Total Amount "& AuthorizedAmt &" should be sum of Net Value and Tax amount" ,"ECC Order Net Amount is :"&NetValue & ", Tax Amount is :"&TaxAmount
	Else
		ocontrols.UpdateExecutionReport micFail,"ScreenValidation: ECC Order Total Amount "& AuthorizedAmt &" should be sum of Net Value and Tax amount" ,"ECC Order Net Amount is :"&NetValue & ", Tax Amount is :"&TaxAmount
	End If
	oControls.SendSAPkey F3
	
	oControls.SelectSAPTabStrip "TAXI_TABSTRIP","Partners"
	
	iloopcount = 1
	
	On error resume next
	
	Set oCustomerTable = SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").SAPGuiTable("name:=SAPLV09CGV_TC_PARTNER_OVERVIEW")
	
	Do while oCustomerTable.GetCellData (iloopcount,"Partner")<> ""
		iloopcount = iloopcount+1
	loop
	
	On error goto 0
	
	totalRowcount = iloopcount-1
	For i = 1 To totalRowcount 
		PartnerFunction = oCustomerTable.GetCellData(i,"Partn.Funct.")
		Partner = oCustomerTable.GetCellData(i,"Partner")
		Name = oCustomerTable.GetCellData(i,"Name")
		Street = oCustomerTable.GetCellData(i,"Street")
		PostalCode = oCustomerTable.GetCellData(i,"Postal Code")
		Cty = oCustomerTable.GetCellData(i,"Cty")
		ocontrols.UpdateExecutionReport micPass,"ScreenValidation: Partner Information - "&PartnerFunction ,"Partner = "& Partner & " # Name = " & Name & " # Street = " & Street & " # PostalCode = " & PostalCode & " # Cty = " & Cty
	Next
	
	Set oCustomerTable = Nothing
	
	oControls.SendSAPkey F3
	
	oControls.SelectSAPTableRow "SAPMV45ATCTRL_U_ERF_AUFTRAG",1
	
	oControls.SendSAPkey F2
	
	MaterialName = oControls.GetSAPEditValue ("VBAP-MATWA")
	
	''MaterialNameFrmxl = mysheet.cells(2,12).value
	MaterialNameFrmxl = Split(arrCSVData,",")(2)
	
	If MaterialName = MaterialNameFrmxl  Then
		ocontrols.UpdateExecutionReport micPass,"ScreenValidation: Hybris Order Material Name is :"&MaterialNameFrmxl ,"Hybris Order Material Name is :"&MaterialName
	Else 
		ocontrols.UpdateExecutionReport micFail,"ScreenValidation: Hybris Order Material Name is :"&MaterialNameFrmxl ,"Hybris Order Tax Amount is :"&MaterialName
	End If
	
	oControls.SelectSAPTabStrip "TAXI_TABSTRIP_ITEM","Conditions"

	iReqRow = oControls.GetSAPTableRowbyCellContent ("SAPLV69ATCTRL_KONDITIONEN","CnTy","ZDIF")
	
	iZDIFAmount = oControls.GetSAPTableCellData("SAPLV69ATCTRL_KONDITIONEN",iReqRow,"Condition value")
	
	If trim(iZDIFAmount) = "0.00"  Then
		ocontrols.UpdateExecutionReport micPass,"ScreenValidation: ZDIF Value should be 0.00", "ZDIF Value is 0.00"
	Else
		ocontrols.UpdateExecutionReport micFail,"ScreenValidation: ZDIF Value should be 0.00", "ZDIF Value is " & iZDIFAmount
	End If
	
	iReqRow1 = oControls.GetSAPTableRowbyCellContent ("SAPLV69ATCTRL_KONDITIONEN","Name","Tax from Avalara")
	
	iAvalaraAmount = oControls.GetSAPTableCellData("SAPLV69ATCTRL_KONDITIONEN",iReqRow1,"Condition value")
	
	iReqRow2 = oControls.GetSAPTableRowbyCellContent ("SAPLV69ATCTRL_KONDITIONEN","Name","Tax Jur Code Level 1")
	
	iYTX1FAmount = oControls.GetSAPTableCellData("SAPLV69ATCTRL_KONDITIONEN",iReqRow2,"Condition value")
	
	iDiffAmount = (iAvalaraAmount - iYTX1FAmount)
	
	If iDiffAmount = 0  Then
		ocontrols.UpdateExecutionReport micPass,"ScreenValidation: Difference between 'Tax from Avalara' and 'Tax Jur Code Level 1' should be 0", "'Tax from Avalara' Amount is " & iAvalaraAmount & " and 'Tav Jur Code Level 1' Amount is " & iYTX1FAmount & " and difference is 0"
	Else
		ocontrols.UpdateExecutionReport micFail,"ScreenValidation: Difference between 'Tax from Avalara' and 'Tax Jur Code Level 1' should be 0", "'Tax from Avalara' Amount is " & iAvalaraAmount & " and 'Tav Jur Code Level 1' Amount is " & iYTX1FAmount
	End If
	'
	myxl.Workbooks.Close
	Set mysheet = nothing
	Set myxl = nothing
	
	Reporter.EndFunction
	Reporter.EndStatusTrack
	
End function


Function OrderValidation_Old_OR (arrCSVData, Iterator)
	
	''Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Order Validation for " & Iterator & " - Row of CSV File","True"
	
	' Éntering T code
	oControls.SetSAPOKCode ("/nVA03")
	''Entering Sales Order
	
	Set myxl = createobject("excel.application")
	myxl.Workbooks.Open "C:\RF_Share\Order_Validation.xlsx" 
	myxl.Application.Visible = FALSE
	Set mysheet = myxl.ActiveWorkbook.Worksheets("Sheet1")
	
	''oControls.SetSAPEdit "VBAK-VBELN",mysheet.cells(2,2).value
	oControls.SetSAPEdit "VBAK-VBELN", Split(arrCSVData,",")(1)
	
	''Enter
	oControls.ClickSAPEnter ()
	oControls.ClickSAPEnter ()
	
	sDeliveryBlock = oControls.GetSAPComboBoxValue ("VBAK-LIFSK")
	
	sBillingBlock = oControls.GetSAPComboBoxValue ("VBAK-FAKSK")
	
	If sDeliveryBlock = ""  Then
		ocontrols.UpdateExecutionReport micPass,"ScreenValidation: There should be no Delivery Block","There is no delivery block"
	Else 
		ocontrols.UpdateExecutionReport micFail,"ScreenValidation: There should be no Delivery Block","There is a delivery block - " & sDeliveryBlock
	End If
	
	If sBillingBlock = ""  Then
		ocontrols.UpdateExecutionReport micPass,"ScreenValidation: There should be no Billing Block","There is no Billing block"
	Else 
		ocontrols.UpdateExecutionReport micFail,"ScreenValidation: There should be no Billing Block","There is a delivery block - " & sBillingBlock
	End If
	
	oControls.ClickSAPButton "BT_HEAD"
	''order type
	OrderType = oControls.GetSAPEditValue ("VBAK-AUART")
	
	''order Reason
	OrderReason = oControls.GetSAPComboBoxValue ("VBAK-AUGRU")
	
	
	''Order Reason NOT provide in given CSV File
	''OrderReasonFrmxl = mysheet.cells(2,3).value
	
'	If OrderReason = OrderReason  Then
'		ocontrols.UpdateExecutionReport micPass,"ScreenValidation: Hybris Order Reason is:"&OrderReasonFrmxl ,"ECC Order Reason is:"&OrderReason
'	Else 
'		ocontrols.UpdateExecutionReport micFail,"ScreenValidation: Hybris Order Reason is:"&OrderReasonFrmxl ,"Hybris Order Reason is:"&OrderReason
'	End If
	
	PricingGroup = oControls.GetSAPComboBoxValue ("VBKD-KONDA")
	
	'Price Group NOT provide in given CSV File
	''PricingGroupFrmxl = mysheet.cells(2,4).value
	
'	If instr(1,PricingGroupFrmxl,PricingGroup) > 0  Then
'		ocontrols.UpdateExecutionReport micPass,"ScreenValidation: Hybris Order Pricing Group is:"&PricingGroupFrmxl ,"Hybris Order Pricing Group is:"&PricingGroup
'	Else 
'		ocontrols.UpdateExecutionReport micFail,"ScreenValidation: Hybris Order Pricing Group is:"&PricingGroupFrmxl ,"Hybris Order Pricing Group is:"&PricingGroup
'	End If
'	
	''select Shipping tab
	oControls.SelectSAPTabStrip "TAXI_TABSTRIP_HEAD","Shipping"
	
	''order ShippingCondition
	ShippingCondition = oControls.GetSAPComboBoxValue ("VBAK-VSBED")
	
	''Shiping Condition NOT provide in given CSV File
	''ShippingConditionFrmxl = mysheet.cells(2,5).value
	
'	If instr(1,ShippingConditionFrmxl,ShippingCondition) > 0  Then
'		ocontrols.UpdateExecutionReport micPass,"ScreenValidation: Hybris Order Shipping Condition is:"&ShippingConditionFrmxl ,"Hybris Order Shipping Condition is:"&ShippingCondition
'	Else 
'		ocontrols.UpdateExecutionReport micFail,"ScreenValidation: Hybris Order Shipping Condition is:"&ShippingConditionFrmxl ,"Hybris Order Shipping Condition is:"&ShippingCondition
'	End If
	
	
	''select Payment cards tab
	oControls.SelectSAPTabStrip "TAXI_TABSTRIP_HEAD","Payment cards"
	
	oControls.SelectSAPTableRow "SAPLV60FTCTRL_ZAHLUNGSKARTEN",2
	
	oControls.SendSAPkey F2
	'
	'AuthanticationNumber = oControls.GetSAPEditValue ("FPLTC-AUNUM")
	'
	'AuthanticationNumber = CDbl(AuthanticationNumber)
	'
	'AuthanticationNumberFrmxl = mysheet.cells(2,6).value
	'
	'
	'If AuthanticationNumber = AuthanticationNumberFrmxl  Then
	'
	'ocontrols.UpdateExecutionReport micPass,"ScreenValidation: Hybris Order Authantication Number is:"&AuthanticationNumberFrmxl ,"Hybris Order Authantication Number is:"&AuthanticationNumber
	'
	'Else 
	'
	'ocontrols.UpdateExecutionReport micFail,"ScreenValidation: Hybris Order Authantication Number is:"&AuthanticationNumberFrmxl ,"Hybris Order Authantication Number is:"&AuthanticationNumber
	'
	'End If
	
	'
	'
	'AuthanticationReferenceCode = oControls.GetSAPEditValue ("FPLTC-AUTRA")
	'
	'
	'AuthanticationReferenceCode = CDbl(AuthanticationReferenceCode)
	'
	'AuthanticationReferenceCodeFrmxl = mysheet.cells(2,7).value
	'
	'
	'If AuthanticationReferenceCode = AuthanticationReferenceCodeFrmxl  Then
	'
	'ocontrols.UpdateExecutionReport micPass,"ScreenValidation: Hybris OrderAuthantication Reference Code is:"&AuthanticationReferenceCodeFrmxl ,"Hybris OrderAuthantication Reference Code is:"&AuthanticationReferenceCode
	'
	'Else 
	'
	'ocontrols.UpdateExecutionReport micFail,"ScreenValidation: Hybris OrderAuthantication Reference Code is:"&AuthanticationReferenceCodeFrmxl ,"Hybris OrderAuthantication Reference Code is:"&AuthanticationReferenceCode
	'
	'End If
	'
	
	AuthorizedAmt = oControls.GetSAPEditValue ("FPLTC-AUTWR")
	AuthorizedAmt = CDbl(AuthorizedAmt)
	
	'CreditCard_authorizedAmount NOT provided in CSV file
	'AuthorizedAmtFrmxl = mysheet.cells(2,8).value
'	If replace(AuthorizedAmt, "$", "") = replace(AuthorizedAmtFrmxl, "$", "") Then
'		ocontrols.UpdateExecutionReport micPass,"ScreenValidation: Hybris OrderAuthorized Amount is:"&AuthorizedAmtFrmxl ,"Hybris OrderAuthorized Amount is:"&AuthorizedAmt
'	Else 
'		ocontrols.UpdateExecutionReport micFail,"ScreenValidation: Hybris OrderAuthorized Amount is:"&AuthorizedAmtFrmxl ,"Hybris OrderAuthorized Amount is:"&AuthorizedAmt
'	End If
	
	CardType = oControls.GetSAPEditValue ("FPLTC-CCINS")
	'CreditCard_Type NOT provided in CSV file
	''CardTypeFrmxl = mysheet.cells(2,9).value
	
'	If CardType = CardTypeFrmxl  Then
'		ocontrols.UpdateExecutionReport micPass,"ScreenValidation: Hybris Order Credit Card Type is:"&CardTypeFrmxl ,"Hybris Order Credit Card Type is:"&CardType
'	Else 
'		ocontrols.UpdateExecutionReport micFail,"ScreenValidation: Hybris Order Credit Card Type is:"&CardTypeFrmxl ,"Hybris Order Credit Card Type is:"&CardType
'	End If
'	
	NetValue = oControls.GetSAPEditValue ("KOMPAX-NETWR")
	
	NetValue = CDbl(NetValue)
	TaxAmount = oControls.GetSAPEditValue ("FPLAA-MWSBK")
	TaxAmount = CDbl(TaxAmount)
	TaxAmountFrmxl = mysheet.cells(2,11).value
	
	If replace(TaxAmount,"$","") = replace(TaxAmountFrmxl,"$","")   Then
		ocontrols.UpdateExecutionReport micPass,"ScreenValidation: Hybris Order Tax Amount is:"&TaxAmountFrmxl ,"ECC Order Tax Amount is :"&TaxAmount
	Else 
		ocontrols.UpdateExecutionReport micFail,"ScreenValidation: Hybris Order Tax Amount is:"&TaxAmountFrmxl ,"ECC Order Tax Amount is :"&TaxAmount
	End If
	
	
	iTotalAmount = TaxAmount + NetValue
	if iTotalAmount = AuthorizedAmt then
		ocontrols.UpdateExecutionReport micPass,"ScreenValidation: ECC Order Total Amount "& AuthorizedAmt &" should be sum of Net Value and Tax amount" ,"ECC Order Net Amount is :"&NetValue & ", Tax Amount is :"&TaxAmount
	Else
		ocontrols.UpdateExecutionReport micFail,"ScreenValidation: ECC Order Total Amount "& AuthorizedAmt &" should be sum of Net Value and Tax amount" ,"ECC Order Net Amount is :"&NetValue & ", Tax Amount is :"&TaxAmount
	End If
	oControls.SendSAPkey F3
	
	oControls.SelectSAPTabStrip "TAXI_TABSTRIP","Partners"
	
	iloopcount = 1
	
	On error resume next
	
	Set oCustomerTable = SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").SAPGuiTable("name:=SAPLV09CGV_TC_PARTNER_OVERVIEW")
	
	Do while oCustomerTable.GetCellData (iloopcount,"Partner")<> ""
		iloopcount = iloopcount+1
	loop
	
	On error goto 0
	
	totalRowcount = iloopcount-1
	For i = 1 To totalRowcount 
		PartnerFunction = oCustomerTable.GetCellData(i,"Partn.Funct.")
		Partner = oCustomerTable.GetCellData(i,"Partner")
		Name = oCustomerTable.GetCellData(i,"Name")
		Street = oCustomerTable.GetCellData(i,"Street")
		PostalCode = oCustomerTable.GetCellData(i,"Postal Code")
		Cty = oCustomerTable.GetCellData(i,"Cty")
		ocontrols.UpdateExecutionReport micPass,"ScreenValidation: Partner Information - "&PartnerFunction ,"Partner = "& Partner & " # Name = " & Name & " # Street = " & Street & " # PostalCode = " & PostalCode & " # Cty = " & Cty
	Next
	
	Set oCustomerTable = Nothing
	
	oControls.SendSAPkey F3
	
	oControls.SelectSAPTableRow "SAPMV45ATCTRL_U_ERF_AUFTRAG",1
	
	oControls.SendSAPkey F2
	
	MaterialName = oControls.GetSAPEditValue ("VBAP-MATWA")
	
	MaterialNameFrmxl = mysheet.cells(2,12).value
	
	If MaterialName = MaterialNameFrmxl  Then
		ocontrols.UpdateExecutionReport micPass,"ScreenValidation: Hybris Order Material Name is :"&MaterialNameFrmxl ,"Hybris Order Material Name is :"&MaterialName
	Else 
		ocontrols.UpdateExecutionReport micFail,"ScreenValidation: Hybris Order Material Name is :"&MaterialNameFrmxl ,"Hybris Order Tax Amount is :"&MaterialName
	End If
	
	oControls.SelectSAPTabStrip "TAXI_TABSTRIP_ITEM","Conditions"

	iReqRow = oControls.GetSAPTableRowbyCellContent ("SAPLV69ATCTRL_KONDITIONEN","CnTy","ZDIF")
	
	iZDIFAmount = oControls.GetSAPTableCellData("SAPLV69ATCTRL_KONDITIONEN",iReqRow,"Condition value")
	
	If trim(iZDIFAmount) = "0.00"  Then
		ocontrols.UpdateExecutionReport micPass,"ScreenValidation: ZDIF Value should be 0.00", "ZDIF Value is 0.00"
	Else
		ocontrols.UpdateExecutionReport micFail,"ScreenValidation: ZDIF Value should be 0.00", "ZDIF Value is " & iZDIFAmount
	End If
	
	iReqRow1 = oControls.GetSAPTableRowbyCellContent ("SAPLV69ATCTRL_KONDITIONEN","Name","Tax from Avalara")
	
	iAvalaraAmount = oControls.GetSAPTableCellData("SAPLV69ATCTRL_KONDITIONEN",iReqRow1,"Condition value")
	
	iReqRow2 = oControls.GetSAPTableRowbyCellContent ("SAPLV69ATCTRL_KONDITIONEN","Name","Tax Jur Code Level 1")
	
	iYTX1FAmount = oControls.GetSAPTableCellData("SAPLV69ATCTRL_KONDITIONEN",iReqRow2,"Condition value")
	
	iDiffAmount = (iAvalaraAmount - iYTX1FAmount)
	
	If iDiffAmount = 0  Then
		ocontrols.UpdateExecutionReport micPass,"ScreenValidation: Difference between 'Tax from Avalara' and 'Tax Jur Code Level 1' should be 0", "'Tax from Avalara' Amount is " & iAvalaraAmount & " and 'Tav Jur Code Level 1' Amount is " & iYTX1FAmount & " and difference is 0"
	Else
		ocontrols.UpdateExecutionReport micFail,"ScreenValidation: Difference between 'Tax from Avalara' and 'Tax Jur Code Level 1' should be 0", "'Tax from Avalara' Amount is " & iAvalaraAmount & " and 'Tav Jur Code Level 1' Amount is " & iYTX1FAmount
	End If
	'
	'YVCVAmount = replace(YVCVAmount,",",".")
	'
	'
	'YVCVAmount = split(YVCVAmount,".")
	'
	'
	'YVCVAmount = YVCVAmount(0)
	'
	'YVCVAmountFrmxl = mysheet.cells(2,13).value
	'
	'If YVCVAmount = YVCVAmountFrmxl  Then
	'
	'ocontrols.UpdateExecutionReport micPass,"Hybris Order YVCV Amount is :"&YVCVAmountFrmxl ,"Hybris Order YVCV Amount is :"&YVCVAmount
	'
	'Else 
	'
	'ocontrols.UpdateExecutionReport micFail,"Hybris Order YVCV Amount is :"&YVCVAmountFrmxl ,"Hybris Order YVCV Amount is :"&YVCVAmount
	'
	'End If
	'
	'
	'RowNumber1 = oControls.GetSAPTableRowbyCellContent ("SAPLV69ATCTRL_KONDITIONEN","CnTy","YVQV")
	'
	'
	'YVQVAmount = oControls.GetSAPTableCellData("SAPLV69ATCTRL_KONDITIONEN",RowNumber1,"Amount")
	'
	'YVQVAmount=replace(YVQVAmount,",",".")
	'
	'YVQVAmount = split(YVQVAmount,".")
	'
	'
	'YVQVAmount = YVQVAmount(0)
	'
	'
	'YVQVAmountFrmxl = mysheet.cells(2,14).value
	'
	'If YVQVAmount = YVQVAmountFrmxl  Then
	'
	'ocontrols.UpdateExecutionReport micPass,"Hybris Order YVQV Amount is :"&YVQVAmountFrmxl ,"Hybris Order YVQV Amount is :"&YVQVAmount
	'
	'Else 
	'
	'ocontrols.UpdateExecutionReport micFail,"Hybris Order YVQV Amount is :"&YVQVAmountFrmxl ,"Hybris Order YVQV Amount is :"&YVQVAmount
	'
	'End If
	'
	'If YVCVAmount <> "0" and YVQVAmount <> "0"Then
	'
	'ocontrols.UpdateExecutionReport micPass,"Hybris Order YVQV Amount is not equal to zero" ,"Hybris Order YVQV Amount is not equal to zero"
	'
	'
	'else
	'
	'exit Function
	'End If
	myxl.Workbooks.Close
	Set mysheet = nothing
	Set myxl = nothing
	
	Reporter.EndFunction
	Reporter.EndStatusTrack
	
End function

'=============================================================================================================

Function DeliveryValidation()
	''Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Delivery Validation","True"
	'' Éntering T code		
	oControls.SetSAPOKCode ("/nVA03")
	
	'enter outbound delivery no
	oControls.SetSAPEdit "VBAK-VBELN","1220907421"
	
	oControls.ClickSAPEnter ()
	
	oControls.SendSAPKey F5
	
	oControls.ClickSAPToolBarButton "&FIND"
	
	oControls.SetSAPModalEdit "LVC_S_SEA-STRING", "Cell Content", "Delivery"
	
	oControls.SendSAPKey ENTER
	
	
	iDelivery = oControls.GetSAPModalEditValue ("MESSTXT1","text")
	
	
	
	If iDelivery = "No corresponding entry found" Then
		
	ocontrols.UpdateExecutionReport micPass,"Hybris Delivery number should not created","Hybris Delivery number should not created"

	Else 

	ocontrols.UpdateExecutionReport micFail,"Hybris Delivery number should created" ,"Hybris Delivery number should created"
	
		
		
	End If
	
	oControls.ClickSAPModalEnter ()
	
	
	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
End Function

'=============================================================================================================


Function OrderTypeValidation ()
	''Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "OrderTypeValidation","True"
	Set myxl = createobject("excel.application")
	myxl.Workbooks.Open "C:\R_And_F\AutoSuiteSAP\R_And_F\Data\OrderType_Validation.xlsx" 
	myxl.Application.Visible = FALSE
	Set mysheet = myxl.ActiveWorkbook.Worksheets("Sheet1")

	'' Éntering T code		
	oControls.SetSAPOKCode ("/nVA03")
	
	'enter outbound delivery no
	oControls.SetSAPEdit "VBAK-VBELN", mysheet.cells(2,2).value
	
	oControls.ClickSAPEnter ()
	
	oControls.ClickSAPButton "BT_HEAD"
	''order type
	OrderType = oControls.GetSAPEditValue ("VBAK-AUART")
	
	OrderReason = oControls.GetSAPComboBoxValue ("VBAK-AUGRU")

	OrderReasonFromExcl = mysheet.cells(2,3).value

If OrderReason = OrderReasonFromExcl  Then

ocontrols.UpdateExecutionReport micPass,"Order Reason Expected Should be:"&OrderReasonFromExcl ,"Order Reason is:"&OrderReason

Else 

ocontrols.UpdateExecutionReport micFail,"Order Reason Expected Should be:"&OrderReasonFromExcl ,"Order Reason is:"&OrderReason

End If

Pricegroup = oControls.GetSAPComboBoxValue ("VBKD-KONDA")

PricegroupFromExcl = mysheet.cells(2,4).value

If Pricegroup = PricegroupFromExcl  Then

ocontrols.UpdateExecutionReport micPass,"Order Price group Expected Should be:"&PricegroupFromExcl,"Order Price group is:"&Pricegroup

Else 

ocontrols.UpdateExecutionReport micFail,"Order Price group Expected Should be:"&PricegroupFromExcl,"Order Price group is:"&Pricegroup

End If

oControls.SelectSAPTabStrip "TAXI_TABSTRIP_HEAD","Additional data B"


OrderReference = oControls.GetSAPEditValue ("VBAK-ZZORDREF")

OrderReference=CDbl(OrderReference)

OrderReferenceFromExcl = mysheet.cells(2,5).value

If OrderReference = OrderReferenceFromExcl Then

ocontrols.UpdateExecutionReport micPass,"Order Reference Expected Should be:"&OrderReferenceFromExcl ,"Order Reference is:"&OrderReference

Else 

ocontrols.UpdateExecutionReport micFail,"Order Reference  Expected Should be:"&OrderReferenceFromExcl ,"Order Reference is:"&OrderReference

End If

OrderTypeFromExcl = mysheet.cells(2,1).value

OrderTypeFromExcl1 = mysheet.cells(2,6).value

	If OrderType = OrderTypeFromExcl Then
	
	ocontrols.UpdateExecutionReport micPass,"Hybris Order type is DebitMemo :" &OrderTypeFromExcl1 ,"Hybris Order type is DebitMemo :" &OrderType

	Elseif OrderType = OrderTypeFromExcl1 Then 

	ocontrols.UpdateExecutionReport micPass,"Hybris Order type is CreditMemo :" &OrderTypeFromExcl1 ,"Hybris Order type is CreditMemo :" &OrderType
		
	else

	ocontrols.UpdateExecutionReport micFail,"Order should be a credit or debit memo","Order is neighter credit or nor a debit memeo"

	End If
	
'''Clicking on save button
'	statusVA01 =	oControls.GetSAPStatusBarInfo("item2")
'	
'	DataTable.Value("SalesOrder") = statusVA01
'	
	fnVA03SO_StandardOrder = 	mysheet.cells(2,2).value
'	
	'End Reporting 
	
	
	Reporter.EndFunction
	Reporter.EndStatusTrack
End Function
'=============================================================================================================

'For i = 1 To iTotalRows
'
'	index = i-1
'	''focus sap label
'	oControls.SetSAPLabelFocusUsingContent_Index "E1EDL24",index
'	''activate the label
'	oControls.SendSAPKey F2
'	''set the value
'	oControls.SetSAPModalOffsetEdit "SVALD-VALUE","31","1",DataTable.Value("Material")
'	'set the value
'	oControls.SetSAPModalOffsetEdit "SVALD-VALUE","31","12","Material Text"
'	
'	''click enter
'	oControls.ClickSAPModalEnter ()	
'	
'	''label focus 
'	oControls.SetSAPLabelFocusUsingContent_Index "E1EDL24",index
'	
'	If i < iTotalRows Then
'		''click copy button
'		oControls.SendSAPKey SHIFT_F5
'		''click paste button
'		oControls.SendSAPKey SHIFT_F6
'		''click at the same level button
'		oControls.ClickSAPButton "SPOP-VAROPTION2"
'		
'	Else 
'		
'		Exit for
'		
'	End If 
'	
'Next
'	
'	
'	
	
'=============================================================================================================
	
Function IdocProcessingFromHybris()
	''Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Idoc Processing From Hybris","True"
	'' Éntering T code		
	oControls.SetSAPOKCode ("/nSE38")
	
	'enter outbound delivery no
	oControls.SetSAPEdit "RS38M-PROGRAMM","RBDSER04"

	oControls.SendSAPKey F8
	
	oControls.SetSAPEdit "P_SERGRP","ZGRP_ORD_ACT"
	
	
	''sDateString = DateAdd("m",-1,date)
	Date1 = date-2
	
	oControls.SetSAPEdit "P_DATE-LOW",Date1
	
	oControls.SetSAPEdit "P_DATE-HIGH",date
	
	oControls.SendSAPKey F8
	
	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
End Function

'=============================================================================================================
	
'=============================================================================================================
Function BD87_Process_IndividualIDocs(sOrder)

	''Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Process IDocs ready to be transfer to Application - BD87","True"
	
	oControls.SetSAPOKCode ("/nWE02")
	
	oControls.SetSAPEdit "CREDAT-LOW", ""
	oControls.SetSAPEdit "CREDAT-HIGH", ""
	
	oControls.SelectSAPTabStrip "TABSTRIP_IDOCTABBL","EDI"
	
	oControls.SetSAPEdit "ARCKEY-LOW",sOrder & "*"
	'msgbox "before execute"
	oControls.SendSAPKey F8
	
'	oControls.SelectSAPTreeNode "0", "Selected IDocs;Inbound IDocs;ZSALESORDER_CREATEFROMDAT2"

	Set oSAPGrid = SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").SAPGuiGrid("name:=shell","type:=GuiShell", "guicomponenttype:=201")
	
	For iCurRow = 1 To oSAPGrid.RowCount ()
	
		If oSAPGrid.GetCellData(iCurRow,"Basic type") = "SALESORDER_CREATEFROMDAT202" Then
		
			iDocStatus = oSAPGrid.GetCellData(iCurRow,"Idoc Status")
			
			iReqIdoc = oSAPGrid.GetCellData(iCurRow, "IDoc Number")
			
		End If
		
	Next
	
	If trim(iDocStatus) = "53" Then
	
		oControls.UpdateExecutionReport micPass, "IDoc should be processed", "Status of IDoc " & iReqIdoc & " is 53"
		
	ElseIf trim(iDocStatus) = "64" Then
		
		oControls.SetSAPOKCode ("/nBD87")
		''Entering Sales Order
	
		oControls.SetSAPEdit "SX_DOCNU-LOW", iReqIdoc
	'msgbox "before execute"
		oControls.SendSAPKey F8
		
		oControls.SelectSAPTreeNode "0", "EQ2 Client 100;IDoc in inbound processing;IDoc ready to be transferred to application;#1"
		
		oControls.ClickSAPButton "Process"
		
		statusVA03 = oControls.GetSAPStatusBarInfo("messagetype")
		
		If statusVA03 <> "E" Then
			
			oControls.UpdateExecutionReport micPass, "IDoc should be processed", "IDoc " & iReqIdoc & " processed successfully"
		Else
			oControls.UpdateExecutionReport micPass, "IDoc should be processed",oControls.GetSAPStatusBarInfo("text")
		End  IF
		
	ElseIf trim(iDocStatus) = "51" Then
	
		oControls.UpdateExecutionReport micFail, "IDoc should be processed", "Status of IDoc " & iReqIdoc & " is 51"
		
	End If
	
	BD87_Process_IndividualIDocs = iReqIdoc 
	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
	
	If iDocStatus = "51" Then 
		ExitRun ()
	End If
	
End function



Sub Update_OrderStatus ()
	
	''Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Update Order staus - SE38","True"
	
	oControls.SetSAPOKCode ("/nSE38")
	
	'enter outbound delivery no
	oControls.SetSAPEdit "RS38M-PROGRAMM","ZSD_ORDSTAT_UPDATE"

	oControls.SendSAPKey F8
	
	'enter message type
	oControls.SetSAPEdit "P_MESTYP","ZSD_ORDSTAT"
	
	'enter message type
	oControls.SetSAPEdit "S_TIME-LOW",(Hour(now)-1) & ":00:00"
	
	'enter message type
	oControls.SetSAPEdit "S_TIME-HIGH","23:59:59"
	
	
	oControls.SendSAPKey F8
	
	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
	
End Sub

Sub ValidateSAP_TO_Hybris_IdocStatus (iIDoc)
	
	''Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Validate SAP to Hybris Idoc status - WE02","True"
	
	oControls.SetSAPOKCode ("/nWE02")
	
	'enter outbound delivery no
	oControls.SetSAPEdit "DOCNUM-LOW",iIDoc

	oControls.SendSAPKey F8
	
	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
	
End Sub

Function fnReadDatafromCSVFile()

	''Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Read Data from CSV file","True"
	
	Set fso=createobject("scripting.filesystemobject")
	Set f  = fso.OpenTextFile(pFolderPath & "\R_And_F\Data\EnrollmentOrder.csv")
	
	Dim arrCSVData() 
	intIncrement = 0
	
	While not f.AtEndOfStream
		ReDim Preserve arrCSVData(intIncrement)
		arrCSVData(intIncrement) = f.ReadLine
		intIncrement = intIncrement + 1
	Wend
	
	f.Close
	Set fso=Nothing
	
	fnReadDatafromCSVFile = arrCSVData
	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
	
End Function


Function fnWriteDatafromCSVFile(strUpdatedCSVData)

	'Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Write Data into CSV file","True"
	
	Set fsoCsv = CreateObject("scripting.filesystemobject")
	Set fCsv  = fsoCsv.OpenTextFile(pFolderPath & "\R_And_F\Data\EnrollmentOrder.csv", 2)
	
	strWriteData = "UserName,Order_Number,Material,Amount,tax,Internal_CV,OrderSV,taxableAmount,orderType,basePrice,OrderStatus,Carrier,TrackingNumber" & vbCrLf _
	               &  strUpdatedCSVData
	fCsv.Write strWriteData
	
	fCsv.Close
	Set fsoCsv = Nothing
	Set fCsv = Nothing
	
	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
	
End Function


