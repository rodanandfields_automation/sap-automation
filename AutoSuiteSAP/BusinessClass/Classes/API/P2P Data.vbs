

Function fnP2PStandard_Flow (strDataSheet)
	'@ Help                    :
	'@ Group                   : Common
	'@ Method Name             : fnP2PStandard_Flow()
	'@ Pre - Condition/Screen  : Data should be there in accessdb
	'@ Purpose                 : Processing all standard orders
	'@ Paramerters	           : None
	'@ Returns                 : None
  
  	DataTable.Import (pFolderPath & "\R_and_F\Data\P2P\" & strDataSheet &".xls")
	MsgBox strDataSheet
	'Start Reporting 
	Reporter.StartStatusTrack
	Reporter.StartFunction Trim(DataTable.Value("Description")),"True"
	''Reporter.StartFunction "fnP2PFlow standard order","True"
	
	'Loggin in to SAP system
	SAPLogon ()
	
	'Create Purchase Order
	strPONumber = fnME21N_CreatePurchaseOrder()
	
	If Trim(DataTable.Value("sTestCaseID")) = "TC_R+F_PTP_03" Then
		'Create Outbound Delivery
		strOutboundDelivery = fnVL10DCreateOutboundDelivery (strPONumber)
		
		'VL02N - Post Goods Issue
		fnVL02NPostGoodsIssue(strOutboundDelivery)
		                      
		'MB03 - Display PGI MaterialDocument
		fnMB03DisplayPGIMaterialDocument


		'VF01 Billing Document(strOutboundDelivery)
		fnVF01BillingDocument(strOutboundDelivery)
	End If
	
	'Release Purchase Order
	If Trim(DataTable.Value("sTestCaseID")) = "TC_R+F_PTP_01" Or Trim(DataTable.Value("sTestCaseID")) = "TC_R+F_PTP_02" Then
		fnME29N_ReleasePurchaseOrder(strPONumber)
	End  IF	
	
	'Goods Receipt
	strMaterialDocument = fnMIGO_GoodsReceipt(strPONumber)
	
	'Display Material Document
	fnMB03_DisplayMaterialDocument (strMaterialDocument)
	
	'Invoice Receipt
	strInvoiceReceiptDocument = fnMIROInvoiceReceipt(strPONumber, Trim(DataTable.Value("sTestCaseID")))
	
	'Display Invoice Document
	fnMIR4DisplayInvoiceDocument strInvoiceReceiptDocument

	'Display PO Number after completing all functionalities
	fnME23NDisplayPONumberDetails strPONumber
	
	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
	
End Function



Function fnME21N_CreatePurchaseOrder()

	''Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Creating Purchase Order","True"
	
	'Entering T code
	oControls.SetSAPOKCode ("/nME21N")
	
	'Select PO Type
	oControls.SelectSAPComboBoxKey "MEPO_TOPLINE-BSART", DataTable.Value("POType") 
	
	'Enter Vendor
	oControls.SetSAPEdit "MEPO_TOPLINE-SUPERFIELD", DataTable.Value("Vendor") 
	
	'Enter Org.Date
	oControls.SetSAPEdit "MEPO1222-EKORG", DataTable.Value("PurchOrg") 
	oControls.SetSAPEdit "MEPO1222-EKGRP", DataTable.Value("PurchGroup") 
	oControls.SetSAPEdit "MEPO1222-BUKRS", DataTable.Value("CompanyCode") 
	
	'Click Enter on Header Screen
	oControls.ClickSAPEnter ()
	
	If Trim(DataTable.Value("POType")) = "ZNB"  Then
		fnEnterMaterialDatainItemsTable (DataTable.Value("MaterialData"))
	Else
		'Read Material Data
		arrMaterialData = Split(DataTable.Value("MaterialData"),"||")
	
		'Entering Material Data in Item Overview Table
		For intArrCnt = LBound(arrMaterialData) To UBound(arrMaterialData)
			oControls.SetSAPTableCellData "SAPLMEGUITC_1211", intArrCnt + 1, "Material", Split(arrMaterialData(intArrCnt),";")(0)
			oControls.SetSAPTableCellData "SAPLMEGUITC_1211", intArrCnt + 1, "PO Quantity", Split(arrMaterialData(intArrCnt),";")(1) 
			oControls.SetSAPTableCellData "SAPLMEGUITC_1211", intArrCnt + 1, "Plnt", Split(arrMaterialData(intArrCnt),";")(2)
			oControls.SetSAPTableCellData "SAPLMEGUITC_1211", intArrCnt + 1, "Stor. Location", Split(arrMaterialData(intArrCnt),";")(3)
		Next
	End If
	
	If Trim(DataTable.Value("POType")) = "YRB"  Then  _
	SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").SAPGuiTable("name:=SAPLMEGUITC_1211").Object.Rows(0).Item(22).Selected = True
	
	'Click Enter on Header Screen
	oControls.ClickSAPEnter ()
	
	If Trim(DataTable.Value("POType")) = "ZNB"  Then
		
		'Navigat to Relase Startegy Tab
		oControls.SelectSAPTabStrip "ITEM_DETAIL", "Conditions"
		
		'Enter Amount for ZNB PO Type
		oControls.SetSAPTableCellData "SAPLV69ATCTRL_KONDITIONEN", 1, "Amount", 10
		
		'Enter Account Assignment Data
		'Navigat to Relase Startegy Tab
		oControls.SelectSAPTabStrip "ITEM_DETAIL", "Account Assignment"
		
		'Select AccAssCat
		oControls.SelectSAPComboBox "MEACCT1200-KNTTP","Cost center"
		
		'Enter G/L Account
		oControls.SetSAPEdit "MEACCT1100-SAKTO", "501105"
		
		'Enter Cost Center
		oControls.SetSAPEdit "COBL-KOSTL", "15006060" 
		
	End  If		
	
	If Trim(DataTable.Value("POType")) = "YRB"  Then
		
		'Navigat to Relase Startegy Tab
		oControls.SelectSAPTabStrip "ITEM_DETAIL", "Conditions"
		
		'Enter Amount for ZNB PO Type
		oControls.SetSAPTableCellData "SAPLV69ATCTRL_KONDITIONEN", 1, "Amount", 10
		
	End  If
	
	Wait 3
	'Click on Save Button
	oControls.SendSAPkey F11
	
	'Read & Verify PO has created or not
	strPONumber = oControls.GetSAPStatusBarInfo("item2")
	
	If IsNumeric(strPONumber) Or strPONumber <> "" Then
		fnME21N_CreatePurchaseOrder = strPONumber
		oControls.UpdateExecutionReport micPass, "PO Creation","Successfully PO has been created: '" & strPONumber & "' "
	Else  oControls.UpdateExecutionReport micFail, "PO Creation","PO creation is Failed"
	End If
	
	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
	
End function


Function fnEnterMaterialDatainItemsTable(strMaterialData)
	
	'Read Material Data
	arrMaterialData = Split(strMaterialData,"||")
	
	'Entering Material Data in Item Overview Table
	For intArrCnt = LBound(arrMaterialData) To UBound(arrMaterialData)
	oControls.SetSAPTableCellData "SAPLMEGUITC_1211", intArrCnt + 1, "A", Split(arrMaterialData(intArrCnt),";")(0)
	oControls.SetSAPTableCellData "SAPLMEGUITC_1211", intArrCnt + 1, "I", Split(arrMaterialData(intArrCnt),";")(1)
		oControls.SetSAPTableCellData "SAPLMEGUITC_1211", intArrCnt + 1, "Material", Split(arrMaterialData(intArrCnt),";")(2)
		oControls.SetSAPTableCellData "SAPLMEGUITC_1211", intArrCnt + 1, "Short Text", Split(arrMaterialData(intArrCnt),";")(3)
		oControls.SetSAPTableCellData "SAPLMEGUITC_1211", intArrCnt + 1, "PO Quantity", Split(arrMaterialData(intArrCnt),";")(4) 
		oControls.SetSAPTableCellData "SAPLMEGUITC_1211", intArrCnt + 1, "OUn", Split(arrMaterialData(intArrCnt),";")(5) 
		oControls.SetSAPTableCellData "SAPLMEGUITC_1211", intArrCnt + 1, "Matl Group", Split(arrMaterialData(intArrCnt),";")(6) 
		oControls.SetSAPTableCellData "SAPLMEGUITC_1211", intArrCnt + 1, "Plnt", Split(arrMaterialData(intArrCnt),";")(7)
		oControls.SetSAPTableCellData "SAPLMEGUITC_1211", intArrCnt + 1, "Stor. Location", Split(arrMaterialData(intArrCnt),";")(8)
	Next
	
End Function

Function fnME29N_ReleasePurchaseOrder(strPONumber)

	'Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Release Purchase Order","True"
	
	'Entering T code
	oControls.SetSAPOKCode ("/nME29N")
	
	'Navigat to created Purchase Order 
	fnOtherPurchaseOrder strPONumber
	
	'Navigat to Relase Startegy Tab
	oControls.SelectSAPTabStrip "HEADER_DETAIL", "Release strategy"
	
	intColumnCnt = SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").SAPGuiGrid("guicomponenttype:=201","name:=shell").ColumnCount
	If intColumnCnt = 5 Then
		intRowCount = oControls.GetSAPGridRowCount()
	
		'Click on Release button
		For Iterator = 1 To intRowCount Step 1
			oControls.ClickSAPGridCell Iterator, "Release Options"
		Next
	End If
	Wait(5)
	
	'Read & Verify 'Release effected with release code JA'
	strReleaseText = oControls.GetSAPStatusBarInfo("text")
	
	'Click on Save Button
	oControls.SendSAPkey F11
	Wait(5)
	
	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
	
End function


Function fnOtherPurchaseOrder(strPONumber)
	
	'Click on Other Purchase Order button in Menu
	oControls.ClickSAPButton "btn[17]"
	
	'Emter PO NUmber
	oControls.SetSAPEdit "MEPO_SELECT-EBELN", strPONumber
	
	'Click on Other Document Button in Popup window
	oControls.ClickSAPButton "btn[0]"
	
End Function


Function fnMIGO_GoodsReceipt(strPONumber)

	'Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Goods Receipt","True"
	
	Wait 15
	'Entering T code
	oControls.SetSAPOKCode ("/nMIGO")
	
	'Get Data for Created PO 
	oControls.SelectSAPComboBox "GODYNPRO-ACTION", "Goods Receipt"
	oControls.SelectSAPComboBox "GODYNPRO-REFDOC", "Purchase Order"
	oControls.SetSAPEdit "GODYNPRO-PO_NUMBER", strPONumber
	
	'Click Enter on Header Screen
	oControls.ClickSAPEnter ()
	
	'Read Material Data to Click on Item OK Check Box
	arrMaterialData = Split(DataTable.Value("MaterialData"),"||")
	
	For intArrCnt = LBound(arrMaterialData) To UBound(arrMaterialData)
		oControls.SelectSAPCheckBox "GODYNPRO-DETAIL_TAKE", "ON"
		If intArrCnt <> 0 Then oControls.ClickSAPButton "OK_NEXT_ITEM"
	Next
	
	'Click on Check Button
	oControls.ClickSAPButton "btn[7]"
	
	If SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").SAPGuiButton("type:=GuiButton","name:=btn[0]").Exist Then
		SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").SAPGuiButton("type:=GuiButton","name:=btn[0]").Click
	End If
	
	'Verify 'Document is O.K.' Text
	strDocumentIsOK = oControls.GetSAPStatusBarInfo("text")
	
	'Click on Post Button
	''If InStr(strDocumentIsOK,"Document is O.K.") > 0 Then 
	oControls.ClickSAPButton "btn[23]"
	
	'Read Material Document Posted is O.K.' Text
	strMaterialDocument = oControls.GetSAPStatusBarInfo("item1")
	fnMIGO_GoodsReceipt = strMaterialDocument  ''5000126966 
	
	
	If strMaterialDocument <> "" Then
		oControls.UpdateExecutionReport micPass, "Goods Receipt:", "Goods Receipt has been created, Goods Receipt: '"&strMaterialDocument&"'"
	Else
		oControls.UpdateExecutionReport micFail, "Goods Receipt:", "Goods Receipt has not created"
	End If
	
	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
	
End  Function



Function fnMB03_DisplayMaterialDocument(strMaterialDocument)
	
	'Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Display Material Document","True"
	Wait 35
	
	'Entering T code
	oControls.SetSAPOKCode ("/nMB03")
	
	'Emter Material Document NUmber
	oControls.SetSAPEdit "RM07M-MBLNR", strMaterialDocument
	
	'Click Enter on Header Screen
	oControls.ClickSAPEnter ()
	Wait 5
	
	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
	
End  Function

Function fnMIROInvoiceReceipt(strPONumber, sTestCaseID)
	
	'Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Invoice Receipt","True"
	
	'Entering T code
	oControls.SetSAPOKCode ("/nMIRO")
	
	'Select trasanction
	If InStr(sTestCaseID,"TC_R+F_PTP_06") > 0 Then
		oControls.SelectSAPComboBox "RM08M-VORGANG", "Credit Memo"
	Else
		oControls.SelectSAPComboBox "RM08M-VORGANG", "Invoice"
	End If
	
	'Enter Invoice Date
	oControls.SetSAPEdit "INVFO-BLDAT", Date
	oControls.ClickSAPEnter ()
	Wait 7
	
	'Enter Reference Data
	oControls.SetSAPEdit "INVFO-XBLNR", "QATesting" & RandomNumber(1000,9999)
	oControls.ClickSAPEnter ()
	Wait 7
	
	'Enter PO Number
	oControls.SetSAPEdit "RM08M-EBELN", strPONumber
	
	'Click Enter on Header Screen
	oControls.ClickSAPEnter ()
	Wait 7
	
	'Read Balance Amount
	strBalance = oControls.GetSAPEditValue("RM08M-DIFFERENZ")

	'Enter Amount
	oControls.SetSAPEdit "INVFO-WRBTR", Left(strBalance,Len(strBalance)-1)
	
	'Click on Save Button
	oControls.SendSAPkey F11
	
	'Click Enter on Header Screen
	''oControls.ClickSAPEnter ()
	
	If SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").SAPGuiEdit("name:=BSET-FWSTE","guicomponenttype:=31").Exist Then _
	oControls.ClickSAPEnter ()
	Wait 7
	
	'Read Material Document Posted is O.K.' Text
	strInvoiceReceiptDocument = oControls.GetSAPStatusBarInfo("text")
	
	fnMIROInvoiceReceipt = Trim(Split(strInvoiceReceiptDocument," ")(2))
	
	If Trim(Split(strInvoiceReceiptDocument," ")(2)) <> "" Then
		oControls.UpdateExecutionReport micPass, "Invoice Receipt:", "Invoice Receipt has been created, Invoice Receipt: '"&Trim(Split(strInvoiceReceiptDocument," ")(2))&"'"
	Else
		oControls.UpdateExecutionReport micFail, "Invoice Receipt:", "Invoice Receipt has not created"
	End If
	
	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
	
End Function

Function fnMIR4DisplayInvoiceDocument(strInvoiceReceiptDocument)

	'Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Display Invoice Receipt Document","True"
	
	'Entering T code
	oControls.SetSAPOKCode ("/nMIR4")
	
	'Enter Invoice Document No
	oControls.SetSAPEdit "RBKP-BELNR", strInvoiceReceiptDocument
	
	'Click Enter on Header Screen
	oControls.ClickSAPEnter ()
	Wait 10
	
	If SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").SAPGuiEdit("name:=RBKPV-BELNR","guicomponenttype:=31").Exist(7) Then
		sScreenName = SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").GetROProperty("text")
		oControls.UpdateExecutionReport micPass, "Invoice Receipt:", "Invoice Receipt Details has been displayed, Invoice Receipt: '"&strInvoiceReceiptDocument&"'"
	Else
		oControls.UpdateExecutionReport micFail, "Invoice Receipt:", "Invoice Receipt Details not displayed"
	End If
	
	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
End Function


Function VerifySAPModalWindowExistance ( )
	' @HELP
	' @class:		Controls_SAP
	' @method:		VerifySAPModalWindowExistance (sSAPTabStrip)
	' @notes:		returns modal window existance  
	' @END 
	On error resume next
		
	If err.description <> "" Then
		Reporter.ReportEvent micFail, "VerifySAPModalWindowExistance","Error Description: " & err.description
	End If
	
	Err.Clear
	On error goto 0	
End Function

Function fnVL10DCreateOutboundDelivery(strPONumber)

	'Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Create Outbound Delivery","True"
	
	'Entering T code
	oControls.SetSAPOKCode ("/nVL10D")
	
	'Navigate Purchase Orders Tab
	oControls.SelectSAPTabStrip "TABSTRIP_ORDER_CRITERIA", "Purchase Orders"
	
	'Entering T code
	oControls.SetSAPEdit "ST_EBELN-LOW", strPONumber
	
	Wait 3
	'Click on Execute Button
	oControls.SendSAPkey F8
	
	'Select Check Box/Selet All Button
	oControls.SendSAPkey F5
	
	'Click on Back Ground Button
	oControls.ClickSAPButton "btn[19]"
	Wait 5
	
	'Entering T code
	oControls.SetSAPOKCode ("/nME23N")
	
	'Navigat to created Purchase Order 
	fnOtherPurchaseOrder strPONumber
	
	'Navigat to Relase Startegy Tab
	oControls.SelectSAPTabStrip "ITEM_DETAIL", "Purchase Order History"

	'Read Outbound Delivery
	strOutboundDelivery = oControls.GetSAPGridCellData(1,"Material Document")
	strOutboundDeliveryText = oControls.GetSAPGridCellData(2,"Short Text")
	fnVL10DCreateOutboundDelivery = strOutboundDelivery
	
	If strOutboundDelivery <> "" And IsNumeric(strOutboundDelivery) Then
		oControls.UpdateExecutionReport micPass, "Ouboud Delivery:", "Outbound Delivery has been created, Outbound Delivery: '"& strOutboundDelivery &"'"
	Else
		oControls.UpdateExecutionReport micFail, "Ouboud Delivery:", "Outbound Delivery not created" 
	End If
		
	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
End Function


Function fnVL02NPostGoodsIssue(strOutboundDelivery)

	'Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Change Delivery - Post Goods Issue","True"
	
	'Entering T code
	oControls.SetSAPOKCode ("/nVL02N")
	
	'Entering Outbound Delivery 
	oControls.SetSAPEdit "LIKP-VBELN", strOutboundDelivery
	                                   
	'Click Enter on Header Screen
	oControls.ClickSAPEnter ()
	
	'Enter Storge Location
	oControls.SetSAPTableCellData "SAPMV50ATC_LIPS_OVER", 1, "SLoc", "1011"
	
	'Click on Header Details 
	oControls.SendSAPkey F8
	
	'Navigate to Shipment Tab
	oControls.SelectSAPTabStrip "TAXI_TABSTRIP_HEAD", "Shipment"
	
	'Enter BillofLad
	oControls.SetSAPEdit "LIKP-BOLNR", "9999999999" 
	
	'Navigate to Shipment Tab
	oControls.SelectSAPTabStrip "TAXI_TABSTRIP_HEAD", "Addtional Data"
	
	'Enter Carrier Name
	oControls.SetSAPEdit "GV_CRNAM", "FedEx"
	
	'Click on Post Goods Issue Button
	oControls.ClickSAPButton "btn[20]"
	
	'Read Material Document Posted is O.K.' Text
	strPGIData = oControls.GetSAPStatusBarInfo("text")
	
	If InStr(strPGIData, strOutboundDelivery) > 0 Then
		oControls.UpdateExecutionReport micPass, "Change delivery - PGI", "PGI material document should createed for, Outbound Delivery: '"& strOutboundDelivery &"'"
	Else
		oControls.UpdateExecutionReport micFail, "Change delivery - PGI", "PGI material document not createed" 
	End If
	
	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
End Function

Function fnMB03DisplayPGIMaterialDocument()
	
	'Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Display PGI material document","True"
	
	'Entering T code
	oControls.SetSAPOKCode ("/nME23N")
	
	'Navigat to created Purchase Order 
	fnOtherPurchaseOrder strPONumber
	
	'Navigat to Relase Startegy Tab
	oControls.SelectSAPTabStrip "ITEM_DETAIL", "Purchase Order History"

	'Read Outbound Delivery
	strPGIMaterialDocument = oControls.GetSAPGridCellData(1,"Material Document")
	
	'Entering T code
	oControls.SetSAPOKCode ("/nMB03")
	
	'Entering Outbound Delivery 
	oControls.SetSAPEdit "RM07M-MBLNR", strPGIMaterialDocument
	
	'Click Enter on Header Screen
	oControls.ClickSAPEnter ()
	Wait 10
	
	If SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").SAPGuiEdit("name:=RM07M-MTSNR","guicomponenttype:=31").Exist(7) Then
		sScreenName = SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").GetROProperty("text")
		oControls.UpdateExecutionReport micPass, "Display PGI Material Document:", "PGI Material Document: has been displayed, Outbound Delivery (PGI): '"& strPGIMaterialDocument &"'"
	Else
		oControls.UpdateExecutionReport micFail, "Display PGI Material Document:", "PGI Material Document: Details not displayed"
	End If
	
	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
End Function

Function fnVF01BillingDocument(strOutboundDelivery)

	'Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Billing Document (VI CC Billing to STO)","True"
	
	'Entering T code
	oControls.SetSAPOKCode ("/nVF01")
	
	'Enter Document Number
	oControls.SetSAPTableCellData "SAPMV60ATCTRL_ERF_FAKT", 1, "Document", strOutboundDelivery
	
	'Click Enter on Header Screen
	oControls.ClickSAPEnter ()
	Wait 10
	
	If SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").SAPGuiEdit("name:=VBRK-VBELN","guicomponenttype:=32").Exist(7) Then
		sScreenName = SAPGuiSession("guicomponenttype:=12").SAPGuiWindow("guicomponenttype:=21").GetROProperty("text")
		oControls.UpdateExecutionReport micPass, "Billing Document (VI CC Billing to STO):", "Billing Document (VI CC Billing to STO), Outbound Delivery : '"& strOutboundDelivery &"'"
	Else
		oControls.UpdateExecutionReport micFail, "Billing Document (VI CC Billing to STO):", "Billing Document (VI CC Billing to STO) Details not displayed"
	End If
	
End Function


Function fnME23NDisplayPONumberDetails(strPONumber)
	
	'Starting the reporting
	Reporter.StartStatusTrack	
	Reporter.StartFunction "Display PO Details","True"
	
	'Entering T code
	oControls.SetSAPOKCode ("/nME23N")
	
	'Navigat to created Purchase Order 
	fnOtherPurchaseOrder strPONumber
	
	'Navigat to Relase Startegy Tab
	oControls.SelectSAPTabStrip "ITEM_DETAIL", "Purchase Order History"

	'End Reporting 
	Reporter.EndFunction
	Reporter.EndStatusTrack
End Function
