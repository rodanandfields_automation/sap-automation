﻿'**********************************************Script Description*************************************************************
'Script Name							   			- 
'Purpose / Description                 		  - 
'********************************************************************************************************************************
'**********************************************Start of Framework initialization**********************************************
Set fso=CreateObject("scripting.filesystemobject")
pFolderPath=Environment("TestDir")
pFolderName=fso.GetParentFolderName(pFolderPath)
CommonExists=False

While not pFolderName=""
	If fso.FolderExists (pFolderName&"\BusinessClass") Then
		CommonExists=true
		ExecuteFile pFolderName &"\BusinessClass\Lib\ProjectMaster.vbs"
        pFolderName=""
	Else
		pFolderName=fso.GetParentFolderName(pFolderPath)
		pFolderPath=pFolderName
	End If	
Wend

If CommonExists=False Then
	Reporter.ReportEvent micFail,"Upload Automation Library","Does not found Script_declaration.vbs to upload the Library"
	ExitTest
End If

Set oDataAccess				= new clsDataAccess

'********************************************** Initialize Required Classes **************************************************
ErrComponentCreation "JPL-885-Receiving Report - Inbound receipts into 3PL"

'Loggin in to SAP system
SAPLogon ()

'Start Demand Planning & MRP
fnJPL885ReceivingReport_InboundReceiptsinto3PL

'**********************************************Release Object Variables****************************************************
Set oDataAccess				= Nothing
Set fso=Nothing
