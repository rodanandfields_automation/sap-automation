﻿'**********************************************Script Description*************************************************************
	
'Script Name							   			- 
Rem Purpose / Description                 		  - 
Rem ********************************************************************************************************************************
Rem **********************************************Start of Framework initialization**********************************************
Set fso=CreateObject("scripting.filesystemobject")
pFolderPath=Environment("TestDir")
pFolderName=fso.GetParentFolderName(pFolderPath)
CommonExists=False
While not pFolderName=""
	If fso.FolderExists (pFolderName&"\BusinessClass") Then
		CommonExists=true
		 ExecuteFile pFolderName &"\BusinessClass\Lib\ProjectMaster.vbs"
         pFolderName=""
	Else
		pFolderName=fso.GetParentFolderName(pFolderPath)
		pFolderPath=pFolderName
	End If	
Wend
If CommonExists=False Then
	Reporter.ReportEvent micFail,"Upload Automation Library","Does not found Script_declaration.vbs to upload the Library"
	ExitTest
End If
Set fso=Nothing
ErrComponentCreation sTestCaseID
''********************************************** Initialize Required Classes **************************************************


Dim aSOArray()
Set objExcel = CreateObject("Excel.Application")

Set objWorkbook = objExcel.Workbooks.Open("C:\RF_Share\Order_Validation.xlsx", True)
rowCount = 2'objExcel.Sheets(1).usedrange.rows.count
ReDim Preserve aSOArray(rowCount-2,8)

For intRow = 2 To RowCount
	aSOArray(intRow-2,0) = objExcel.Cells(intRow, 1).Value
	aSOArray(intRow-2,1) = objExcel.Cells(intRow, 9).Value
	aSOArray(intRow-2,2) = objExcel.Cells(intRow, 3).Value
	aSOArray(intRow-2,3) = objExcel.Cells(intRow, 4).Value
	aSOArray(intRow-2,4) = objExcel.Cells(intRow, 5).Value
	aSOArray(intRow-2,5) = objExcel.Cells(intRow, 6).Value
	aSOArray(intRow-2,6) = objExcel.Cells(intRow, 7).Value
	aSOArray(intRow-2,7) = objExcel.Cells(intRow, 8).Value
	aSOArray(intRow-2,8) = objExcel.Cells(intRow, 2).Value
Next
objExcel.Quit

DataTable.Import (pFolderPath & "\R_And_F\Data\OTCStandard_Flow.xls")

Call OTC_Flow_DeliveryDeletion_DebitMemoValidation(aSOArray(0,8)) @@ hightlight id_;_0_;_script infofile_;_ZIP::ssf54.xml_;_

' **********************************************Release Object Variables****************************************************
Set oDataAccess				= Nothing

