﻿'**********************************************Script Description*************************************************************
	
'Script Name							   			- 
Rem Purpose / Description                 		  - 
Rem ********************************************************************************************************************************
Rem **********************************************Start of Framework initialization**********************************************
Set fso=CreateObject("scripting.filesystemobject")
pFolderPath=Environment("TestDir")
pFolderName=fso.GetParentFolderName(pFolderPath)
CommonExists=False
While not pFolderName=""
	If fso.FolderExists (pFolderName&"\BusinessClass") Then
		CommonExists=true
		 ExecuteFile pFolderName &"\BusinessClass\Lib\ProjectMaster.vbs"
         pFolderName=""
	Else
		pFolderName=fso.GetParentFolderName(pFolderPath)
		pFolderPath=pFolderName
	End If	
Wend
If CommonExists=False Then
	Reporter.ReportEvent micFail,"Upload Automation Library","Does not found Script_declaration.vbs to upload the Library"
	ExitTest
End If
Set fso=Nothing
ErrComponentCreation sTestCaseID
''********************************************** Initialize Required Classes **************************************************

DataTable.Import (pFolderPath & "\R_And_F\Data\OTCStandard_Flow.xls")

Call ReturnOrderValidation ()

' **********************************************Release Object Variables****************************************************
Set oDataAccess				= Nothing
ExitRun ()




		
