﻿'**********************************************Script Description*************************************************************
	
'Script Name							   			- 
Rem Purpose / Description                 		  - 
Rem ********************************************************************************************************************************
Rem **********************************************Start of Framework initialization**********************************************
Set fso=CreateObject("scripting.filesystemobject")
pFolderPath=Environment("TestDir")
pFolderName=fso.GetParentFolderName(pFolderPath)
CommonExists=False

While not pFolderName=""
	If fso.FolderExists (pFolderName&"\BusinessClass") Then
		CommonExists=true
		 ExecuteFile pFolderName &"\BusinessClass\Lib\ProjectMaster.vbs"
         pFolderName=""
	Else
		pFolderName=fso.GetParentFolderName(pFolderPath)
		pFolderPath=pFolderName
	End If	
Wend

If CommonExists=False Then
	Reporter.ReportEvent micFail,"Upload Automation Library","Does not found Script_declaration.vbs to upload the Library"
	ExitTest
End If

Set fso=Nothing
ErrComponentCreation sTestCaseID
''********************************************** Initialize Required Classes **************************************************

DataTable.Import (pFolderPath & "\R_And_F\Data\OTCStandard_Flow.xls")

''Loggin in to SAP system
SAPLogon ()

'Read Data from CSV File
arrCSVData = fnReadDatafromCSVFile()

For Iterator = 1 To UBound(arrCSVData) Step 1
	'Call method for Order Validation
	Call OrderValidation (arrCSVData(Iterator), Iterator)
	
	'Method Callig for IdocProcessingFromHybris
	IdocProcessingFromHybris
	
	'Process IndividualIDocs
	iReqIdoc = BD87_Process_IndividualIDocs(Split(arrCSVData(Iterator),",")(1))
	
	Update_OrderStatus
	ValidateSAP_TO_Hybris_IdocStatus iReqIdoc
	
Next
 @@ hightlight id_;_0_;_script infofile_;_ZIP::ssf54.xml_;_
' **********************************************Release Object Variables****************************************************
Set oDataAccess				= Nothing
ExitRun ()


    
